﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CRUDFriend.Startup))]
namespace CRUDFriend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
