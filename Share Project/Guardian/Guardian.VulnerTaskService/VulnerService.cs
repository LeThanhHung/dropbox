﻿using log4net;
using Ninject;
using Oas.Infrastructure;
using Oas.Infrastructure.Domain;
using Oas.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Guardian.VulnerTaskService
{
    public partial class VulnerService : ServiceBase
    {
        Ninject.IKernel kernal;
        public readonly ILog log;
        Timer timer;
        private DatabaseContext dbContext;

        public DatabaseContext DbContext
        {
            get { return dbContext ?? (dbContext = kernal.Get<DatabaseContext>()); }

        }
        private IVulnerScanService _vulnerscanservice;
        public IVulnerScanService VulnerScanService { get { return _vulnerscanservice ?? (_vulnerscanservice = kernal.Get<IVulnerScanService>()); } }
        private IHistoryService _historyService;
        public IHistoryService historyService { get { return _historyService ?? (_historyService = kernal.Get<IHistoryService>()); } }
        private IScanDetailService _scanDetailService;
        public IScanDetailService scanDetailService { get { return _scanDetailService ?? (_scanDetailService = kernal.Get<IScanDetailService>()); } }
        private IScanService _scanService;
        public IScanService scanService { get { return _scanService ?? (_scanService = kernal.Get<IScanService>()); } }
        private IUserService _userService;
        public IUserService userService
        {
            get
            {
                _userService = _userService ?? (_userService = kernal.Get<IUserService>());
                return _userService;
            }
        }
        private IScanResultService _scanResultService;
        public IScanResultService scanResultService { get { return _scanResultService ?? (_scanResultService = kernal.Get<IScanResultService>()); } }
        private IScanResultDetailService _scanResultDetailService;
        public IScanResultDetailService scanResultDetailService { get { return _scanResultDetailService ?? (_scanResultDetailService = kernal.Get<IScanResultDetailService>()); } }
        private IImageCheckerService _imageCheckerService;
        public IImageCheckerService imageCheckerService { get { return _imageCheckerService ?? (_imageCheckerService = kernal.Get<IImageCheckerService>()); } }
        private IImageCheckerRestoreService _imageCheckerRestoreService;
        public IImageCheckerRestoreService imageCheckerRestoreService { get { return _imageCheckerRestoreService ?? (_imageCheckerRestoreService = kernal.Get<IImageCheckerRestoreService>()); } }
        private IRestoreHistoryService _restoreHistoryService;
        public IRestoreHistoryService restoreHistoryService { get { return _restoreHistoryService ?? (_restoreHistoryService = kernal.Get<IRestoreHistoryService>()); } }
        private IImageCheckerDetailService _imageCheckerDetailService;
        public IImageCheckerDetailService imageCheckerDetailService { get { return _imageCheckerDetailService ?? (_imageCheckerDetailService = kernal.Get<IImageCheckerDetailService>()); } }
        private ILoggingActionClientService _loggingActionClientService;
        public ILoggingActionClientService loggingActionClientService { get { return _loggingActionClientService ?? (_loggingActionClientService = kernal.Get<ILoggingActionClientService>()); } }


        public VulnerService()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            timer = new Timer();
            kernal = new StandardKernel();
            kernal.Load(Assembly.GetExecutingAssembly());
        }

        protected override void OnStart(string[] args)
        {
            Debugger.Launch();
            if (AppSettings.IntervalValue > 0)
            {
                timer.Interval = AppSettings.IntervalValue * 1000;
                timer.Elapsed += timer_Elapsed;
                timer.Start();
            }
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            //run Vulnerability
            VulnerabilityService(); 

            timer.Start();
        }

        protected override void OnStop()
        {
        }

        #region Private Function
        public void VulnerabilityService()
        {
            List<User> listuser = userService.Get();
            foreach (var user in listuser)
            {
                //get list Vulner Scan
                var list = VulnerScanService.GetByUserId(user.Id);
                if (list != null && list.Count > 0)
                {
                    //get list scan from raptor
                    var listvulner = VulnerabilityHandle.GetVulHistoryByUser(user.UserName);
                    foreach (var item in list)
                    {
                        if (String.IsNullOrEmpty(item.ReportPath))
                        {
                            var itemVulner = (from a in listvulner
                                              where a.ReportPath.EndsWith(String.Format("{0}.json", item.Id))
                                              select a).FirstOrDefault();

                            if (itemVulner != null)
                            {
                                item.IsCompleted = true;
                                item.IsInQueue = true;
                                item.IsScanning = false;
                                item.ReportPath = itemVulner.ReportPath;

                                VulnerScanService.Update(item);
                            }
                        }
                    }
                }

            }
        }
        #endregion 
    }

    public class AppSettings
    {
        public static int IntervalValue
        {
            get
            {
                var result = 0;
                Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["Interval"], out result);
                return result;
            }
        }
        public static string PathUploadTest
        {
            get
            {

                return System.Configuration.ConfigurationManager.AppSettings["PathUploadTest"];

            }
        }
        public static string PathUploadBackupZip
        {
            get
            {

                return System.Configuration.ConfigurationManager.AppSettings["PathUploadBackupZip"];

            }
        }
        public static string PathUPloadFTPUserFiles
        {
            get
            {

                return System.Configuration.ConfigurationManager.AppSettings["PathUPloadFTPUserFiles"];

            }
        }

        public static string PathUploadImageChecker
        {
            get
            {

                return System.Configuration.ConfigurationManager.AppSettings["PathUploadImageChecker"];

            }
        }
    }
}
