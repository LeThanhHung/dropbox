﻿using log4net;
using Ninject;
using Oas.Infrastructure;
using Oas.Infrastructure.Domain;
using Oas.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Guardian.BackupTaskService
{
    public partial class BackupTaskService : ServiceBase
    {
        Ninject.IKernel kernal;
        public readonly ILog log;
        Timer timer;
        private DatabaseContext dbContext;

        public DatabaseContext DbContext
        {
            get { return dbContext ?? (dbContext = kernal.Get<DatabaseContext>()); }

        }
        private IVulnerScanService _vulnerscanservice;
        public IVulnerScanService VulnerScanService { get { return _vulnerscanservice ?? (_vulnerscanservice = kernal.Get<IVulnerScanService>()); } }
        private IHistoryService _historyService;
        public IHistoryService historyService { get { return _historyService ?? (_historyService = kernal.Get<IHistoryService>()); } }
        private IScanDetailService _scanDetailService;
        public IScanDetailService scanDetailService { get { return _scanDetailService ?? (_scanDetailService = kernal.Get<IScanDetailService>()); } }
        private IScanService _scanService;
        public IScanService scanService { get { return _scanService ?? (_scanService = kernal.Get<IScanService>()); } }
        private IUserService _userService;
        public IUserService userService
        {
            get
            {
                _userService = _userService ?? (_userService = kernal.Get<IUserService>());
                return _userService;
            }
        }
        private IScanResultService _scanResultService;
        public IScanResultService scanResultService { get { return _scanResultService ?? (_scanResultService = kernal.Get<IScanResultService>()); } }
        private IScanResultDetailService _scanResultDetailService;
        public IScanResultDetailService scanResultDetailService { get { return _scanResultDetailService ?? (_scanResultDetailService = kernal.Get<IScanResultDetailService>()); } }
        private IImageCheckerService _imageCheckerService;
        public IImageCheckerService imageCheckerService { get { return _imageCheckerService ?? (_imageCheckerService = kernal.Get<IImageCheckerService>()); } }
        private IImageCheckerRestoreService _imageCheckerRestoreService;
        public IImageCheckerRestoreService imageCheckerRestoreService { get { return _imageCheckerRestoreService ?? (_imageCheckerRestoreService = kernal.Get<IImageCheckerRestoreService>()); } }
        private IRestoreHistoryService _restoreHistoryService;
        public IRestoreHistoryService restoreHistoryService { get { return _restoreHistoryService ?? (_restoreHistoryService = kernal.Get<IRestoreHistoryService>()); } }
        private IImageCheckerDetailService _imageCheckerDetailService;
        public IImageCheckerDetailService imageCheckerDetailService { get { return _imageCheckerDetailService ?? (_imageCheckerDetailService = kernal.Get<IImageCheckerDetailService>()); } }
        private ILoggingActionClientService _loggingActionClientService;
        public ILoggingActionClientService loggingActionClientService { get { return _loggingActionClientService ?? (_loggingActionClientService = kernal.Get<ILoggingActionClientService>()); } }


        public BackupTaskService()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            timer = new Timer();
            kernal = new StandardKernel();
        }

        protected override void OnStart(string[] args)
        {
            if (AppSettings.IntervalValue > 0)
            {
                timer.Interval = AppSettings.IntervalValue * 1000;
                timer.Elapsed += timer_Elapsed;
                timer.Start();
            }
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop(); 
            //run backup auto
            BackupService(); 

            timer.Start();
        }


        protected override void OnStop()
        {
            timer.Stop();
        }

        #region Private Function
        public void BackupService()
        {
            //get all user
            List<User> listuser = userService.Get();
            foreach (var user in listuser)
            {
                //get imagetrackers by user
                ImageChecker entity = new ImageChecker();
                entity = imageCheckerService.GetByUserId(user.Id).FirstOrDefault();
                if (entity == null || (entity != null && entity.LastModify.Day > DateTime.Now.Day && entity.LastModify.Month >= DateTime.Now.Month && entity.LastModify.Year >= DateTime.Now.Year)) return;

                if (entity.IsDownloaded)
                {
                    int DateOfMonth = DateTime.Now.Day;
                    //monthly
                    if ((entity.RunInMothOrWeek == 2 && DateOfMonth == 1)
                        || (entity.RunInMothOrWeek == 1 && DateTime.Now.DayOfWeek == DayOfWeek.Monday))
                    {
                        //change stauts to IsDownloading
                        entity.IsDownloaded = false;
                        entity.IsDownloading = true;
                        imageCheckerService.Update(entity);

                        ExecuteBackup(user, entity);
                    }
                }
            }

        } 
        public bool HasPersmissionFTP(string userId)
        {
            User user = userService.Get(userId);
            if (user != null)
            {
                try
                {
                    string localFile = String.Format("{0}/test{1:MMddyyyyHHmmss}.txt", AppSettings.PathUploadTest, DateTime.Now);
                    FileInfo file = new FileInfo(localFile);
                    if (!file.Exists)
                    {
                        using (FileStream stream = System.IO.File.Create(localFile))
                        {

                        }
                    }
                    return scanService.VerifyPermissionAccount(user.FtpServer, user.FtpPort, user.FtpAccount, user.FtpPassword, localFile);

                }
                catch { return false; }
            }
            return false;
        }

        private ImageCheckerRestore TrackImageCheckerRestore(Guid imageCheckerId, bool isSuccess, Guid userId, string action, string errorMessage = "", bool isRead = false)
        {
            try
            {
                ImageCheckerRestore restore = new ImageCheckerRestore();
                restore.Id = Guid.NewGuid();
                restore.ImageCheckerId = imageCheckerId;
                restore.IsSuccess = isSuccess;
                restore.ErrorMessage = errorMessage;
                restore.RestoredDate = DateTime.Now;
                restore.Action = action;
                restore.UserId = userId;
                restore.IsRead = isRead;
                imageCheckerRestoreService.Add(restore);

                return restore;
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return null;
        }

        private void ExecuteBackup(User currentUser, ImageChecker entity)
        {


            if (!HasPersmissionFTP(currentUser.Id))
            {
                entity.IsDownloaded = false;
                entity.IsDownloading = false;
                //save to db to mark as Downloaded.
                imageCheckerService.Update(entity);
                TrackImageCheckerRestore(Guid.Empty, false, Guid.Parse(currentUser.Id), "Backup", "No permission on your FTP Folder.");
            }
            try
            {

                //step 1 :verify account aganit  from ftp account 
                if (currentUser != null)
                {
                    string formatPathFile = "ftp://" + currentUser.FtpAccount + ':' + currentUser.FtpPassword + "@{0}:{1}//";
                    string rootFolder = String.Format(formatPathFile, currentUser.FtpServer, currentUser.FtpPort);
                    Guid QueueId = Guid.NewGuid();
                    string pathLocalFolderRoot = Path.Combine(AppSettings.PathUploadImageChecker, QueueId.ToString());
                    DirectoryInfo dirLocalfolder = Directory.CreateDirectory(pathLocalFolderRoot);
                    if (!dirLocalfolder.Exists) throw new Exception("Cannot create folder!");
                    string serverLocalFolder = String.Format("~/Upload/FTPUserFiles/ImageCheckerFiles/{0}", QueueId.ToString());

                    //step 2 : get file and folder from ftp account 
                    Task<List<ScanDetail>> list = scanService.BackupAllFileOfFTP(currentUser.FtpServer, currentUser.FtpPort, currentUser.FtpAccount, currentUser.FtpPassword, rootFolder, true, "/", Guid.Empty, pathLocalFolderRoot, QueueId, serverLocalFolder);
                    if (list != null && list.Result != null && list.Result.Count > 0)
                    {
                        // save all file to ImageCheckerDetail table 
                        List<ImageCheckerDetail> listImageCheckerDetails = new List<ImageCheckerDetail>();
                        list.Result.ForEach(c =>
                        {
                            ImageCheckerDetail imgDetail = new ImageCheckerDetail();
                            imgDetail.Id = Guid.NewGuid();
                            imgDetail.CreatedDate = DateTime.Now;
                            imgDetail.ImageCheckerId = entity.Id;
                            imgDetail.PathLocalFile = c.MapFileLocal;
                            imgDetail.PathServerFile = c.PathFile;
                            imgDetail.FileName = c.FileName;
                            imgDetail.Volume = c.Volume;

                            imageCheckerDetailService.Add(imgDetail);
                        });
                    }
                    entity.IsDownloaded = true;
                    entity.IsDownloading = false;
                    entity.Location = serverLocalFolder;
                    entity.LastModify = DateTime.Now;
                    //save to db to mark as Downloaded.
                    imageCheckerService.Update(entity);
                    TrackImageCheckerRestore(Guid.Empty, true, Guid.Parse(currentUser.Id), "Backup", "Backed up successful.");
                    return;
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
                TrackImageCheckerRestore(Guid.Empty, false, Guid.Parse(currentUser.Id), "Backup", ce.Message);
            }
            entity.IsDownloaded = false;
            entity.IsDownloading = false;
            //save to db to mark as Downloaded.
            imageCheckerService.Update(entity);
        }

        #endregion
    }

    public class AppSettings
    {
        public static int IntervalValue
        {
            get
            {
                var result = 0;
                Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["Interval"], out result);
                return result;
            }
        }
        public static string PathUploadTest
        {
            get
            {

                return System.Configuration.ConfigurationManager.AppSettings["PathUploadTest"];

            }
        }
        public static string PathUploadBackupZip
        {
            get
            {

                return System.Configuration.ConfigurationManager.AppSettings["PathUploadBackupZip"];

            }
        }
        public static string PathUPloadFTPUserFiles
        {
            get
            {

                return System.Configuration.ConfigurationManager.AppSettings["PathUPloadFTPUserFiles"];

            }
        }

        public static string PathUploadImageChecker
        {
            get
            {

                return System.Configuration.ConfigurationManager.AppSettings["PathUploadImageChecker"];

            }
        }
    }
}
