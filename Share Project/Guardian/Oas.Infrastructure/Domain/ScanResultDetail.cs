﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Domain
{
    public class ScanResultDetail
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ScanResultId { get; set; }
        public string AntiVirusName { get; set; }
        public int ScanResultI { get; set; }
        public string ThreatFound { get; set; }
        public DateTime DefTime { get; set; }
        public int ScanTimes { get; set; }

        [ForeignKey("ScanResultId")]
        public ScanResult ScanResult { get; set; }

        private bool isActive = true;
        [DefaultValue(true)]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
    }
}
