﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Domain
{
    public class ImageCheckerDetail
    {
        [Key]
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public Guid ImageCheckerId { get; set; }
        public string PathLocalFile { get; set; }
        public string Volume { get; set; }
        public string PathServerFile { get; set; }
        public DateTime CreatedDate { get; set; }

        private bool isActive = true;
        [DefaultValue(true)]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
        
    }
}
