﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Domain
{
    public class ScanDetail
    {
        [Key] 
        public Guid Id { get; set; }

        public string PathFile { get; set; }
        public string Volume { get; set; }
        public string FileName { get; set; }
        public string MapFileLocal { get; set; }
        public bool IsScaned { get; set; }
        public bool IsDownloaded { get; set; }
        public Guid QueueID { get; set; }
        public Guid HistoryID { get; set; }

        public string Data_Id { get; set; }
        public string Rest_Ip { get; set; }
       
        [ForeignKey("HistoryID")]
        public History History { get; set; }

        private bool isActive = true;
        [DefaultValue(true)]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
    }
}
