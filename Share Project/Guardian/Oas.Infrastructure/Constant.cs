﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure
{
    public class Constant
    {
        public const string AppConnection = "AppConnection";


        public const string Message_LoginSuccessful = "Login successful.";

        public const string Message_LoginUnsuccessful = "Login unsuccessful.";

        public const string Message_InvalidLogin = "Invalid UserName or Password.";
        
    }

    /// <summary>
    /// seal status
    /// </summary>
    public enum SealStatus
    { 
        Invalid,Valid
    }

    /// <summary>
    /// Standard , Premium -> month pay
    /// Plus -> Year pay
    /// </summary>
    public enum SealType
    { 
        Standard ,
        Premium,
        Plus
    }
}
