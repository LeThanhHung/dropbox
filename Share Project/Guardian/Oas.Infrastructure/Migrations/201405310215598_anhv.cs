namespace Oas.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class anhv : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessCategories", "GooglePlaceIconUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusinessCategories", "GooglePlaceIconUrl");
        }
    }
}
