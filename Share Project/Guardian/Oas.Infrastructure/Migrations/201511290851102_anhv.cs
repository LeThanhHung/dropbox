namespace Oas.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class anhv : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ImageCheckerDetails", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.ImageCheckerRestores", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.ImageCheckers", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.LoggingActionClients", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.RestoreHistories", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScanDetails", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScanResultDetails", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScanResults", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.VulnerScans", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VulnerScans", "IsActive");
            DropColumn("dbo.ScanResults", "IsActive");
            DropColumn("dbo.ScanResultDetails", "IsActive");
            DropColumn("dbo.ScanDetails", "IsActive");
            DropColumn("dbo.RestoreHistories", "IsActive");
            DropColumn("dbo.LoggingActionClients", "IsActive");
            DropColumn("dbo.ImageCheckers", "IsActive");
            DropColumn("dbo.ImageCheckerRestores", "IsActive");
            DropColumn("dbo.ImageCheckerDetails", "IsActive");
        }
    }
}
