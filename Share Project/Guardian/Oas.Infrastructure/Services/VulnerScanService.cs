﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Oas.Infrastructure.Model;
using System.Collections;

namespace Oas.Infrastructure.Services
{
    public class VulnerScanService : ServiceBase<VulnerScan>, IVulnerScanService
    { 
        public VulnerScanService(IRepository<VulnerScan> repository):base(repository) { }
        protected override Guid GetKey(VulnerScan entity)
        {
            return entity.Id;
        }

        public VulnerScan GetByNameAndUser(string packageName, string userId)
        {
            return Repository.Get.Where(c => c.Name.ToLower().Equals(packageName.ToLower()) && c.UserId == userId).FirstOrDefault();
        }


        public List<VulnerScan> GetByUserId(string userId)
        {
            return Repository.Get.Where(c => c.UserId.Equals(userId) && c.IsActive).ToList();
        }
    }
}
