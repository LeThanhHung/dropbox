﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public class RestoreHistoryService :ServiceBase<RestoreHistory>, IRestoreHistoryService
    {
        private readonly IRepository<RestoreHistory> RestoreHistoryRepository;

        public RestoreHistoryService(IRepository<RestoreHistory> ImageCheckerDetailRepository)
            : base(ImageCheckerDetailRepository)
        {
            this.RestoreHistoryRepository = ImageCheckerDetailRepository;
        }


        protected override Guid GetKey(RestoreHistory entity)
        {
            return entity.Id;
        }

        public List<RestoreHistory> GetByImageCheckerRestoreId(Guid imageCheckerRestoreId)
        {
           return Repository.Get.Where(c => c.ImageCheckerRestoreId.Equals(imageCheckerRestoreId)).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool BulkInsert(List<RestoreHistory> list)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AppConnection"].ConnectionString;
            if (!String.IsNullOrEmpty(connectionString))
            {
                try
                {
                    var result = (from a in list select a);
                    
                    IDataReader reader = result.AsDataReader();
                    //bulk data to db 
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        using (SqlTransaction tran = con.BeginTransaction())
                        {

                            SqlBulkCopy bc = new SqlBulkCopy(con,
                            SqlBulkCopyOptions.CheckConstraints |
                            SqlBulkCopyOptions.FireTriggers |
                            SqlBulkCopyOptions.KeepNulls, tran);

                            bc.BatchSize = 1000;
                            bc.DestinationTableName = "RestoreHistories";
                            bc.WriteToServer(reader);

                            tran.Commit();
                        }
                        con.Close();
                        return true;
                    }
                }
                catch
                { return false; }
            }
            return false;
        } 
       
    }
}
