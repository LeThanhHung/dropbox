﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public interface IRestoreHistoryService       :IService<RestoreHistory>
    { 
        List<RestoreHistory> GetByImageCheckerRestoreId(Guid imageCheckerRestoreId);
        bool BulkInsert(List<RestoreHistory> list);
         
    }
}
