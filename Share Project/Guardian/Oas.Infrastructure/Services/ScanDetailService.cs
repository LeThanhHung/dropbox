﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public class ScanDetailService : ServiceBase<ScanDetail> , IScanDetailService
    {
        private readonly IRepository<ScanDetail> scanDetailRepository;

        public ScanDetailService(IRepository<ScanDetail> scanDetailRepository)        :base(scanDetailRepository)
        {
            this.scanDetailRepository = scanDetailRepository;
        }

        protected override Guid GetKey(ScanDetail entity)
        {
            return entity.Id;
        }
         
        /// <summary>
        /// batch insert to scan detail
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool BulkInsert(List<ScanDetail> list)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AppConnection"].ConnectionString;
            if (!String.IsNullOrEmpty(connectionString))
            {
                try
                {
                    var result = (from a in list select a);

                    IDataReader reader = result.AsDataReader();
                    //bulk data to db 
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        using (SqlTransaction tran = con.BeginTransaction())
                        {

                            SqlBulkCopy bc = new SqlBulkCopy(con,
                            SqlBulkCopyOptions.CheckConstraints |
                            SqlBulkCopyOptions.FireTriggers |
                            SqlBulkCopyOptions.KeepNulls, tran);

                            bc.BatchSize = 1000;
                            bc.DestinationTableName = "ScanDetails";
                            bc.WriteToServer(reader);

                            tran.Commit();
                        }
                        con.Close();
                        return true;
                    }
                }
                catch
                { return false; }
            }
            return false;
        }
    }
}
