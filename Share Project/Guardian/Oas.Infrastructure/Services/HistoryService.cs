﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Oas.Infrastructure.Model;
using System.Collections;

namespace Oas.Infrastructure.Services
{
    public class HistoryService : ServiceBase<History>, IHistoryService
    {
        private readonly IRepository<History> historyRepository;
        private readonly IRepository<ScanDetail> scanDetailRepository;
        private readonly IRepository<ScanResult> scanResultRepository;
        private readonly IRepository<ScanResultDetail> scanResultDetailRepository;
        public HistoryService(IRepository<History> historyRepository, IRepository<ScanResultDetail> scanResultDetailRepository,
            IRepository<ScanResult> scanResultRepository, IRepository<ScanDetail> scanDetailRepository)
            : base(historyRepository)
        {
            this.historyRepository = historyRepository;
            this.scanDetailRepository = scanDetailRepository;
            this.scanResultDetailRepository = scanResultDetailRepository;
            this.scanResultRepository = scanResultRepository;
        }


        protected override Guid GetKey(History entity)
        {
            return entity.Id;
        }





        /// <summary>
        /// get Result view report 
        /// </summary>
        /// <param name="historyId"></param>
        /// <returns></returns>
        public HistoryModel GetViewReportById(Guid historyId)
        {
            HistoryModel model = new HistoryModel();
            History history = historyRepository.Get.Where(c => c.Id == historyId).FirstOrDefault();
            if (history != null)
            {
                model.History = history;
                model.ScanDetails = new List<ScanDetailModel>();
                //get scandetail by history Id 
                var scanDetailList = scanDetailRepository.Get.Where(c => c.HistoryID == historyId).ToList();
                var scanDetailIDList = (from a in scanDetailList
                                        select a.Id).ToList();
                //get scanResultList 
                var scanResultList = scanResultRepository.Get.Where(c => scanDetailIDList.Contains(c.ScanDetailId)).ToList();
                var scanResultIDList = (from a in scanResultList
                                        select a.Id).ToList();
                //get scanResultDetailList 
                var scanResultDetailList = scanResultDetailRepository.Get.Where(c => scanResultIDList.Contains(c.Id)).ToList();
                foreach (var scandetail in scanDetailList)
                {
                    ScanDetailModel sdm = new ScanDetailModel();
                    model.ScanDetails.Add(sdm);
                    sdm.ScanDetail = scandetail;
                    sdm.ScanResult = (from a in scanResultList
                                      where a.ScanDetailId == scandetail.Id
                                      select a).FirstOrDefault();
                    if (sdm.ScanResult != null)
                    {
                        //sdm.ScanResultDetails = (from a in scanResultDetailList
                        //                         where a.ScanResultId == sdm.ScanResult.Id
                        //                         select a).ToList();

                    }
                }
            }
            return model;
        }

        /// <summary>
        /// get summary 
        /// </summary>
        /// <param name="historyId"></param>
        /// <returns></returns>
        public List<ArrayList> GetSummaryScanById(Guid historyId)
        {
            List<ArrayList> list = new List<ArrayList>();

            //get scandetail by history Id 
            var scanDetailList = scanDetailRepository.Get.Where(c => c.HistoryID == historyId).ToList();
            var scanDetailIDList = (from a in scanDetailList
                                    select a.Id).ToList();

            //get scanResultList 
            var scanResultList = scanResultRepository.Get.Where(c => scanDetailIDList.Contains(c.ScanDetailId)).ToList();
            var scanResultIDList = (from a in scanResultList
                                    select a.Id).ToList();
            //get all file scan

            var listFile = scanResultDetailRepository.Get.Where(c => scanResultIDList.Contains(c.ScanResultId)).ToList();

            list = (from a in listFile
                    group a by a.AntiVirusName into b
                    select new ArrayList(new object[] { b.First().AntiVirusName, b.Where(d => !String.IsNullOrEmpty(d.ThreatFound)).Count() })).ToList();
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<HistoryLookupModel> GetListHistoryModelByUserID(string userId)
        {
            List<HistoryLookupModel> listModel = new List<HistoryLookupModel>();
            List<History> Listhistory = historyRepository.Get.Where(c => c.UserId == userId && c.IsActive).OrderByDescending(c => c.CreatedDate).ToList();
            if (Listhistory != null)
            {
                foreach (History history in Listhistory)
                {
                    HistoryLookupModel model = new HistoryLookupModel();
                    model.History = history;
                    model.ScanDetails = new List<ScanDetail>();
                    model.ScanResults = new List<ScanResult>();
                    //get scandetail by history Id 
                    var scanDetailList = scanDetailRepository.Get.Where(c => c.HistoryID == history.Id).ToList();
                    var scanDetailIDList = (from a in scanDetailList
                                            select a.Id).ToList();
                    //get scanResultList 
                    var scanResultList = scanResultRepository.Get.Where(c => scanDetailIDList.Contains(c.ScanDetailId)).ToList();


                    //set scandetails 
                    model.ScanDetails = scanDetailList;
                    model.ScanResults = scanResultList;

                    listModel.Add(model);
                }
            }
            return listModel;
        }


        public bool CheckExistByName(string name, string userId)
        {
            return Repository.Get.Where(c => c.Name.ToLower().Equals(name.ToLower().Trim()) && c.IsActive && c.UserId.Equals(userId)).FirstOrDefault() == null;
        }
    }
}
