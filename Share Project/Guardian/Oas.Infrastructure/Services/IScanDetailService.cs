﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public  interface IScanDetailService   :IService<ScanDetail>
    {   
        bool BulkInsert(List<ScanDetail> list);
    }
}
