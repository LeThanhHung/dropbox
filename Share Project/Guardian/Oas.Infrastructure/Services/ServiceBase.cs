﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public abstract class ServiceBase<TEntity> : IService<TEntity> where TEntity : class
    {
        protected readonly IRepository<TEntity> Repository;
        public ServiceBase(IRepository<TEntity> repository)
        {
            this.Repository = repository;
        }

        protected abstract Guid GetKey(TEntity entity);

        /// <summary>
        /// get entities
        /// </summary>
        /// <returns></returns>
        public IList<TEntity> Get()
        {
            var list = Repository.Get.ToList();
            if(list!=null && list.Count>0)
            {
                return list.Where(c=>CheckActive(c,true)).ToList();
            }
            return new List<TEntity>();
        }

        public IList<TEntity> Get(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return Repository.GetIncluding(includeProperties).ToList();
        }

        /// <summary>
        /// get histories by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TEntity Get(Guid id)
        {
            TEntity entity = Repository.Find(id);
            if (entity != null)
            {
                PropertyInfo pro = entity.GetType().GetProperty("IsActive");
                if (pro != null)
                {
                    bool isActive = (bool)pro.GetValue(entity);
                    if (isActive)
                    {
                        return entity;
                    }
                }

                return null;
            }
            return entity;
        }

        public bool CheckActive(TEntity entity, bool hasActive)
        {
            if (entity != null)
            {
                PropertyInfo pro = entity.GetType().GetProperty("IsActive");
                if (pro != null)
                {
                    bool isActive = (bool)pro.GetValue(entity);
                    if (hasActive == isActive)
                    {
                        return true;
                    }
                }

                return false;
            }
            return false;
        }

        public virtual TEntity Get(Guid Id, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// add new a School
        /// </summary>
        /// <param name="School"></param>
        /// <returns></returns>
        public TEntity Add(TEntity entity)
        {
            if (entity == null) return entity;
            PropertyInfo pro = entity.GetType().GetProperty("IsActive");
            if (pro != null)
            {
                pro.SetValue(entity, true);
            } 

            Repository.Add(entity);
            return entity;
        }

        /// <summary>
        /// update a School
        /// </summary>
        /// <param name="School"></param>
        /// <returns></returns>
        public TEntity Update(TEntity entity)
        { 
            Repository.Update(entity);
            return entity;
        }

        /// <summary>
        /// remove a School
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool Remove(Guid Id)
        {
            var entity = Get(Id);
            if (entity == null) return false;

            PropertyInfo pro = entity.GetType().GetProperty("IsActive");
            if (pro != null)
            {
                pro.SetValue(entity, false);
            } 
            Repository.Update(entity);
            return true;
        }

        public TEntity Save(TEntity entity)
        {
            return GetKey(entity) == Guid.Empty ? Add(entity) : Update(entity);
        }
    }
}
