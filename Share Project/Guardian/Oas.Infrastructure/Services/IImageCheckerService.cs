﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public interface IImageCheckerService : IService<ImageChecker>
    {

        IList<ImageChecker> GetByUserId(string userId);
        
        bool BulkInsert(List<ImageChecker> list); 
    }
}
