﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public interface IImageCheckerRestoreService : IService<ImageCheckerRestore>
    {
        List<ImageCheckerRestore> GetRestoreHistoryUserId(string userId);
    }
}
