﻿using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Oas.Infrastructure.Domain;
using Oas.Infrastructure.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Zip;
using log4net;

namespace Oas.Infrastructure.Services
{
    public class ScanService : IScanService
    {
        private readonly IRepository<History> historyRepository;
        private readonly IRepository<ScanDetail> scanDetailRepository;
        private readonly IRepository<ScanResult> scanResultRepository;
        private readonly IRepository<ScanResultDetail> scanResultDetailRepository;
        public readonly ILog log;
        public ScanService(IRepository<History> historyRepository, IRepository<ScanResultDetail> scanResultDetailRepository, IRepository<ScanResult> scanResultRepository, IRepository<ScanDetail> scanDetailRepository)
           
        {
            this.historyRepository = historyRepository;
            this.scanDetailRepository = scanDetailRepository;
            this.scanResultDetailRepository = scanResultDetailRepository;
            this.scanResultRepository = scanResultRepository;

            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        
        #region Service Methods

        /// <summary>
        /// get list information of files and folders of a FTP specificate
        /// </summary>
        /// <param name="servername"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="folder"></param>
        /// <param name="isRoot"></param>
        /// <param name="folderChange"></param>
        /// <param name="parentId"></param>
        /// <param name="folderLocal"></param>
        /// <param name="queueId"></param>
        /// <param name="serverLocalFolder"></param>
        /// <returns></returns>
        public async Task<List<ScanDetail>> GetAllFileOfFTP(string servername, int port, string username, string password, string folder, bool isRoot, string folderChange, Guid parentId, string folderLocal, Guid queueId, string serverLocalFolder, bool isNeedDownload=true)
        {
            List<Task> downloadTasks = new List<Task>();
            List<ScanDetail> listFile = new List<ScanDetail>();

            NetworkCredential networkCredential = new NetworkCredential(username, password);
            downloadTasks.Add(DownLoadFolder(downloadTasks, listFile, folderLocal, networkCredential, servername, string.Empty, parentId, queueId , isNeedDownload));

            await Task.WhenAll(downloadTasks);
            return listFile;
        }

        public async Task DownLoadFile(List<ScanDetail> listFile, string folderDestination, NetworkCredential credentials, string servername, string folderParent, List<FileSortInfo> fileList, Guid parentId, Guid queueId , bool isNeedDowload=true)
        {    

            string folderPath = string.Format(@"{0}{1}", folderDestination, folderParent.Replace("/", "\\"));
            if ( isNeedDowload && !Directory.Exists(folderPath))
            {
                DirectoryInfo dirInfo = Directory.CreateDirectory(folderPath);

            }

            foreach (var file in fileList)
            {
                string[] arr = file.FileName.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                string path = string.Format("ftp://{0}{1}/{2}", servername, folderParent, arr[0]);
                if (path.Trim().EndsWith(".")) continue;
                string filePath = string.Format(@"{0}\{1}", folderPath, arr[0]);
                  string remoteFile=string.Format("{0}/{1}",folderParent, arr[0]);
                #region mapping file list. AnhV if you need more information, please add inside region code
                ScanDetail tmp = new ScanDetail();
                tmp.Id = Guid.NewGuid();
                tmp.HistoryID = parentId;
                tmp.QueueID = queueId;
                tmp.Volume = file.Volume;
                tmp.FileName = Path.GetFileName(path);
                tmp.MapFileLocal = filePath;
                tmp.IsDownloaded =  isNeedDowload? DownloadFtpFile(remoteFile, filePath, credentials.UserName, credentials.Password, servername):true;
                tmp.PathFile = path;

                listFile.Add(tmp);
                #endregion    
               
            }
        }
         
        /// <summary>
        /// Download Folder from ftp to local
        /// </summary>
        /// <param name="listTask"></param>
        /// <param name="listFile"></param>
        /// <param name="folderDestination"></param>
        /// <param name="credentials"></param>
        /// <param name="servername"></param>
        /// <param name="folderParent"></param>
        /// <param name="parentId"></param>
        /// <param name="queueId"></param>
        /// <returns></returns>
        private async Task DownLoadFolder(List<Task> listTask, List<ScanDetail> listFile, string folderDestination, NetworkCredential credentials, string servername,
            string folderParent, Guid parentId, Guid queueId,bool isNeedDownload=true)
        {
            List<string> folderList = new List<string>();
            List<FileSortInfo> fileList = new List<FileSortInfo>();
            string content = GetContent(servername, folderParent, credentials, WebRequestMethods.Ftp.ListDirectoryDetails);

            if (!content.Contains("drwxr-xr-x"))
            {
                //Windows
                List<string> directories = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                folderList =
                   directories.Where(t => t.Contains("<DIR>"))
                   .Select(t => t.Split(new[] { "<DIR>", "\t" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim()).ToList();


                string[] extensionFileRemove = { ".JPG", ".JPEG", ".JPE", ".JFIF", ".TIF", ".TIFF", ".GIF", ".BMP", ".PNG", ".ICO", ".XML", ".CONFIG", ".FTPQUOTA", "" };
                fileList = directories.Where(t => !t.Contains("<DIR>") && !extensionFileRemove.Contains(Path.GetExtension(t).ToUpper()))
                    .Select(t =>
                    {
                        //DongTH fix bug
                        var x = t.Substring(17).Trim();
                        var fileName = x.Substring(x.IndexOf(" ")).Trim();
                        var volume = x.Substring(0, x.IndexOf(" ")).Trim();
                        return new FileSortInfo()
                        {
                            FileName = fileName,
                            Volume = volume
                        };

                    }).ToList();
            }
            else if (!String.IsNullOrEmpty(content))
            {
                //Linux
                List<string> directories = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                folderList =
                   directories.Where(t => t.Contains("drwxr-xr-x"))
                   .Select(t => t.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)[8].Trim()).ToList();

                folderList = folderList.Where(t => t.Replace(".", "").Count() != 0).ToList();

                string[] extensionFileRemove = { ".JPG", ".JPEG", ".JPE", ".JFIF", ".TIF", ".TIFF", ".GIF", ".BMP", ".PNG", ".ICO", ".XML", ".CONFIG", ".FTPQUOTA", "" };
                fileList = directories.Where(t => !t.Contains("drwxr-xr-x") && !extensionFileRemove.Contains(Path.GetExtension(t).ToUpper()))
                    .Select(t =>
                    {
                        //DongTH fix bug
                        var fileName = t.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)[8].ToString();
                        var volume = t.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)[4].ToString();
                        return new FileSortInfo()
                        {
                            FileName = fileName,
                            Volume = volume
                        };

                    }).ToList();
            }


            listTask.Add(DownLoadFile(listFile, folderDestination, credentials, servername, folderParent, fileList, parentId, queueId,isNeedDownload));

            Parallel.ForEach(folderList, async folder =>
            {
                listTask.Add(DownLoadFolder(listTask, listFile, folderDestination, credentials, servername, folderParent + "/" + folder, parentId, queueId,isNeedDownload));
            });
        }

        private async Task DownLoadAllFolder(List<Task> listTask, List<ScanDetail> listFile, string folderDestination, NetworkCredential credentials, string servername,
           string folderParent, Guid parentId, Guid queueId, bool isNeedDownload =true)
        {
            List<string> folderList = new List<string>();
            List<FileSortInfo> fileList = new List<FileSortInfo>();

            string content = GetContent(servername, folderParent, credentials, WebRequestMethods.Ftp.ListDirectoryDetails);

            if (!content.Contains("drwxr-xr-x"))
            {
                //Windows
                List<string> directories = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                folderList =
                   directories.Where(t => t.Contains("<DIR>"))
                   .Select(t => t.Split(new[] { "<DIR>", "\t" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim()).ToList();


                string[] extensionFileRemove = { ".JPG", ".JPEG", ".JPE", ".JFIF", ".TIF", ".TIFF", ".GIF", ".BMP", ".PNG", ".ICO", ".XML", ".CONFIG", ".FTPQUOTA", "" };
                fileList = directories.Where(t => !t.Contains("<DIR>") && !extensionFileRemove.Contains(Path.GetExtension(t).ToUpper()))
                    .Select(t =>
                    {
                        //DongTH fix bug
                        var x = t.Substring(17).Trim();
                        var fileName = x.Substring(x.IndexOf(" ")).Trim();
                        var volume = x.Substring(0, x.IndexOf(" ")).Trim();
                        return new FileSortInfo()
                        {
                            FileName = fileName,
                            Volume = volume
                        };

                    }).ToList();
            }
            else if (!String.IsNullOrEmpty(content))
            {
                //Linux
                List<string> directories = content.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                folderList =
                   directories.Where(t => t.Contains("drwxr-xr-x"))
                   .Select(t => t.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)[8].Trim()).ToList();

                folderList = folderList.Where(t => t.Replace(".", "").Count() != 0).ToList();

                string[] extensionFileRemove = { ".JPG", ".JPEG", ".JPE", ".JFIF", ".TIF", ".TIFF", ".GIF", ".BMP", ".PNG", ".ICO", ".XML", ".CONFIG", ".FTPQUOTA", "" };
                fileList = directories.Where(t => !t.Contains("drwxr-xr-x") && !extensionFileRemove.Contains(Path.GetExtension(t).ToUpper()))
                    .Select(t =>
                    {
                        //DongTH fix bug
                        var fileName = t.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)[8].ToString();
                        var volume = t.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)[4].ToString();
                        return new FileSortInfo()
                        {
                            FileName = fileName,
                            Volume = volume
                        };

                    }).ToList();
            }


            listTask.Add(DownLoadFile(listFile, folderDestination, credentials, servername, folderParent, fileList, parentId, queueId, isNeedDownload));

            Parallel.ForEach(folderList, async folder =>
            {
                listTask.Add(DownLoadAllFolder(listTask, listFile, folderDestination, credentials, servername, folderParent + "/" + folder, parentId, queueId,isNeedDownload));
            });
        }

        public async Task<List<ScanDetail>> BackupAllFileOfFTP(string servername, int port, string username, string password, string folder, bool isRoot, string folderChange, Guid parentId, string folderLocal, Guid queueId, string serverLocalFolder, bool isNeedDownload = true)
        {
            List<Task> downloadTasks = new List<Task>();
            List<ScanDetail> listFile = new List<ScanDetail>();

            NetworkCredential networkCredential = new NetworkCredential(username, password);
            downloadTasks.Add(DownLoadAllFolder(downloadTasks, listFile, folderLocal, networkCredential, servername, string.Empty, parentId, queueId,isNeedDownload));

            await Task.WhenAll(downloadTasks);
            return listFile;
        }

        /// <summary>
        /// Add more object for get volume
        /// </summary>
        public class FileSortInfo
        {
            public string FileName { get; set; }
            public string Volume { get; set; }
        }


        /// <summary>
        /// get infomation of result engines 
        /// </summary>
        /// <param name="rest_ip"></param>
        /// <param name="data_id"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string GetScanningFileInfo(string rest_ip, string data_id, string fileName)
        {
            var url = string.Format(@"https://{0}/file/{1}", rest_ip, data_id);
            var request = WebRequest.Create(url);
            request.Headers.Add("apikey", GetApiKey());
            request.Headers.Add("filename", fileName);
            request.Method = "GET";
            var value = request.GetResponse();
            var streamReader = new StreamReader(value.GetResponseStream());
            var jsonRaw = streamReader.ReadToEnd();
            streamReader.Close();
            return jsonRaw;
        }

        /// <summary>
        /// send a file to metascan for scanning directly with ftp url
        /// ftp://username:password@server-name/filename
        /// </summary>
        /// <param name="servername"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string ScanFtpFile(string servername, int port, string username, string password, string filePath)
        {
            string newFilePath = filePath.Insert(6, String.Format("{0}:{1}@", username, password));

            //set up scan configuration to scan
            var request = WebRequest.Create(GetScanUrl());
            request.Timeout = 1800000;
            request.Headers.Add("apikey", GetApiKey());
            request.Headers.Add("filename", newFilePath);
            request.Method = "POST";

            //send data to scan server
            var stream = request.GetRequestStream(); 
            stream.Close();

            //get feedback from server
            var value = request.GetResponse();
            var streamReader = new StreamReader(value.GetResponseStream());

            //process feedback
            var jsonRaw = streamReader.ReadToEnd();
            streamReader.Close();
            return jsonRaw;
        }

        /// <summary>
        /// send a file to metascan for scanning
        /// </summary>
        /// <param name="servername"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string ScanFile(string servername, int port, string username, string password, string filePath)
        {
            byte[] byteData = File.ReadAllBytes(filePath);

            //set up scan configuration to scan
            var request = WebRequest.Create(GetScanUrl());
            request.Timeout = 1800000;
            request.Headers.Add("apikey", GetApiKey());
            request.Headers.Add("filename", filePath);
            request.Method = "POST";

            //send data to scan server
            var stream = request.GetRequestStream();
            stream.Write(byteData, 0, byteData.Length);
            stream.Close();

            //get feedback from server
            var value = request.GetResponse();
            var streamReader = new StreamReader(value.GetResponseStream());

            //process feedback
            var jsonRaw = streamReader.ReadToEnd();
            streamReader.Close();
            return jsonRaw;
        }
        /// <summary>
        /// Execute rescan async a file 
        /// </summary>
        /// <param name="scanDetailId"></param>
        /// <param name="scanResultId"></param>
        /// <returns></returns>
        public async Task ExcuteRescanAsync(Guid scanDetailId, Guid scanResultId)
        {
            var scandetail = scanDetailRepository.Get.Where(c => c.Id == scanDetailId).FirstOrDefault();
            if (scandetail != null)
            {
                if (!String.IsNullOrEmpty(scandetail.Data_Id) && !String.IsNullOrEmpty(scandetail.Rest_Ip))
                {
                    //request to metascan for get result again 
                    GetReportInfoAsync(scandetail.Data_Id, scandetail.Rest_Ip, scandetail.FileName, scanResultId);
                }
            }
        }
        /// <summary>
        /// Get report of file scanned
        /// </summary>
        /// <param name="data_id"></param>
        /// <param name="rest_ip"></param>
        /// <param name="fileName"></param>
        /// <param name="ScanResultId"></param>
        /// <returns></returns>
        public bool GetReportInfoAsync(string data_id, string rest_ip, string fileName, Guid ScanResultId)
        {
            //try
            {
                var listScanResultDetail = new List<ScanResultDetail>();
                string reportRaw = GetScanningFileInfo(rest_ip, data_id, fileName);
                if (!String.IsNullOrEmpty(reportRaw))
                {
                    dynamic reportInfo = JsonConvert.DeserializeObject<JObject>(reportRaw);
                    if (reportInfo != null)
                    {
                        dynamic scanresults = reportInfo.scan_results;
                        if (scanresults != null)
                        {
                            ScanResult resultInfo = scanResultRepository.Get.Where(c => c.Id == ScanResultId).FirstOrDefault();
                            //get process  info    
                            resultInfo.Rescan_available = scanresults.rescan_available;
                            resultInfo.Scan_all_result_i = scanresults.scan_all_result_i;
                            resultInfo.Start_time = scanresults.start_time;
                            resultInfo.Total_time = scanresults.total_time;
                            resultInfo.Total_avs = scanresults.total_avs;
                            resultInfo.Progress_percentage = scanresults.progress_percentage;
                            resultInfo.In_queue = scanresults.in_queue;
                            resultInfo.Scan_all_result_a = scanresults.scan_all_result_a;
                            //update to db 
                            scanResultRepository.Update(resultInfo);
                            //get all scan detail
                            foreach (var prop in scanresults.scan_details)
                            {
                                int threadFound = 0;
                                Int32.TryParse(prop.Value.threat_found.ToString(), out threadFound);
                                ScanResultDetail detail = new ScanResultDetail();
                                detail.Id = Guid.NewGuid();
                                detail.ScanResultId = ScanResultId;
                                detail.AntiVirusName = prop.Name;

                                detail.ScanResultI = prop.Value.scan_result_i;
                                detail.ThreatFound = prop.Value.threat_found;
                                detail.DefTime = prop.Value.def_time;
                                detail.ScanTimes = prop.Value.scan_time;

                                listScanResultDetail.Add(detail);
                            }

                            //delete batch ScanResultDetail 
                            if (scanResultDetailRepository.DeleteBatch(ScanResultId, "ScanResultDetails", "ScanResultId"))
                            {
                                BulkInsert(listScanResultDetail);
                            }
                        }
                    }
                }
            }

            return true;
        }
        /// <summary>
        /// rollback result 
        /// </summary>
        /// <param name="historyId"></param>
        /// <returns></returns>
        public bool RollbackResultScan(Guid historyId)
        {
            try
            {
                List<ScanDetail> listscandetail = scanDetailRepository.Get.Where(c => c.HistoryID == historyId).ToList();
                List<Guid> listscanDetailID = (from a in listscandetail
                                               select a.Id).ToList();

                List<ScanResult> listscanresult = scanResultRepository.Get.Where(c => listscanDetailID.Contains(c.ScanDetailId)).ToList();
                foreach (ScanResult item in listscanresult)
                {
                    scanResultDetailRepository.DeleteBatch(item.Id, "ScanResultDetails", "ScanResultId");
                }
                foreach (Guid item in listscanDetailID)
                {
                    scanResultRepository.DeleteBatch(item, "ScanResult", "ScanDetailId");
                }
                scanDetailRepository.DeleteBatch(historyId, "ScanDetails", "HistoryId");
            }
            catch
            {
                return false;
            }
            return true;
        }

        #endregion Service Methods

        #region Private Methods

        /// <summary>
        /// Get Api Key
        /// </summary>
        /// <returns></returns>
        private static string GetApiKey()
        {
            try
            {
                return ConfigurationManager.AppSettings["ApiKey"] ?? "d60e5748638e1c90c347026c0413ade4";
            }
            catch
            {
                return "d60e5748638e1c90c347026c0413ade4";
            }
        }
        /// <summary>
        /// get Scan URL API
        /// </summary>
        /// <returns></returns>
        private static string GetScanUrl()
        {
            try
            {
                return ConfigurationManager.AppSettings["OnlineScanUrl"] ?? "https://scan.metascan-online.com/v2/file";
            }
            catch
            {
                return "https://scan.metascan-online.com/v2/file";
            }
        }
        /// <summary>
        /// bulk insert Scan Result Detail
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool BulkInsert(List<ScanResultDetail> list)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AppConnection"].ConnectionString;
            if (!String.IsNullOrEmpty(connectionString))
            {
                try
                {
                    var result = (from a in list select a);

                    IDataReader reader = result.AsDataReader();
                    //bulk data to db 
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        using (SqlTransaction tran = con.BeginTransaction())
                        {

                            SqlBulkCopy bc = new SqlBulkCopy(con,
                            SqlBulkCopyOptions.CheckConstraints |
                            SqlBulkCopyOptions.FireTriggers |
                            SqlBulkCopyOptions.KeepNulls, tran);

                            bc.BatchSize = 1000;
                            bc.DestinationTableName = "ScanResultDetails";
                            bc.WriteToServer(reader);

                            tran.Commit();
                        }
                        con.Close();
                        return true;
                    }
                }
                catch
                { return false; }
            }
            return false;
        }

        /// <summary>
        /// Verify account have full permission on who's folder.
        /// </summary>
        /// <param name="servername"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="fileLocal"></param>
        /// <returns></returns>
        public bool VerifyPermissionAccount(string servername, int port, string username, string password, string fileLocal)
        {
            bool isHavePermission = false;
            try
            {
                string folderName = String.Format("FTPUPLOAD_{0:MMddyyyyhhmmss}", DateTime.Now);
                //create folder
                bool isCreateFolder = CreateDirectory(folderName, username, password, servername);

                if (isCreateFolder)
                {
                    string messageError = string.Empty;
                    isHavePermission = UploadFtpFile(folderName + "/testupload.txt", fileLocal, username, password, servername, ref messageError);
                    isHavePermission = DeleteFTPDirectory(folderName, servername, username, password);
                }
            }
            catch (Exception ce)
            {
                return false;
            }
            return isHavePermission;
        }


        /* Upload File */
        public bool UploadFtpFile(string remoteFile, string localFile, string username, string password, string host,ref string messageError)
        {
            try
            {
                /* Create an FTP Request */
                FtpWebRequest ftpRequest = (FtpWebRequest)FtpWebRequest.Create(string.Format("ftp://{0}", host + "/" + remoteFile));
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(username, password);
                ftpRequest.Timeout = 1800000;
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftpRequest.ConnectionGroupName = "Guardian";
                ftpRequest.ServicePoint.ConnectionLimit = 8;
                 
                /* Establish Return Communication with the FTP Server */
                Stream ftpStream = ftpRequest.GetRequestStream();
                /* Open a File Stream to Read the File for Upload */

                FileStream localFileStream = File.OpenRead(localFile);
                /* Buffer for the Downloaded Data */
                byte[] byteBuffer = new byte[2048];
                int bytesSent = localFileStream.Read(byteBuffer, 0, 2048);
                /* Upload the File by Sending the Buffered Data Until the Transfer is Complete */
                try
                {
                    while (bytesSent != 0)
                    {
                        ftpStream.Write(byteBuffer, 0, bytesSent);
                        bytesSent = localFileStream.Read(byteBuffer, 0, 2048);
                    }
                }
                catch (Exception ex) { return false; }
                /* Resource Cleanup */
                localFileStream.Close();
                ftpStream.Close();
                ftpRequest = null;

            }
            catch (Exception ex) { messageError = ex.Message; log.Error(ex); return false; }
            return true;
        }

        /* Download File */
        public bool DownloadFtpFile(string remoteFile, string localFile, string username, string password, string host)
        {
            try
            {
                /* Create an FTP Request */
                FtpWebRequest ftpRequest = (FtpWebRequest)FtpWebRequest.Create(string.Format("ftp://{0}", host + "/" + remoteFile));
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(username, password);
                ftpRequest.Timeout = 1800000;
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;

                using (FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse())
                using (Stream responseStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(responseStream))
                using (StreamWriter destination = new StreamWriter(localFile))
                {
                    destination.Write(reader.ReadToEnd());
                    destination.Flush();
                }
            }
            catch (Exception ex) { log.Error(ex); return false; }
            return true;
        }

        /* Delete File */
        public bool DeleteFileFtp(string deleteFile, string username, string password, string host)
        {
            try
            {
                /* Create an FTP Request */
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(string.Format("ftp://{0}", host + "/" + deleteFile));
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(username, password);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                /* Establish Return Communication with the FTP Server */
                FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Resource Cleanup */
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { return false; }
            return true;
        }

        /* Rename File */
        public bool RenameFtpFile(string currentFileNameAndPath, string newFileName, string username, string password, string host)
        {
            try
            {
                /* Create an FTP Request */
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(string.Format("ftp://{0}", host + "/" + currentFileNameAndPath));
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(username, password);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.Rename;
                /* Rename the File */
                ftpRequest.RenameTo = newFileName;
                /* Establish Return Communication with the FTP Server */
                FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Resource Cleanup */
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { return false; }
            return true;
        }

        /* Create a New Directory on the FTP Server */
        public bool CreateDirectory(string newDirectory, string username, string password, string host)
        {
            try
            {
                /* Create an FTP Request */
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(string.Format("ftp://{0}", host + "/" + newDirectory));
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(username, password);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                /* Establish Return Communication with the FTP Server */
                FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Resource Cleanup */
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    response.Close();
                    return true;
                }
                else
                {
                    response.Close();
                    return false;
                }
            }
            return true;
        }
        /* Create a New Directory on the FTP Server */
        public bool DeleteFtpDirectory(string deleteDirectory, string username, string password, string host)
        {
            try
            {
                /* Create an FTP Request */
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(string.Format("ftp://{0}", host + "/" + deleteDirectory));
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(username, password);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.RemoveDirectory;
                /* Establish Return Communication with the FTP Server */
                FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Resource Cleanup */
                ftpResponse.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { return false; }
            return true;
        }
        private static List<string> DirectoryListing(string Path, string ServerAdress, string Login, string Password)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + ServerAdress + "/" + Path);
            request.Credentials = new NetworkCredential(Login, Password);
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = true;
            request.Method = WebRequestMethods.Ftp.ListDirectory;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);

            List<string> result = new List<string>();

            while (!reader.EndOfStream)
            {
                result.Add(reader.ReadLine());
            }

            reader.Close();
            response.Close();

            return result;
        }
        private void DeleteFTPFile(string Path, string ServerAdress, string Login, string Password)
        {
            FtpWebRequest clsRequest = (System.Net.FtpWebRequest)WebRequest.Create("ftp://" + ServerAdress + "/" + Path);
            clsRequest.Credentials = new System.Net.NetworkCredential(Login, Password);
            clsRequest.UsePassive = true;
            clsRequest.UseBinary = true;
            clsRequest.KeepAlive = true;
            clsRequest.Method = WebRequestMethods.Ftp.DeleteFile;

            string result = string.Empty;
            FtpWebResponse response = (FtpWebResponse)clsRequest.GetResponse();
            long size = response.ContentLength;
            Stream datastream = response.GetResponseStream();
            StreamReader sr = new StreamReader(datastream);
            result = sr.ReadToEnd();
            sr.Close();
            datastream.Close();
            response.Close();
        }
        private bool DeleteFTPDirectory(string Path, string ServerAdress, string Login, string Password)
        {
            try
            {
                FtpWebRequest clsRequest = (System.Net.FtpWebRequest)WebRequest.Create("ftp://" + ServerAdress + "/" + Path);
                clsRequest.Credentials = new System.Net.NetworkCredential(Login, Password);
                clsRequest.UsePassive = true;
                clsRequest.UseBinary = true;
                clsRequest.KeepAlive = true;
                List<string> filesList = DirectoryListing(Path, ServerAdress, Login, Password);

                foreach (string file in filesList)
                {
                    if (!file.EndsWith("."))
                        DeleteFTPFile(Path + "/" + file, ServerAdress, Login, Password);
                }

                clsRequest.Method = WebRequestMethods.Ftp.RemoveDirectory;

                string result = string.Empty;
                FtpWebResponse response = (FtpWebResponse)clsRequest.GetResponse();
                long size = response.ContentLength;
                Stream datastream = response.GetResponseStream();
                StreamReader sr = new StreamReader(datastream);
                result = sr.ReadToEnd();
                sr.Close();
                datastream.Close();
                response.Close();
            }
            catch { return false; }
            return true;
        }

        public string GetContent(string servername, string folderParent, NetworkCredential credentials, string webmethod)
        {
            string content = string.Empty;

            try
            {
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(string.Format("ftp://{0}{1}", servername, folderParent));

                ftpRequest.Credentials = credentials;
                ftpRequest.UsePassive = true;
                ftpRequest.UseBinary = true;
                ftpRequest.KeepAlive = true;
                ftpRequest.Method = webmethod; //WebRequestMethods.Ftp.ListDirectoryDetails;
                ftpRequest.Timeout = 1200000;
                ftpRequest.ReadWriteTimeout = 1200000;
               
                using (FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    StreamReader streamReader = new StreamReader(response.GetResponseStream());

                    //String content = "drwxr-xr-x 1 ftp ftp              0 Feb 26 18:53 .\r\ndrwxr-xr-x 1 ftp ftp              0 Feb 26 18:53 ..\r\n-rw-r--r-- 1 ftp ftp         699192 Feb 17 19:11 MikeKoonA_45_C_CTS.jpg\r\n-rw-r--r-- 1 ftp ftp             60 Feb 17 19:11 MikeKoonA_45_C_CTS.txt\r\ndrwxr-xr-x 1 ftp ftp              0 Feb 26 18:53 misc\r\n-rw-r--r-- 1 ftp ftp        2796344 Feb 22 10:59 RayMossA_24_C_CTS.dds";
                    content = streamReader.ReadToEnd();
                    streamReader.Close();
                    response.Close();
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return content;
        }

        #endregion Private Methods


        public bool ZipCurrentSource(string pathFolderNeedZip, string pathzipedFolder)
        {
            try
            {
                if (!String.IsNullOrEmpty(pathFolderNeedZip))
                {
                    ZipOutputStream zip = new ZipOutputStream(File.Create(pathzipedFolder));
                    zip.SetLevel(9);
                    ZipFolder(pathFolderNeedZip, pathFolderNeedZip, zip);
                    zip.Finish();
                    zip.Close();
                }
            }
            catch (Exception ce)
            { return false; }
            return true;
        }

        public static void ZipFolder(string RootFolder, string CurrentFolder, ZipOutputStream zStream)
        {
            string[] SubFolders = Directory.GetDirectories(CurrentFolder);

            //calls the method recursively for each subfolder
            foreach (string Folder in SubFolders)
            {
                ZipFolder(RootFolder, Folder, zStream);
            }

            string relativePath = CurrentFolder.Substring(RootFolder.Length) + "/";

            //the path "/" is not added or a folder will be created
            //at the root of the file
            if (relativePath.Length > 1)
            {
                ZipEntry dirEntry;
                dirEntry = new ZipEntry(relativePath);
                dirEntry.DateTime = DateTime.Now;
            }

            //adds all the files in the folder to the zip
            foreach (string file in Directory.GetFiles(CurrentFolder))
            {
                AddFileToZip(zStream, relativePath, file);
            }
        }

        private static void AddFileToZip(ZipOutputStream zStream, string relativePath, string file)
        {
            byte[] buffer = new byte[4096];

            //the relative path is added to the file in order to place the file within
            //this directory in the zip
            string fileRelativePath = (relativePath.Length > 1 ? relativePath : string.Empty)
                                      + Path.GetFileName(file);

            ZipEntry entry = new ZipEntry(fileRelativePath);
            entry.DateTime = DateTime.Now;
            zStream.PutNextEntry(entry);

            using (FileStream fs = File.OpenRead(file))
            {
                int sourceBytes;
                do
                {
                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                    zStream.Write(buffer, 0, sourceBytes);
                } while (sourceBytes > 0);
            }
        }


        public FolderLocalInfo ReadArtchitectFolder(string rootPath, out int totalFolder, out int totalFile)
        {
            if (!String.IsNullOrEmpty(rootPath))
            {
                FolderLocalInfo root = new FolderLocalInfo();
                DirectoryInfo dirRoot = new DirectoryInfo(rootPath);
                if (dirRoot.Exists)
                {
                    root.FolderName = dirRoot.Name;
                    root.FolderLocalInfos = new List<FolderLocalInfo>();
                    root.FileLocalInfos = new List<FileLocalInfo>();

                    //get all children folder 
                    DirectoryInfo[] listdir = dirRoot.GetDirectories();
                    totalFolder = listdir.Count();
                    foreach (DirectoryInfo dir in listdir)
                    {
                        FolderLocalInfo child = ReadArtchitectFolder(Path.Combine(rootPath, dir.Name), out totalFolder, out totalFile);
                        if (child != null)
                        {
                            root.FolderLocalInfos.Add(child);
                        }
                    }

                    //get all file in this folder 
                    FileInfo[] listFile = dirRoot.GetFiles();
                    totalFile = listFile.Count();
                    foreach (FileInfo f in listFile)
                    {
                        FileLocalInfo fileLocal = new FileLocalInfo();
                        fileLocal.FileName = f.Name;
                        fileLocal.PathFile = Path.Combine(rootPath, f.FullName);
                        root.FileLocalInfos.Add(fileLocal);
                    }

                    return root;
                }
            }
            totalFolder = 0;
            totalFile = 0;
            return null;
        }
    }
}