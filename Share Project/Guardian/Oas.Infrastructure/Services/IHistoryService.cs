﻿using Oas.Infrastructure.Domain;
using Oas.Infrastructure.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public  interface IHistoryService     :IService<History>
    {   
        HistoryModel GetViewReportById(Guid historyId);

        List<ArrayList> GetSummaryScanById(Guid historyId);

        List<HistoryLookupModel> GetListHistoryModelByUserID(string userId);

        bool CheckExistByName(string name,string username);
    }
}
