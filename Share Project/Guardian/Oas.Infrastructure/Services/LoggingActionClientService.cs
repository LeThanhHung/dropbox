﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Oas.Infrastructure.Model;
using System.Collections;

namespace Oas.Infrastructure.Services
{
    public class LoggingActionClientService :ServiceBase<LoggingActionClient>, ILoggingActionClientService
    {
        private readonly IRepository<LoggingActionClient> LoggingActionClientRepository;
        public LoggingActionClientService(IRepository<LoggingActionClient> LoggingActionClientRepository)
            : base(LoggingActionClientRepository)
        {
            this.LoggingActionClientRepository = LoggingActionClientRepository;     
        }
        protected override Guid GetKey(LoggingActionClient entity)
        {
            return entity.Id;
        }       
    }
}
