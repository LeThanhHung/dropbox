﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public class ImageCheckerRestoreService :ServiceBase<ImageCheckerRestore>, IImageCheckerRestoreService
    {
        private readonly IRepository<ImageCheckerRestore> ImageCheckerRestoreRepository;

        public ImageCheckerRestoreService(IRepository<ImageCheckerRestore> ImageCheckerRestoreRepository)      :base
            (ImageCheckerRestoreRepository)
        {
            this.ImageCheckerRestoreRepository = ImageCheckerRestoreRepository;
        }


        protected override Guid GetKey(ImageCheckerRestore entity)
        {
            return entity.Id;
        }

        public List<ImageCheckerRestore> GetRestoreHistoryUserId(string userId)
        {
            Guid id = !String.IsNullOrEmpty(userId) ? Guid.Parse(userId) : Guid.Empty;
            return Repository.Get.Where(c => c.UserId.Equals(id)
                            && (c.Action.ToLower().Equals("backup") || c.Action.ToLower().Equals("restore"))).ToList();
        }
       
    }
}
