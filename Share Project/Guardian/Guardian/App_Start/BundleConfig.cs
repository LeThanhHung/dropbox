﻿using System.Web;
using System.Web.Optimization;

namespace Guardian
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/app").IncludeDirectory("~/app","*.js",true));

            bundles.Add(new ScriptBundle("~/bundles/app")
                .Include(
                      "~/app/ng-app.js",
                      "~/app/master-service.js",
                      "~/app/controller/master-controller.js",
                      "~/app/controller/mappingmaster-controller.js",
                      "~/app/directive/common-directive.js",
                    "~/app/controller/user/user-service.js",
                     "~/app/controller/scanner/history-service.js",
                      "~/app/controller/scanner/scandetail-service.js",
                      "~/app/controller/scanner/map-controller.js",
                //load profile 
                    "~/app/controller/user/profile-controller.js",
                    //load restore 
                    "~/app/controller/restore/restoringhistory-controller.js",
                    "~/app/controller/restore/restoringhistory-service.js",
                    "~/app/controller/restore/restoringhistorydetail-controller.js",
                    "~/app/controller/restore/restoringhistorydetail-service.js",
                // load generate script 
                    "~/app/controller/generatescript/generatescript-controller.js",
                //load for seal
                    "~/app/controller/seal/seal-controller.js",
                    "~/app/controller/scanner/registeronlinescan-controller.js",
                    "~/app/controller/scanner/scanprogress-controller.js",
                    "~/app/controller/scanner/history-controller.js",
                     "~/app/controller/scanner/viewreportscan-controller.js",
                      "~/app/controller/scanner/viewscandetail-controller.js",
                      "~/app/controller/imagechecker/imagechecker-controller.js",
                      "~/app/controller/imagechecker/imagechecker-service.js",
                      "~/app/controller/vul/vul-controller.js",
                      "~/app/controller/vul/vul-service.js")
                      );

            bundles.Add(new ScriptBundle("~/bundles/appLogin").Include(
                    "~/app/ng-app-login.js",
                    "~/app/master-service.js",
                     "~/app/directive/common-directive.js",
                    //load for user
                    "~/app/controller/user/register-controller.js",
                    "~/app/controller/user/login-controller.js",
                    "~/app/controller/user/user-service.js"));

           
        }
    }
}