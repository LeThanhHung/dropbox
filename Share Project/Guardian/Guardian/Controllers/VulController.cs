﻿using Oas.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Oas.Infrastructure;
using System.IO;
using Oas.Infrastructure.Domain;
using System.Net;
using System.Threading.Tasks;
using System.Collections;

namespace Guardian.Controllers
{
    [Authorize]
    public class VulController : BaseController
    {
        //
        // GET: /Vul/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetVulHistoryByUser()
        {
            List<VulHistoryModel> list = new List<VulHistoryModel>();
            try
            {
                var id = this.User.Identity.GetUserId();
                if (id != null)
                {
                    var user = userService.Get(id);
                    if (user != null)
                    {
                        var listVulHistory = VulnerScanService.GetByUserId(user.Id);
                        if (listVulHistory != null && listVulHistory.Count > 0)
                        {
                            foreach (var item in listVulHistory)
                            {
                                VulHistoryModel m = new VulHistoryModel();
                                m.Id = item.Id;
                                m.Name = item.Name;
                                m.CreateDate = item.CreateDate;
                                m.GitPath = item.GitPath;
                                m.ReportPath = item.ReportPath;
                                m.IsCompleted = item.IsCompleted;
                                m.IsInQueue = item.IsInQueue;
                                m.IsScanning = item.IsScanning;
                                m.IsError = item.IsError;
                                list.Add(m);
                            }
                        }
                    }
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetVulDetail(string report_path)
        {
            List<VulDetail> list = new List<VulDetail>();
            try
            {
                if (!String.IsNullOrEmpty(report_path))
                {
                    list = VulnerabilityHandle.GetVulDetailScan(report_path);
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteVulDetail(Guid id)
        {

            if (id != Guid.Empty)
            {
                var item = VulnerScanService.Get(id);
                if (item != null)
                {
                    return Json(VulnerScanService.Remove(id), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddnewVulner(NewVulScan entity)
        {
            var currentUser = getCurrentUser();
            if (currentUser == null) { return Json(null, JsonRequestBehavior.AllowGet); }
            try
            {
                VulnerScan item = new VulnerScan();
                item.Id = Guid.NewGuid();
                item.IsScanning = true;
                item.IsCompleted = false;
                item.Name = entity.ScanName;
                item.IsInQueue = false;
                item.UserId = currentUser.Id;
                item.CreateDate = DateTime.Now.ToString();
                item.IsActive = true;
                VulnerScanService.Add(item);
                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ce)
            {
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> ExecuteVulnerScan(NewVulScan entity)
        {
            var currentUser = getCurrentUser();
            if (!HasPersmissionFTP())
            {
                throw new Exception("No permission on your FTP Folder.");
            }
            if (entity != null && currentUser != null)
            {
                //step 1 : get lastest source
                var histories = historyService.GetListHistoryModelByUserID(currentUser.Id);
                if (histories != null && histories.Count > 0)
                {
                    string formatPathFile = "ftp://" + currentUser.FtpAccount + ':' + currentUser.FtpPassword + "@{0}:{1}//";
                    string rootFolder = String.Format(formatPathFile, currentUser.FtpServer, currentUser.FtpPort);
                    Guid QueueId = Guid.NewGuid();
                    string pathLocalFolderRoot = Path.Combine(Server.MapPath("~/Upload/FTPUserFiles"), QueueId.ToString());
                    DirectoryInfo dirLocalfolder = Directory.CreateDirectory(pathLocalFolderRoot);
                    if (!dirLocalfolder.Exists) throw new Exception("Cannot create folder!");
                    string serverLocalFolder = String.Format("~/Upload/FTPUserFiles/{0}", QueueId.ToString());

                    //step 2 : get file and folder from ftp account 
                    List<ScanDetail> list = await scanService.GetAllFileOfFTP(currentUser.FtpServer, currentUser.FtpPort, currentUser.FtpAccount, currentUser.FtpPassword, rootFolder, true, "/", Guid.Empty, pathLocalFolderRoot, QueueId, serverLocalFolder);

                    if (list != null && list.Count > 0)
                    {
                        //path of zip file
                        string pathBackupZipFolder = Path.Combine(Server.MapPath("~/Upload/BackupZip"), String.Format("{0}.zip", QueueId.ToString()));
                        //execute zip folder
                        if (scanService.ZipCurrentSource(pathLocalFolderRoot, pathBackupZipFolder))
                        {
                            //upload zip file to ubuntu server where we are hosting 
                            var result = Oas.Infrastructure.VulnerabilityHandle.RequestHelper.PostMultipart(VulnerabilityHandle.RaptorUploadURL,
                            new Dictionary<string, object>() {  
                                 {"vul_id",entity.Id.ToString()},
                                { "file", new Oas.Infrastructure.VulnerabilityHandle.FormFile() { FilePath=pathBackupZipFolder , Name =String.Format("{0}.zip",QueueId.ToString()), ContentType = "application/zip" }}, 
                                {"scan_name",entity.ScanName},
                                {"usr",currentUser.UserName} 
                            });
                            return Json(true, JsonRequestBehavior.AllowGet);
                            //var item = VulnerScanService.Get(entity.Id);
                            //if (item == null)
                            //{
                            //    return Json(false, JsonRequestBehavior.AllowGet);
                            //}
                            //if (!String.IsNullOrEmpty(result) && !result.ToLower().Equals("failed"))
                            //{
                            //    item.IsCompleted = true;
                            //    item.IsScanning = false;
                            //    item.IsError = false;
                            //    item.ReportPath = result.Trim();

                            //    VulnerScanService.Update(item);
                            //    return Json(true, JsonRequestBehavior.AllowGet);
                            //}
                            //else
                            //{
                            //    item.IsCompleted = true;
                            //    item.IsScanning = false;
                            //    item.IsError = true;
                            //    item.ErrorMessage = "Occured error during execute your request";
                            //    VulnerScanService.Update(item);
                            //}
                        }
                    }

                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ServiceHeartBeat()
        { 
            return Json(VulnerabilityHandle.CheckHeartBeat(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult VulUploadFile(HttpPostedFileBase sheet)
        {
            if (Request.Files[0].ContentLength > 0)
            {
                sheet = Request.Files[0];
                string fileExt = Path.GetExtension(sheet.FileName);

                if (fileExt.ToLower().Equals(".png") || fileExt.ToLower().Equals(".jpg")
                    || fileExt.ToLower().Equals(".jpge") || fileExt.ToLower().Equals(".gif"))
                {
                    var fileName = String.Format("{0:MMddyyyyhhmmss}{1}", DateTime.Now, fileExt);
                    var path = Path.Combine(Server.MapPath("/Upload"), fileName);
                    sheet.SaveAs(path);
                    string pathReturn = ("/Upload/" + fileName);
                    return Json(pathReturn, JsonRequestBehavior.AllowGet);
                }
                else { throw new Exception("Extension is not support"); }
            }
            throw new Exception("File Invalid");
        }

        [HttpPost]
        public ActionResult CheckExistedPackage(string packageName)
        {
            var currentUser = getCurrentUser();
            if (currentUser != null)
            {
                var entity = VulnerScanService.GetByNameAndUser(packageName, currentUser.Id);
                return Json(entity == null, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}
