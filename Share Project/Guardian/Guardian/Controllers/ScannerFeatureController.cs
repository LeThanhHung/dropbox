﻿using Oas.Infrastructure;
using Oas.Infrastructure.Model;
using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using Oas.Infrastructure.Services;
using Oas.Infrastructure.Domain;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Linq;
using System.Threading;
using System.Transactions;
using Guardian.Models;
namespace Guardian.Controllers
{
    public class ScannerFeatureController : BaseController
    {

        public ScannerFeatureController()
        {

        }

        public ActionResult Index()
        {
            return View();
        }


        #region Private method


        private void ExecuteRescan(Guid scanDetailId, Guid scanResultId)
        {
            var scandetail = scanDetailService.Get(scanDetailId);
            if (scandetail != null)
            {
                if (!String.IsNullOrEmpty(scandetail.Data_Id) && !String.IsNullOrEmpty(scandetail.Rest_Ip))
                {
                    //request to metascan for get result again 
                    GetReportInfo(scandetail.Data_Id, scandetail.Rest_Ip, scandetail.FileName, scanResultId);
                }
            }
        }

        private void GetReportInfo(string data_id, string rest_ip, string fileName, Guid ScanResultId)
        {
            try
            {
                var listScanResultDetail = new List<ScanResultDetail>();
                string reportRaw = scanService.GetScanningFileInfo(rest_ip, data_id, fileName);
                if (!String.IsNullOrEmpty(reportRaw))
                {
                    dynamic reportInfo = JsonConvert.DeserializeObject<JObject>(reportRaw);
                    if (reportInfo != null)
                    {
                        dynamic scanresults = reportInfo.scan_results;
                        if (scanresults != null)
                        {
                            ScanResult resultInfo = scanResultService.Get(ScanResultId);
                            //get process  info    
                            resultInfo.Rescan_available = scanresults.rescan_available;
                            resultInfo.Scan_all_result_i = scanresults.scan_all_result_i;
                            resultInfo.Start_time = scanresults.start_time;
                            resultInfo.Total_time = scanresults.total_time;
                            resultInfo.Total_avs = scanresults.total_avs;
                            resultInfo.Progress_percentage = scanresults.progress_percentage;
                            resultInfo.In_queue = scanresults.in_queue;
                            resultInfo.Scan_all_result_a = scanresults.scan_all_result_a;
                            //update to db 
                            scanResultService.Update(resultInfo);
                            //get all scan detail
                            foreach (var prop in scanresults.scan_details)
                            {
                                ScanResultDetail detail = new ScanResultDetail();
                                detail.Id = Guid.NewGuid();
                                detail.ScanResultId = ScanResultId;
                                detail.AntiVirusName = prop.Name;

                                detail.ScanResultI = prop.Value.scan_result_i;
                                detail.ThreatFound = prop.Value.threat_found;
                                detail.DefTime = prop.Value.def_time;
                                detail.ScanTimes = prop.Value.scan_time;

                                listScanResultDetail.Add(detail);
                            }

                            //delete batch ScanResultDetail 
                            if (scanResultDetailService.DeleteBatch(ScanResultId, "ScanResultDetails", "ScanResultId"))
                            {
                                scanResultDetailService.BulkInsert(listScanResultDetail);
                            }
                        }
                    }
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
        }

        private string WaitScanComplete(string reportRaw, string rest_ip, string data_id, string filename)
        {
            if (!String.IsNullOrEmpty(reportRaw))
            {
                dynamic reportInfo = JsonConvert.DeserializeObject<JObject>(reportRaw);
                if (reportInfo != null)
                {
                    dynamic scanresults = reportInfo.scan_results;
                    if (scanresults != null && scanresults.progress_percentage < 100)
                    {
                        Thread.Sleep(5000);
                        reportRaw = scanService.GetScanningFileInfo(rest_ip, data_id, filename);
                        reportRaw = WaitScanComplete(reportRaw, rest_ip, data_id, filename);
                    }
                    else
                    {
                        return reportRaw;
                    }
                }
            }
            return reportRaw;
        }
        #endregion

        #region FTP function
        /// <summary>
        /// This is called when client click on  button Backup Now  or 
        /// create new a backup.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> BackupFiles(ImageChecker entity)
        {
            //step 0 : get current user
            var currentUser = getCurrentUser();
            ResponeResult resResult = new ResponeResult();
            if (!HasPersmissionFTP())
            {
                entity.IsDownloaded = false;
                entity.IsDownloading = false;
                //save to db to mark as Downloaded.
                imageCheckerService.Update(entity);
                TrackImageCheckerRestore(Guid.Empty, false, Guid.Parse(currentUser.Id), Guardian.Models.Action.Backup.ToString(), "No permission on your FTP Folder.");
                return Json(new ResponeResult() { IsSuccess = false, Data = false, ErrorMessage = "No permission on your FTP Folder." }, JsonRequestBehavior.AllowGet);
            }
            try
            {

                //step 1 :verify account aganit  from ftp account 
                if (currentUser != null)
                {
                    string formatPathFile = "ftp://" + currentUser.FtpAccount + ':' + currentUser.FtpPassword + "@{0}:{1}//";
                    string rootFolder = String.Format(formatPathFile, currentUser.FtpServer, currentUser.FtpPort);
                    Guid QueueId = Guid.NewGuid();
                    string pathLocalFolderRoot = Path.Combine(Server.MapPath("~/Upload/FTPUserFiles/ImageCheckerFiles"), QueueId.ToString());
                    DirectoryInfo dirLocalfolder = Directory.CreateDirectory(pathLocalFolderRoot);
                    if (!dirLocalfolder.Exists) throw new Exception("Cannot create folder!");
                    string serverLocalFolder = String.Format("~/Upload/FTPUserFiles/ImageCheckerFiles/{0}", QueueId.ToString());

                    //step 2 : get file and folder from ftp account 
                    List<ScanDetail> list = await scanService.BackupAllFileOfFTP(currentUser.FtpServer, currentUser.FtpPort, currentUser.FtpAccount, currentUser.FtpPassword, rootFolder, true, "/", Guid.Empty, pathLocalFolderRoot, QueueId, serverLocalFolder);
                    if (list != null && list.Count > 0)
                    {
                        // save all file to ImageCheckerDetail table 
                        List<ImageCheckerDetail> listImageCheckerDetails = new List<ImageCheckerDetail>();
                        list.ForEach(c =>
                        {
                            ImageCheckerDetail imgDetail = new ImageCheckerDetail();
                            imgDetail.Id = Guid.NewGuid();
                            imgDetail.CreatedDate = DateTime.Now;
                            imgDetail.ImageCheckerId = entity.Id;
                            imgDetail.PathLocalFile = c.MapFileLocal;
                            imgDetail.PathServerFile = c.PathFile;
                            imgDetail.FileName = c.FileName;
                            imgDetail.Volume = c.Volume;

                            imageCheckerDetailService.Add(imgDetail);
                        });
                    }
                    entity.IsDownloaded = true;
                    entity.IsDownloading = false;
                    entity.Location = serverLocalFolder;
                    entity.LastModify = DateTime.Now;
                    //save to db to mark as Downloaded.
                    imageCheckerService.Update(entity);
                    TrackImageCheckerRestore(Guid.Empty, true, Guid.Parse(currentUser.Id), Guardian.Models.Action.Backup.ToString(), "Backed up successful.");
                    return Json(new ResponeResult() { IsSuccess = true, Data = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
                TrackImageCheckerRestore(Guid.Empty, false, Guid.Parse(currentUser.Id), Guardian.Models.Action.Backup.ToString(), ce.Message);
                resResult.IsSuccess = false;
                resResult.ErrorMessage = ce.Message;
            }
            entity.IsDownloaded = false;
            entity.IsDownloading = false;
            //save to db to mark as Downloaded.
            imageCheckerService.Update(entity);
            return Json(resResult, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Verify account FTP 
        /// </summary>
        /// <param name="servername"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="port"></param>
        /// <param name="initalFolder"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult VerifyFtpAccount(string servername, string username, string password, int port, string initalFolder)
        {
            bool isHaspermission = false;
            try
            {
                isHaspermission = HasPersmissionFTP();
            }
            catch (Exception ce)
            {
                log.Error(ce);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(isHaspermission, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Execute scan a folder FTP
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> ExecuteScan(History entity)
        {
            DateTime time1 = DateTime.Now;
            var currentUser = getCurrentUser();
            try
            {
                //step 0 : get current user

                if (!HasPersmissionFTP())
                {

                    throw new Exception("No permission on your FTP Folder.");
                }
                if (entity != null)
                {

                    //step 1 :verify account aganit  from ftp account 
                    if (currentUser != null)
                    {

                        string formatPathFile = "ftp://" + currentUser.FtpAccount + ':' + currentUser.FtpPassword + "@{0}:{1}//";
                        string rootFolder = String.Format(formatPathFile, currentUser.FtpServer, currentUser.FtpPort);
                        Guid QueueId = Guid.NewGuid();
                        string pathLocalFolderRoot = Path.Combine(Server.MapPath("~/Upload/FTPUserFiles"), QueueId.ToString());
                        DirectoryInfo dirLocalfolder = Directory.CreateDirectory(pathLocalFolderRoot);
                        if (!dirLocalfolder.Exists) throw new Exception("Cannot create folder!");
                        string serverLocalFolder = String.Format("~/Upload/FTPUserFiles/{0}", QueueId.ToString());

                        //step 2 : get file and folder from ftp account 
                        List<ScanDetail> list = await scanService.GetAllFileOfFTP(currentUser.FtpServer, currentUser.FtpPort, currentUser.FtpAccount, currentUser.FtpPassword, rootFolder, true, "/", entity.Id, pathLocalFolderRoot, QueueId, serverLocalFolder);
                        //step 3 insert these path file to database 
                        bool isSuccess = scanDetailService.BulkInsert(list);


                        //step 4 : Upload  these file to scan  
                        List<ScanResult> listScanResult = new List<ScanResult>();
                        List<ScanResultDetail> listScanResultDetail = new List<ScanResultDetail>();
                        //order by list by volumne , priority for lightest file
                        list = list.OrderBy(c => Int32.Parse(c.Volume)).ToList();
                        int totaldefect = 0;
                        foreach (ScanDetail file in list)
                        {
                            string filePath = file.MapFileLocal;
                            //old code with scan physical file
                            string infoScanRaw = scanService.ScanFile(currentUser.FtpServer, currentUser.FtpPort, currentUser.FtpAccount, currentUser.FtpPassword, filePath);
                            //new code scan with ftp file 
                            //string infoScanRaw = scanService.ScanFtpFile(currentUser.FtpServer, currentUser.FtpPort, currentUser.FtpAccount, currentUser.FtpPassword, file.PathFile);

                            if (!String.IsNullOrEmpty(infoScanRaw))
                            {
                                dynamic jsonResult = JsonConvert.DeserializeObject<JObject>(infoScanRaw);
                                string data_id = string.Empty;
                                string rest_ip = string.Empty;
                                foreach (var prop in jsonResult.Properties())
                                {
                                    if (prop.Name == "data_id")
                                    {
                                        data_id = prop.Value;
                                    }
                                    else
                                    {
                                        rest_ip = prop.Value;
                                    }
                                }
                                file.IsScaned = true;
                                file.Data_Id = data_id;
                                file.Rest_Ip = rest_ip;
                                scanDetailService.Update(file);
                                Thread.Sleep(5000);
                                #region ScanResultInfo
                                //get file report  
                                string reportRaw = scanService.GetScanningFileInfo(rest_ip, data_id, file.FileName);
                                reportRaw = WaitScanComplete(reportRaw, rest_ip, data_id, file.FileName);
                                if (!String.IsNullOrEmpty(reportRaw))
                                {
                                    dynamic reportInfo = JsonConvert.DeserializeObject<JObject>(reportRaw);
                                    if (reportInfo != null)
                                    {
                                        dynamic scanresults = reportInfo.scan_results;
                                        if (scanresults != null)
                                        {
                                            ScanResult resultInfo = new ScanResult();
                                            resultInfo.Id = Guid.NewGuid();
                                            resultInfo.ScanDetailId = file.Id;
                                            resultInfo.Rescan_available = scanresults.rescan_available;
                                            resultInfo.Scan_all_result_i = scanresults.scan_all_result_i;
                                            resultInfo.Start_time = scanresults.start_time;
                                            resultInfo.Total_time = scanresults.total_time;
                                            resultInfo.Total_avs = scanresults.total_avs;
                                            resultInfo.Progress_percentage = scanresults.progress_percentage;
                                            resultInfo.In_queue = scanresults.in_queue;
                                            resultInfo.Scan_all_result_a = scanresults.scan_all_result_a;
                                            //fileInfo
                                            dynamic file_info = reportInfo.file_info;
                                            if (file_info != null)
                                            {
                                                resultInfo.MD5 = file_info.md5;
                                                resultInfo.Sha1 = file_info.sha1;
                                                resultInfo.Sha256 = file_info.sha256;
                                                resultInfo.FileTypeCategory = file_info.file_type_category;
                                                resultInfo.FileTypeDescription = file_info.file_type_description;
                                                resultInfo.FileTypeExtension = file_info.file_type_extension;
                                            }
                                            //insert to db ScanResult
                                            scanResultService.Add(resultInfo);
                                            //get all scan detail
                                            foreach (var prop in scanresults.scan_details)
                                            {
                                                ScanResultDetail detail = new ScanResultDetail();
                                                detail.Id = Guid.NewGuid();
                                                detail.ScanResultId = resultInfo.Id;
                                                detail.AntiVirusName = prop.Name;

                                                detail.ScanResultI = prop.Value.scan_result_i;
                                                totaldefect += detail.ScanResultI > 0 ? 1 : 0;
                                                detail.ThreatFound = prop.Value.threat_found;
                                                detail.DefTime = prop.Value.def_time;
                                                detail.ScanTimes = prop.Value.scan_time;

                                                listScanResultDetail.Add(detail);
                                            }


                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                        if (totaldefect > 0)
                        {
                            TrackImageCheckerRestore(Guid.Empty, true, Guid.Parse(currentUser.Id), Guardian.Models.Action.Scan.ToString(), String.Format("{0} are defected", totaldefect));
                        }
                        //insert result Info to db by BulkInsert v
                        //if (scanResultService.BulkInsert(listScanResult))
                        {
                            if (scanResultDetailService.BulkInsert(listScanResultDetail))
                            {
                                DateTime timeEnd = DateTime.Now;
                                TimeSpan timespanEnd = timeEnd.Subtract(time1);
                                var totalMinEnd = timespanEnd.Minutes;
                                //upadte history scanned 
                                entity.IsCompleted = true;
                                entity.IsScanning = false;
                                entity.TotalTimeScan = totalMinEnd;
                                historyService.Update(entity);
                                TrackImageCheckerRestore(Guid.Empty, true, Guid.Parse(currentUser.Id), Guardian.Models.Action.Scan.ToString(), "Scan Successful");
                                return Json(new ResponeResult() { IsSuccess = true, Data = true }, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }
                }
            }
            catch (Exception ce)
            {
                DateTime time2 = DateTime.Now;
                TimeSpan timespan = time2.Subtract(time1);
                var totalMin = timespan.Minutes;
                //upadte history scanned 
                entity.IsCompleted = false;
                entity.IsScanning = false;
                entity.TotalTimeScan = totalMin;
                historyService.Update(entity);
                log.Error(ce);
                scanService.RollbackResultScan(entity.Id);
                TrackImageCheckerRestore(Guid.Empty, false, Guid.Parse(currentUser.Id), Guardian.Models.Action.Scan.ToString(), ce.Message);
                //throw ce;
                return Json(new ResponeResult() { IsSuccess = false, Data = true, ErrorMessage = ce.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new ResponeResult() { IsSuccess = false, Data = true, ErrorMessage = "Occurred error during execute scan." }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Rescan a file 
        /// </summary>
        /// <param name="scanDetailId"></param>
        /// <param name="scanResultId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Rescan(Guid scanDetailId, Guid scanResultId)
        {
            //get ScanResult by Id 
            ExecuteRescan(scanDetailId, scanResultId);
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region history
        /// <summary>
        /// Add new History 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertHistory(History entity)
        {
            if (entity != null)
            {
                entity.Id = Guid.NewGuid();
                entity.CreatedDate = DateTime.Now;
                entity.CompletedDate = DateTime.Now;
                entity.IsCompleted = false;
                entity.UserId = this.User.Identity.GetUserId();
                historyService.Add(entity);
                return Json(entity, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update History
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateHistory(History entity)
        {
            if (entity != null)
            {
                entity.CompletedDate = DateTime.Now;
                entity.CreatedDate = DateTime.Now;
                historyService.Update(entity);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Delete History
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteHistory(History entity)
        {
            if (entity != null)
            {
                historyService.Remove(entity.Id);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteHistories(List<History> histories)
        {
            if (histories != null)
            {
                foreach (var entity in histories)
                {
                    historyService.Remove(entity.Id);
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get list History
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetListHistory()
        {
            var id = this.User.Identity.GetUserId();
            if (id != null)
            {
                var list = historyService.GetListHistoryModelByUserID(id);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Get Summanry report by History
        /// </summary>
        /// <param name="historyId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetViewReportById(Guid historyId)
        {
            var model = historyService.GetViewReportById(historyId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Detail of Scan by HistoryID
        /// </summary>
        /// <param name="historyId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetSummaryReportScanById(Guid historyId)
        {
            var model = historyService.GetSummaryScanById(historyId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Check Existed Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CheckExistName(string name)
        {
            var id = this.User.Identity.GetUserId();
            if (id != null)
            {
                bool isExist = historyService.CheckExistByName(name,id);
                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);

        }
        #endregion history

        #region scan detail
        /// <summary>
        /// Add New Scan Detail
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertScanDetail(ScanDetail entity)
        {
            if (entity != null)
            {
                entity.Id = Guid.NewGuid();
                scanDetailService.Add(entity);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Upadte ScanDetail
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateScanDetail(ScanDetail entity)
        {
            if (entity != null)
            {
                scanDetailService.Update(entity);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Delete Scan Detail
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteScanDetail(ScanDetail entity)
        {
            if (entity != null)
            {
                scanDetailService.Remove(entity.Id);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetListScanDetail()
        {
            var list = scanDetailService.Get();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion scan detail

        #region Scan Result Detail
        /// <summary>
        /// Get ScanResult by ScanDetailID
        /// </summary>
        /// <param name="scanresultid"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetScanResultDetailByParent(Guid scanresultid)
        {
            IList<ScanResultDetail> list = scanResultDetailService.Get();
            var listByParent = (from a in list
                                where a.ScanResultId == scanresultid
                                select a).OrderByDescending(c => c.ThreatFound).ToList();

            return Json(listByParent, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Scan Result by ID
        /// </summary>
        /// <param name="scanresultid"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetScanResultById(Guid scanresultid)
        {
            var scanresult = scanResultService.GetIncludeScanDetailById(scanresultid);
            return Json(scanresult, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
