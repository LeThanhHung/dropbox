﻿using Oas.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Guardian.Controllers
{
    public class ProfileController : BaseController
    {
        

        public ProfileController()   
        {
                   
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Profile()
        {
            return PartialView();
        }

    }
}
