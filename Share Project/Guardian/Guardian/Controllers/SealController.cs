﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Guardian.Controllers
{
    public class SealController : BaseController
    {
        //
        // GET: /Seal/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SealInfo()
        {
            return PartialView();
        }
    }
}
