﻿var ProfileController = function ($http, $scope, users) {
    $scope.myprofile;
    $scope.Submitted = false;
    $scope.$parent.IsProfile = true;
    $scope.Contributions = [{ Value: '', Name: 'Selecione Contribuinte...' }, { Value: '0', Name: 'Não' }, { Value: '1', Name: 'Sim' }];
    $scope.option = {
        url: '/Vul/VulUploadFile',
        acceptFileTypes: /(\.|\/)(png|gif|jpg)$/i,
        dataType: 'json',
        autoUpload: true,
        maxFileSize: 10000000,
        formData: {},
        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) { 
            var jqXHR = data.submit();


        },
        success: function (result, textStatus, jqXHR) {
            if (result != undefined && result != null) {
                if ($scope.myprofile != undefined) {
                    $scope.myprofile.PathLogo = result;
                    $scope.$apply();
                }
            }
            
        },
        fail: function (e, data) {
            // Something has gone wrong!
            $scope.$parent.ShowNotification('error', 'Upload Image Profile', 'Occurred error during uploading image.', 'tr');
             
        }

    }
    $scope.Save = function (valid) { 
        $scope.Submitted = true;
        if (valid) {
            //save to db 
            users.edit($scope.myprofile).then(function (data) {
                if (data) {
                    $scope.$parent.ShowNotification('info', 'Update Profile', 'Updated your profile success!', 'tr');
                }
                else {
                    $scope.$parent.ShowNotification('error', 'Update Profile', 'Occurred error during create new user, please contact to administrator for supporting.', 'tr'); 
                }
            })

        }
    }
    //get currentt user 
    users.getCurrentUser().then(function (result) {
        if (result != undefined)
        {
            result.Contribuinte = result.Contribuinte ? "1" : "0";
        }
        console.log(result);
        $scope.myprofile = result;
        $scope.$parent.currentuserapp = result;
    })

}