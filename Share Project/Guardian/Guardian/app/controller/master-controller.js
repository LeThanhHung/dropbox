﻿var MasterController = function ($scope, $http, users,vuls, signalRHubProxy, $interval) {
    $scope.IsProfile = false;
    $scope.currentuserapp;
    $scope.IsChange = false;
    $scope.IsRunning = false;
    $scope.Notifications;
    $scope.NumberUnread = 0;


    //get current user
    users.getCurrentUser().then(function (result) {
        $scope.currentuserapp = result;
    });
    //generate client proxy
    var clientPushHubProxy = signalRHubProxy(signalRHubProxy.defaultServer, 'serviceHub', { logging: true });
    $.connection.hub.start().done(function () {
        // $interval($scope.RunTimer, 2000);
    });

    $scope.RunTimer = function () {
        if ($scope.currentuserapp != undefined) {
            if (!$scope.IsRunning) {
                //call server to execute rescan 
                //clientPushHubProxy.proxy.server.executeRescanByUserID($scope.currentuserapp.Id, $.connection.hub.id);
                $scope.IsRunning = true;
            }
        }
    };

    
    $scope.ShowNotification = function (style, title, message, position) {
        if (style == "error") {
            icon = "fa fa-exclamation";
        } else if (style == "warning") {
            icon = "fa fa-warning";
        } else if (style == "success") {
            icon = "fa fa-check";
        } else if (style == "info") {
            icon = "fa fa-question";
        } else {
            icon = "fa fa-circle-o";
        }
        $.notify({
            title: title,
            text: message,
            image: "<i class='" + icon + "'></i>"
        }, {
            style: 'metro',
            className: style,
            globalPosition: position,
            showAnimation: "show",
            showDuration: 0,
            hideDuration: 0,
            autoHideDelay: 3000,
            autoHide: true,
            clickToHide: true
        });

    }

    //clientPushHubProxy.proxy.client.ReceiveNotificateRescanResult = function (userId, action)
    //{
    //    if ($scope.currentuserapp != undefined && userId === $scope.currentuserapp.Id)
    //    {
    //        if (action == 'HasChange') {
    //            $scope.IsChange = !$scope.IsChange;
    //        }
    //    } 
    //    $scope.IsRunning = false;
    //}

    $scope.GotoSeal = function () { window.location.href = "/Home/Index#/Home/GenerateScript" }
    $scope.GotoSetting = function () { window.location.href = "/Home/Index#/Home/RegisterOnlineScan" }
    $scope.GotoScan = function () { window.location.href = "/Home/Index#/Home/ScanProgress" }
    $scope.GotoBackup = function () { window.location.href = "/Home/Index#/Home/ImageChecker" }
    $scope.GotoRestoreHistory = function () { window.location.href = "/Home/Index#/Home/RestoreHistoryView"; }
    $scope.Logout = function () {
        users.logoff().then(function (result) {
            if (result) {
                window.location.href = "/";
            }
            else {
                alert("Desculpe! Ocorreu erro durante log off sua conta .. , por favor, tente novamente.");
            }
        });
    }

    //convert time to calculate time ago 
    $scope.ConvertTimeToStatusTime = function (time) {

        switch (typeof time) {
            case 'number': break;
            case 'string': time = +new Date(time); break;
            case 'object': if (time.constructor === Date) time = time.getTime(); break;
            default: time = +new Date();
        }
        var time_formats = [
            [60, 'seconds', 1], // 60
            [120, '1 minute ago', '1 minute from now'], // 60*2
            [3600, 'minutes', 60], // 60*60, 60
            [7200, '1 hour ago', '1 hour from now'], // 60*60*2
            [86400, 'hours', 3600], // 60*60*24, 60*60
            [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
            [604800, 'days', 86400], // 60*60*24*7, 60*60*24
            [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
            [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
            [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
            [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
            [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
            [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
            [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
            [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
        ];
        var seconds = (+new Date() - time) / 1000,
            token = 'ago', list_choice = 1;

        if (seconds == 0) {
            return 'Just now'
        }
        if (seconds < 0) {
            seconds = Math.abs(seconds);
            token = 'from now';
            list_choice = 2;
        }
        var i = 0, format;
        while (format = time_formats[i++])
            if (seconds < format[0]) {
                if (typeof format[2] == 'string')
                    return format[list_choice];
                else
                    return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
            }
        return time;
    }

    //count number of notification what is marked as unread
    $scope.CountUnread = function () {
        $scope.NumberUnread = 0;
        if ($scope.Notifications != undefined && $scope.Notifications.length > 0) {
            for (var i = 0 ; i < $scope.Notifications.length; i++) {
                $scope.NumberUnread += !$scope.Notifications[i].IsRead ? 1 : 0;
            }
        }
    }

    //load all notification of a user
    $scope.LoadListNotification = function () {
        users.getListNotification().then(function (result) {
            if (result.IsSuccess) {
                $scope.Notifications = result.Data;
                $scope.CountUnread();
            }
        });
    }

    //mark a notification is read
    $scope.MarkIsRead = function (imagecheckrestore) {
        imagecheckrestore.IsRead = true;
        users.updateIsReadNotification(imagecheckrestore).then(function (result) { });
    }

    //clear all notifications
    $scope.ClearAll = function () {
        users.clearNotifications().then(function (result) {
            if (result.IsSuccess)
            { $scope.LoadListNotification(); }
        });
    }

    //View All
    $scope.ViewAll = function () {

    }

    //run load notification 
    $scope.LoadListNotification();
}



app.factory('signalRHubProxy', ['$rootScope', '$http',
    function ($rootScope, $http) {
        function signalRHubProxyFactory(serverUrl, hubName, startOptions) {
            var proxy = $.connection.serviceHub;
            return {
                proxy: proxy
            };
        };

        return signalRHubProxyFactory;
    }]);