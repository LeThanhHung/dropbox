﻿app.factory('vuls', ['$http', '$q', function ($http, $q) {
    return {
        list: function () {
            var path = '/Vul/GetVulHistoryByUser';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        add:function(entity)
        {
            var path = '/Vul/AddnewVulner';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        edit: function (report_path) {
            var path = '/Vul/GetVulDetail';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'report_path': report_path },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        del: function (id) {
            var path = '/Vul/DeleteVulDetail';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'id': id },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        scan: function (entity)
        {
            
            var path = '/Vul/ExecuteVulnerScan';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        heartbeat: function () {

            var path = '/Vul/ServiceHeartBeat';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path, 
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },

    }
}]);