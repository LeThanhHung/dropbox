﻿var NewVulController = function ($http, $scope, vuls, users, $modal, masters, signalRHubProxy) {
    $scope.currentuser;
    $scope.entity = {IsGit:true};
    $scope.IsGit = true;
    $scope.ChangeTab = function (isHome) {
        if (isHome) {
            $('#demo5-home').attr("class", "tab-pane fade active in");
            $('#demo5-profile').attr("class", "tab-pane fade");
            $scope.IsGit = true;
            $scope.entity.IsGit = true;
        } else {
            $('#demo5-profile').attr("class", "tab-pane fade active in");
            $('#demo5-home').attr("class", "tab-pane fade");
            $scope.IsGit = false;
            $scope.entity.IsGit=false
        }
    }
   
    $scope.option = {
        url: 'http://108.161.136.165/raptor/uploads/upload_file.php',
        acceptFileTypes: /(\.|\/)(zip)$/i,
        dataType: 'json',
        autoUpload: true,
        maxFileSize: 10000000,
        formData: {},
        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {
            // Automatically upload the file once it is added to the queue
            //disable scan button 
            $("#btnScanUpload").prop('disabled', true);
            var jqXHR = data.submit();


        },
        success: function (result, textStatus, jqXHR) {
            if (result != undefined && result != null) {

            }
            $("#btnScanUpload").prop('disabled', false);
        },
        fail: function (e, data) {
            // Something has gone wrong!
            alert("Occured error during upload file.Please try again!");

            $("#btnScanUpload").prop('disabled', false);
        }

    }

    $scope.Scan = function (isvalid) {
        $scope.Submitted = true;
        if (isvalid) { 
            //need to create vulnerscan for first before we start scan 
            vuls.add($scope.entity).then(function (resultaddnew) {
                if (resultaddnew != undefined) {
                    $scope.entity.Id = resultaddnew.Id;
                    vuls.scan($scope.entity).then(function (result) {
                       
                    });
                    window.location.href = "/Home/Index#/Home/Vulner";
                }
                else { alert("Occurred error during we execute your request, please contact to administrator for supportting.");}
            }) 
        }
    }

    $scope.Cancel = function () {
        window.location.href = "/Home/Index#/Home/Vulner";
    }
}

var VulController = function ($http, $scope, vuls, users, $modal, masters, signalRHubProxy, $interval) {
    $scope.currentuser;
    $scope.vullist;
    $scope.ServiceHeartBeat = false;
    $scope.promiseService;
    InitTable();
    loadlist();
    $scope.startInterval = function ()
    {
        $scope.promiseService = $interval(CheckServiceHeartBeat, 5000);
    }

    $scope.stopInterval = function ()
    {
        $interval.cancel($scope.promiseService);
    }
    //get interval for check service heartbeat
    $scope.startInterval();
    function CheckServiceHeartBeat() {
        $scope.stopInterval();
        vuls.heartbeat().then(function (result) {
            $scope.ServiceHeartBeat =  result==="true";
            if (!$scope.ServiceHeartBeat) {
                $scope.$parent.ShowNotification('error', 'Service Unavaliable', 'Currently, this service cannot execute your request, please back later.', 'b,r');
            }
            //start again
            $scope.startInterval();
        });
    }
    $scope.view = function (report_path) {
        window.location.href = "/Home/Index#/Home/Issues?report_path=" + report_path;
    }

    $scope.remove = function (id) {
        if (confirm("Do you want to remove this item?")) {
            vuls.del(id).then(function (result)
            {
                loadlist();
                if (result == "true") {
                  
                } else {
                        alert("Occurred error during remove this item.Please contact administrator for support.")
                }
            })
        }
    }

    $scope.add = function () {
        window.location.href = "/Home/Index#/Home/NewVulnerScan";
    }



    

    //intital the table result
    function InitTable() {
        $scope.aoColumns = [
            {
                "sTitle": "Name",
                "sClass": "col-lg-2",
                "mRender": function (data, type, oObj) {
                    returnValue = data;
                    return returnValue;
                }
            }, 

            {
                "sTitle": "Create Date",
                "sClass": "col-lg-3",
            },
             {
                 "sTitle": "Status",
                 "sClass": "col-lg-3",
                 "mRender": function (data, type, oObj)
                 {
                     var returnValue = "";
                     if (oObj != undefined)
                     {
                         if (oObj.IsScanning) {
                             returnValue = oObj.IsScanning ? " <span class=\"label label-warning\">Carregando</span>  <img id=\"" + oObj.Id + "\"   src=\"/Themes/img/loading.gif\" >" : "";
                         }
                         if (oObj.IsError)
                         {
                             returnValue = "<span class=\"label label-danger\">Error</span>";
                         }
                         else if (oObj.IsCompleted) {
                             returnValue = "<span class=\"label label-success\">Concluído</span>";
                         }
                     }
                    
                     return returnValue;
                 }
             }, 
            {
                "sTitle": "Action",
                "sClass": "col-lg-3",
                "mRender": function (data, type, oObj) {
                    var action = "";
                    if (oObj.IsCompleted && !oObj.IsError) {
                        action = "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-primary\" onclick=\"angular.element(this).scope().view('" + data + "')\"><i class=\"glyphicon glyphicon-zoom-in\"></i> View</button> ";
                     }
                    action += "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-danger\" onclick=\"angular.element(this).scope().remove('" + oObj.Id + "')\"><i class=\"glyphicon glyphicon-trash\"></i> Delete</button>";
                    return action;
                }
            }
        ];

        $scope.aoColumnDefs = [
                                { "mDataProp": "Name", "aTargets": [0] }, 
                                { "mDataProp": "CreateDate", "aTargets": [1] },
                                { "mDataProp": "CreateDate", "aTargets": [2] },
                                { "mDataProp": "ReportPath", "aTargets": [3] },
        ];
        //end config
    }
    //load list history to show to table
    function loadlist() {
        //load list history
        vuls.list().then(function (result) {
            if (result != undefined && result.length > 0) {
                $scope.vullist = result;
            }
        });
    }
}

var VulIssueController = function ($http, $scope, vuls, users, $modal, masters, signalRHubProxy, $routeParams) {
    $scope.issueslist;
    $scope.report_path = $routeParams.report_path;
    loadlist();
    InitTable();
    //intital the table result
    function InitTable() {
        $scope.aoColumns = [
            {
                "sTitle": "Rule ID",
                "sClass": "col-sm-1",
            },
             {
                 "sTitle": "Type",
                 "sClass": "col-sm-1",
             },

            {
                "sTitle": "File",
                "sClass": "col-sm-3",
            },
                {
                    "sTitle": "Description",
                    "sClass": "col-sm-3",
                },
                {
                    "sTitle": "Snippet",
                    "sClass": "col-sm-2",
                },
                {
                    "sTitle": "Plugin",
                    "sClass": "col-sm-2",
                },
                {
                    "sTitle": "Severity",
                    "sClass": "col-sm-3",
                },
                {
                    "sTitle": "Reference",
                    "sClass": "col-sm-3",
                },
                 {
                     "sTitle": "Location",
                     "sClass": "col-sm-3",
                 },
                  {
                      "sTitle": "UserInput",
                      "sClass": "col-sm-3",
                  },
            {
                "sTitle": "RenderPath",
                "sClass": "col-sm-3",
            }
        ];

        $scope.aoColumnDefs = [
                                { "mDataProp": "RuleID", "aTargets": [0] },
                                { "mDataProp": "Type", "aTargets": [1] },
                                { "mDataProp": "File", "aTargets": [2] },
                                { "mDataProp": "Description", "aTargets": [3] },
                                  { "mDataProp": "Code", "aTargets": [4] },

                                { "mDataProp": "Plugin", "aTargets": [5] },
                                { "mDataProp": "Severity", "aTargets": [6] },
                                { "mDataProp": "Reference", "aTargets": [7] },
                                 { "mDataProp": "Location", "aTargets": [8] },
                                { "mDataProp": "UserInput", "aTargets": [9] },
                                { "mDataProp": "RenderPath", "aTargets": [10] },

        ];
        //end config
    }
    //load list history to show to table
    function loadlist() {
        //load list history
        vuls.edit($scope.report_path).then(function (result) {
            if (result != undefined && result.length > 0) {
                $scope.issueslist = result;
            }
        });
    }
}