﻿var ViewDetailScanController = function ($scope, $http, $rootScope, users, histories, $route, $routeParams) {
    $scope.$routeParams = $routeParams;
    $scope.fileId = $routeParams.fileId;
    $scope.parentId = $routeParams.parentId;
    $scope.filename = $routeParams.filename;
    $scope.filesize = $routeParams.filesize;
    $scope.scanallresult = $routeParams.scanallresult != undefined && $routeParams.scanallresult != '' ? $routeParams.scanallresult : "Na Fila";
    $scope.defecttext;
    $scope.listscanresultdetail;
    $scope.scanresult;
    $scope.TotalVirus = 0;
    $scope.$parent.IsProfile = false;
    $scope.SearchValue = "";

    histories.getScanResultDetails($scope.fileId).then(function (result) {
        if (result != undefined) {
            $scope.listscanresultdetail = result;
            var totalDefect = 0;
            for (var i = 0 ; i < result.length; i++) {
                if (result[i].ThreatFound.trim().length>0)
                    totalDefect += 1;
            }
            $scope.defecttext = totalDefect + '/' + result.length;
            $scope.TotalVirus = totalDefect;
        }
    });

    histories.getScanResultById($scope.fileId).then(function (result) {
        if (result != undefined) {
            $scope.scanresult = result;
        }
    });

    $scope.Cancel = function () {
        window.location.href = "/Home/Index#/Home/ViewReportScan/" + $scope.parentId;
    }

    InitTable();
    function InitTable() {
        $scope.aoColumns = [
            {
                "sTitle": "Motor",
                "sClass": "col-md-2",
                "mRender": function (data, type, obj) {
                    return "<span class=\"pd-l-sm\"></span>" + data;
                }
            },
            {
                "sTitle": "Resposta",
                "sClass": "col-md-1",
                "mRender": function (data, type, oObj) {
                    var returnValue = data + " ms";
                    return returnValue;
                }
            },
             {
                 "sTitle": "Última Atualização",
                 "sClass": "col-md-1",
             },
             {
                 "sTitle": "Resultado",
                 "sClass": "col-md-2",
                 "mRender": function (data, type, oObj) {
                     if (data == '') {
                         return " <span class=\"badge bg-success\"><i class=\"fa fa-check-circle\"></i></span>";
                     }
                     return "  <span class=\"badge bg-danger\"><i class=\"fa fa-bug\"></i> </span>  <span class=\"text-danger\">" + data + "</span>";
                 }
             }
        ];

        $scope.aoColumnDefs = [
                                { "mDataProp": "AntiVirusName", "aTargets": [0] },
                                { "mDataProp": "ScanTimes", "aTargets": [1] },
                                 { "mDataProp": "DefTime", "aTargets": [2] },
                                { "mDataProp": "ThreatFound", "aTargets": [3] },
        ];
        //end config
    }

    $scope.ViewDetail = function (idfile) {
        window.location.href = "/Home/Index#/Home/ViewDetailScan/" + idfile;
    }

};