﻿var ImageCheckerController = function ($http, $scope, imagecheckers, users, $modal, masters, signalRHubProxy, imagecheckerhistories) {
    $scope.currentuser;
    $scope.imagecheckerlist;
    $scope.imagecheckerrestorelist;
    $scope.IsShowCheckerButton = false;
    $scope.IsVisible = false;
    $scope.IsRestore = false;
    $scope.process = {}; $scope.$parent.IsProfile = false;
    $scope.SearchValue = "";
    //inital signup
    var clientPushHubProxy = signalRHubProxy(signalRHubProxy.defaultServer, 'serviceHub', { logging: true });
    //initial table
    InitTable();
    loadlist();
    loadlistrestore();
    //get current user
    users.getCurrentUser().then(function (result) {
        $scope.currentuser = result;
        VerifyAccount(result);      
    });

    $scope.AddChecker = function () {
        var modalInstance = $modal.open({
            templateUrl: '/Home/AddNewImageChecker',
            controller: 'ImageCheckerModalController'
        });
        //modal opened
        modalInstance.opened.then(function () {
            //ComponentsFormTools.init();
        });
        //reload list
        modalInstance.result.then(function (result1) {
            if (result1 != undefined) {
                var entityId = result1.Id;
                result1.IsDownloaded = false;
                result1.IsDownloading = true;
                //reload list History 
                loadlist();
                $("#site_statistics_loading").remove();
                imagecheckers.edit(result1).then(function (result2) {
                    loadlist();
                    imagecheckers.backup(result1).then(function (result2) {
                        loadlist();
                        $("body").append("<div id='site_statistics_loading' style='display: none;'>   <img alt='loading' src=\"/Themes/img/loading.gif\" style=\"position: absolute; left: 50%; top: 50%;\"></div>");
                    });
                }); 
            }
        });
    }

    $scope.BackupNow = function (id) {
        if (confirm("Você quer reescrever o backup existente ?")) {
            var u = $.grep($scope.imagecheckerlist, function (e) { return e.Id == id; });
            var entity = u[0];
            if (entity != undefined) {
                //update status
                entity.IsDownloaded = false;
                entity.IsDownloading = true;
                //remove this load =  
                $("#site_statistics_loading").remove();
                imagecheckers.edit(entity).then(function (result) {
                    loadlist();
                    imagecheckers.backup(entity).then(function (result1) {
                        if (!result1.IsSuccess)
                        { alert(result1.ErrorMessage);}
                        loadlist();
                        //load notification
                        $scope.$parent.LoadListNotification();
                        $("body").append("<div id='site_statistics_loading' style='display: none;'>   <img alt='loading' src=\"/Themes/img/loading.gif\" style=\"position: absolute; left: 50%; top: 50%;\"></div>");
                    });
                });
            }
        }
    }

    $scope.StopNow = function (id)
    {
        if (confirm("Deseja parar esta ação?")) {
            var u = $.grep($scope.imagecheckerlist, function (e) { return e.Id == id; });
            var entity = u[0];
            if (entity != undefined) {
                //update status
                entity.IsStop = true;  
                imagecheckers.edit(entity).then(function (result) {
                    loadlist(); 
                });
            }
        }
    }

    $scope.Restore = function ()
    {
        if (confirm("Deseja restaurar?")) {
            if ($scope.currentuser != undefined) {
                $("#site_statistics_loading").remove();
                imagecheckers.changeStatusLastestRestoreVersion(true).then(function (sr) {
                    if (sr.IsSuccess) {
                        //load list restoring again.
                        loadlist();
                        imagecheckers.restore().then(function (result) {      
                            loadlist();
                            //load notification
                            $scope.$parent.LoadListNotification();
                            $("body").append("<div id='site_statistics_loading' style='display: none;'>   <img alt='loading' src=\"/Themes/img/loading.gif\" style=\"position: absolute; left: 50%; top: 50%;\"></div>");
                        });
                    }
                    else
                    {
                        alert(sr.ErrorMessage);
                    }
                });
                
            }
            else {
                alert("Ocorreu um erro durante o processo de restauração, tente novamente mais tarde.");
            }
        }
    }

    $scope.RestoreHistory = function ()
    {
        window.location.href = "/Home/Index#/Home/RestoreHistoryView";
    }

    clientPushHubProxy.proxy.client.ReceiveNotificateRestoreResult = function (model) {
        $scope.process = model;
        $scope.IsRestore = !model.IsCompleted;
        $scope.$apply();
    }

    $scope.StartNow = function (id)
    {
        var u = $.grep($scope.imagecheckerlist, function (e) { return e.Id == id; });
        var entity = u[0];
        if (entity != undefined) {
            //update status
            entity.IsStop = false;
            imagecheckers.edit(entity).then(function (result) {
                loadlist();
            });
        }
    }

    //intital the table result
    function InitTable() {
        $scope.aoColumns = [
            {
                "sTitle": "Execução",
                "sClass": "col-lg-2",
                "mRender": function (data, type, oObj) {
                    returnValue = data == 1 ? "Monthly" : "Weekly";
                    return returnValue;
                }
            },
             {
                 "sTitle": "Hora",
                 "sClass": "col-lg-2",
             },
              {
                  "sTitle": "Estado",
                  "sClass": "col-lg-2",
                  "mRender": function (data, type, oObj) {
                      var returnValue = data ? " <span class=\"label label-success\">Conluído</span>" : "";
                      if (oObj.IsDownloading) {
                          returnValue = " <span class=\"label label-warning\">Carregando</span>  <img   src=\"/Themes/img/loading.gif\" >"
                      }
                      else if (oObj.IsRestore)
                      {
                          returnValue = " <span class=\"label label-warning\">Restaurando</span>  <img   src=\"/Themes/img/loading.gif\" >"
                      }
                      return returnValue;
                  }
              },
            {
                "sTitle": "Conclusão",
                "sClass": "col-lg-3",
            },

            {
                "sTitle": "Backup",
                "sClass": "col-lg-3",
                "mRender": function (data, type, oObj) {
                    var action ="";
                    if (!oObj.IsStop) {
                        if (!oObj.IsDownloading) {
                            action = "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-primary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().StopNow('" + data + "')\"><i class=\"ti ti-control-stop\"></i> Cancelar</button> ";
                             action += "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-primary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().BackupNow('" + data + "')\"><i class=\"ti-search\"></i> Novo Backup</button>";
                        }
                        else {
                            action = "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-primary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().StopNow('" + data + "')\" disabled=\"disabled\"><i class=\"ti ti-control-stop\"></i> Cancelar</button> ";
                            action += "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-primary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().BackupNow('" + data + "')\" disabled=\"disabled\"><i class=\"ti-search\"></i> Novo Backup</button>";
                        }
                    }
                    else {
                        action = "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-primary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().StartNow('" + data + "')\"><i class=\"ti-control-play\"></i> Iniciar</button>";
                    }
                    
                    return action;
                }
            }
        ];

        $scope.aoColumnDefs = [
                                { "mDataProp": "RunInMothOrWeek", "aTargets": [0] },
                                { "mDataProp": "StartTime", "aTargets": [1] },
                                { "mDataProp": "IsDownloaded", "aTargets": [2] },
                                { "mDataProp": "CreatedDate", "aTargets": [3] },
                                { "mDataProp": "Id", "aTargets": [4] }
        ];
        //end config
    }
    //load list history to show to table
    function loadlist() {
        //load list history
        imagecheckers.list().then(function (result) {
            if (result != undefined && result.length > 0) {
                $scope.imagecheckerlist = result;
            }
            $scope.IsShowCheckerButton = $scope.imagecheckerlist == undefined || ($scope.imagecheckerlist != undefined && $scope.imagecheckerlist.length <= 0);
        });
        //verify account
        VerifyTypeOfAccount();
    }
    //load list restore history 
    function loadlistrestore() {
        //load list history
        imagecheckerhistories.list().then(function (result) {
            if (result != undefined && result.length > 0) {
                $scope.imagecheckerrestorelist = result;
            }
        });
        //verify account
        VerifyTypeOfAccount();
    }
    //verifi ftp account
    function VerifyAccount(currentUser) {
        if (currentUser == undefined && (currentUser != undefined && !currentUser.IsVerifiedFtpAccount)) {
            alert('Conta inválida, tente novamente.');
            window.location.href = "/Home/Index#/Home/RegisterOnlineScan"
        }
    }             
    //verify Type of Account
    function VerifyTypeOfAccount()
    {
        imagecheckers.verifyscope().then(function (result) {
            $scope.IsVisible = result == "true";
        })
    }
}

//this controller apply to modal create new scan
var ImageCheckerModalController = function ($scope, $http, $modalInstance, imagecheckers, masters) {
    $scope.entity = { "RunInMothOrWeek": 2 };
    $scope.Submitted = false;
    $scope.TimeToRun = masters.gettimetorun();
    $scope.Inproccess = false;
    //save the new scan schedule
    $scope.Save = function (isvalid) {
        $scope.Submitted = true;
        if (isvalid) {
            $scope.Inproccess = true;
            imagecheckers.insert($scope.entity).then(function (result) {
                if (result != undefined && result != "false") {
                    $modalInstance.close(result);
                }
                else {
                    alert("Ocorreu erro durante a digitalização criar uma lista de discussão , por favor, feche o formulário e criar novamente ou administrador de contato para suporte.");
                }
                $scope.Inproccess = false;
            });
        }
    }

    $scope.Cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.ChangeRun = function ()
    {
        $scope.Submitted = false;
        $scope.entity.StartTime = '';
    }
}
 
//validate Run at and  time to run 
app.directive('validExistBackup', function ($http, $resource, $timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            var stop_timeout;
            return scope.$watch(function () {
                return ngModel.$modelValue;
            }, function (name) {
                //$timeout.cancel(stop_timeout);

                if (name === '')
                    return ngModel.$setValidity('validexistbackup', true);
                    ngModel.$setValidity('validexistbackup', false);
                    //validate exist email
                    $http({
                        method: 'POST',
                        url: '/ImageChecker/CheckExistBackup',
                        data: { runin: scope.entity.RunInMothOrWeek, timetorun: name },
                        async: false,
                    }).success(function (data, status, headers, config) {
                        //stop_timeout = $timeout(function () {
                            return ngModel.$setValidity('validexistbackup', data === 'true');
                        //})
                    }).error(function (data, status, headers, config) {
                        return ngModel.$setValidity('validexistbackup', false);
                    });
                
            });
        }
    }
});

 