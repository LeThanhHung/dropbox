﻿app.factory('imagecheckers', ['$http', '$q', function ($http, $q) {
    return { 
        list: function () {
            var path = '/ImageChecker/GetImageChecker';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        insert: function (entity) {
            var path = '/ImageChecker/InsertImageChecker';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        edit: function (entity) {
            var path = '/ImageChecker/UpdateImageChecker';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        del: function (entity) {
            var path = '/ImageChecker/DelImageChecker';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        backup: function (entity)
        {
            var path = '/ScannerFeature/BackupFiles';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        verifyscope: function () {
            var path = '/ImageChecker/VerifyScope';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path, 
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        restore: function ()
        {
            var path = '/ImageChecker/Restore';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        changeStatusLastestRestoreVersion: function (status)
        {
            var path = '/ImageChecker/ChangeStatusLastestRestoreVersion';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data:{status:status},
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        }
    }
}]);
 