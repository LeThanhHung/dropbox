﻿var app = angular.module('GuardianLogin', ['ui.bootstrap', 'ngRoute', 'ngResource']);

var regexIso8601 = /\/Date\((.*?)\)\//gi;
function convertDateStringsToDates(input) {
    // Ignore things that aren't objects.
    if (typeof input !== "object") return input;

    for (var key in input) {
        if (!input.hasOwnProperty(key)) continue;

        var value = input[key];
        var match;
        // Check for string properties which look like dates.
        // TODO: Improve this regex to better match ISO 8601 date strings.
        if (typeof value === "string" && (match = value.match(regexIso8601))) {
            // Assume that Date.parse can parse ISO 8601 strings, or has been shimmed in older browsers to do so.
            var parsedDate = new Date(parseInt(value.substr(6)));

            var jsDate = new Date(parsedDate);
            input[key] = pad((jsDate.getMonth() + 1)) + '/' + pad(jsDate.getDate()) + '/' + pad(jsDate.getFullYear());

        } else if (typeof value === "object") {
            // Recurse into object
            convertDateStringsToDates(value);
        }
    }
}
function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}
//app.config(function (blockUIConfigProvider) {
//    // Change the default overlay message
//    blockUIConfigProvider.message('Please wait, your request is processing......!');
//});
app.config(function ($httpProvider) {
    $httpProvider.responseInterceptors.push('myHttpInterceptor');

    var spinnerFunction = function (data, headersGetter, $rootScope) {
        // todo start the spinner here
        var loading = "#site_statistics_loading";
        //$(loading).show();
        return data;
    };
    //show loading 
    $httpProvider.defaults.transformRequest.push(spinnerFunction);
    //convert datetime json to normal datetime to display on form
    $httpProvider.defaults.transformResponse.push(function (responseData) {
        convertDateStringsToDates(responseData);
        return responseData;
    });
    //check session timeout
    $httpProvider.interceptors.push(['$injector',
    function ($injector) {
        return $injector.get('AuthInterceptor');
    }]);

});
  app.factory('AuthInterceptor', function ($q) {
    return {
        responseError: function (response) {
            if (response.status === 401) {
                //The user is not logged in
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated,
                                      response);
            }
            if (response.status === 403) {
                //The user is logged in but isn’t allowed access
                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized,
                                      response);
            }
            if (response.status === 419 || response.status === 440) {
                //Session has expired
                $rootScope.$broadcast(AUTH_EVENTS.sessionTimeout,
                                      response);
            }
            return $q.reject(response);
        }
    };
});
// register the interceptor as a service, intercepts ALL angular ajax http calls
 app.factory('myHttpInterceptor', function ($q, $window) {

        return function (promise) {
            return promise.then(function (response) {
                // do something on success
                // todo hide the spinner
                //$("#site_statistics_loading").hide();
                return response;

            }, function (response) {
                // do something on error
                // todo hide the spinner
               // $("#site_statistics_loading").hide();
                return $q.reject(response);
            });
        };
    });


//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider
        //Register page 
        .when('/Account/Register',
            {
                controller: 'RegisterController',
                templateUrl: '/Account/Register'
            })
        //login page
         .when('/Account/Login',
            {
                controller: 'LoginController',
                templateUrl: '/Account/Login'
            })
        .otherwise({ templateUrl: '/Account/Login', controller: 'LoginController' });
});
