﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Guardian.Models
{
    public class ResponeResult
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage{get;set;}
        public dynamic Data { get; set; }
    }
}