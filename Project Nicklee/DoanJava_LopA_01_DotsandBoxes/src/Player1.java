import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * 
 * Form hiển thị khi player 1 có số điểm lớn hơn player 2.
 *
 */

public class Player1 extends JFrame {
	

	/**
	 * Tạo form, set size, set background
	 */
	
	public Player1(){
		
		// SET FORM
		
		setUndecorated(true);
		setAlwaysOnTop(true);
		setBounds(395, 300, 508, 225);
		setLayout(null);
		
		// CLOSE
		
		final JLabel bClose = new JLabel();
		bClose.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bClose.setIcon(new ImageIcon(Player1.class.getResource("/data/img/close1.png")));
		bClose.setBounds(296, 138, 100, 50);
		getContentPane().add(bClose);
		bClose.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseExited(MouseEvent e) {
				bClose.setIcon(new ImageIcon(Player1.class.getResource("/data/img/close1.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				bClose.setIcon(new ImageIcon(Player1.class.getResource("/data/img/closeenter.png")));
			}
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
			
		});
		
		// PLAYER 1 WIN
		
		JLabel lbWin = new JLabel();
		lbWin.setIcon(new ImageIcon(Player1.class.getResource("/data/img/player1win.gif")));
		lbWin.setBounds(0, 0, 508, 225);
		getContentPane().add(lbWin);
		
		// TẠO HÌNH NỀN
		
		JLabel lb = new JLabel();
		lb.setIcon(new ImageIcon(Player1.class.getResource("/data/img/bg.png")));
		lb.setBounds(0, 0, 508, 225);
		getContentPane().add(lb);
	}
}
