import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * 
 * Exit form: Hiá»ƒn thá»‹ khi ngÆ°á»�i chÆ¡i click vÃ o nÃºt Quit
 *
 */

public class ExitForm extends JFrame{
	
	/**
	 * Táº¡o form, set size, set background
	 */
	
	public ExitForm(){
		
		// THIET LAP FORM
		
		setUndecorated(true);
		setAlwaysOnTop(true);
		setSize(300, 200);
		setLocationRelativeTo(null);
		setLayout(null);
		
		// YES
		
		JLabel yes = new JLabel();
		yes.setBounds(71, 132, 69, 36);
		yes.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		getContentPane().add(yes);
		yes.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
		
		// NO
		
		JLabel no = new JLabel();
		no.setBounds(169, 132, 69, 36);
		no.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		getContentPane().add(no);
		no.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
			
			
		});
		
		// TAO HINH NEN
		
		JLabel bg = new JLabel();
		bg.setIcon(new ImageIcon(ExitForm.class.getResource("/data/img/exitbg.png")));
		bg.setBounds(0, 0, 300, 200);
		getContentPane().add(bg);
	}
}
