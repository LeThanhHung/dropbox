import javax.swing.*;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.Font;

/**
 * 
 * Tạo giao diện chính của game và xây dựng các phương thức xử lý
 * 
 */

public class GameForm extends JFrame {
	
	public JLabel p1_color; // label thể hiện màu sắc của player 1
	
	public JLabel p1_score; // label thể hiện điểm player 1
	
	public JLabel p2_score; // label thể hiện điểm của player 2
	
	public JLabel p2_color; // label thể hiện màu sắc của player 2
	
	public JLabel lbCOM; 
	
	public JLabel lbPlayer2;
	
	public JLabel lbPlayer1;
	
	public JLabel lblVs;
	
	public JLabel lbbg; 
	
	public JButton bMenu;
	
	final JButton bAbout;
	
	public JButton bnewgame;
	
	public JButton bquit;
	
	public int request;
	
	int N = 1; // Số người chơi. Mặc định lúc đầu bằng 1
	
	int tmpI; // số thứ ô vuông
	
	int tmpJ; // số thứ tự cạnh
	
	int turn=0;
	
	final JLabel [] line_col = new JLabel [73];
	
	final JLabel [] line_row = new JLabel [73];
	
	int [][] square = new int [65][7];
	
	public JLabel [] Box = new JLabel [65];
	
	public JPanel Canvas;
	
	private JLabel timer = new JLabel("00:00:00");

	// TIMER
	private int hour = 0, min = 0, sec = 0;
	private String gio, phut, giay;
	Timer t = new Timer(1000, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {

			++sec;
			if (sec == 60) {
				sec = 0;
				++min;
				if (min == 60) {
					min = 0;
					hour++;
				}
			}

			if (sec < 10)
				giay = "0" + String.valueOf(sec);
			else
				giay = String.valueOf(sec);
			if (min < 10)
				phut = "0" + String.valueOf(min);
			else
				phut = String.valueOf(min);
			if (hour < 10)
				gio = "0" + String.valueOf(hour);
			else
				gio = String.valueOf(hour);
			timer.setText(gio + ":" + phut + ":" + giay);
		}
	});
	
	/**
	 * Phương thức reset Timer
	 */

	public void resetTimer() {
		t.stop();
		hour = 0;
		min = 0;
		sec = 0;
		t.start();
	}
	
	/**
	 * Tạo mảng 64 ô vuông
	 */
	public void createBox(){
		int x =42,y=118;
		for(int i =1;i<=64;i++){
						
			Box[i] = new JLabel();
			Box[i].setBounds(x,y,53,51);
			getContentPane().add(Box[i]);
			if ((i==8)||(i==16)||(i==24)||(i==32)||(i==40)||(i==48)||(i==56)){
				x=42;
				y+=56;
			}
			else {
				x+=58;
			}
		}
		
	}
	/**
	 * Tạo giao diện, gọi các phương thức xử lý trong game
	 */
	public GameForm() {
		
		// THIẾT LẬP FORM
		
		ImageIcon icon = new ImageIcon(GameForm.class.getResource("/data/img/icon.png"));
		setIconImage(icon.getImage());
		setSize(700,650);
		setResizable(false);
		setLocationRelativeTo(null);
		setLayout(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		
		// TIMER
		
		t.start();
		timer.setFont(new Font("Times New Roman", Font.BOLD, 17));
		timer.setForeground(Color.blue);
		timer.setBounds(470, 587, 110, 35);
		getContentPane().add(timer);
		
		// COM
		
		lbCOM = new JLabel();
		lbCOM.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/pc.png")));
		lbCOM.setBounds(404, 13, 90, 45);
		getContentPane().add(lbCOM);
		
		// COM MÀU:
		
		p2_color = new JLabel();
		p2_color.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/blue.png")));
		p2_color.setFont(new Font("Rosewood Std Regular", Font.BOLD, 25));
		p2_color.setBounds(367, 18, 25, 16);
		getContentPane().add(p2_color);
		
		// COM ĐIỂM:
		
		p2_score = new JLabel("0",JLabel.CENTER);
		p2_score.setForeground(Color.BLACK);
		p2_score.setFont(new Font("Cambria", Font.BOLD, 20));
		p2_score.setBounds(367, 40, 25, 16);
		getContentPane().add(p2_score);
		
		// Player 1:
		
		lbPlayer1 = new JLabel();
		lbPlayer1.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p1.png")));
		lbPlayer1.setBounds(47, 13, 90, 45);
		getContentPane().add(lbPlayer1);
		
		// Player 1 ĐIỂM:
		
		p1_score = new JLabel("0",JLabel.CENTER);
		p1_score.setForeground(Color.BLACK);
		p1_score.setFont(new Font("Cambria", Font.BOLD, 20));
		p1_score.setBackground(Color.RED);
		p1_score.setBounds(150, 40, 25, 16);
		getContentPane().add(p1_score);
		
		// Player 1 MÀU:
		
		p1_color = new JLabel();
		p1_color.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/red.png")));
		p1_color.setFont(new Font("Rosewood Std Regular", Font.BOLD, 25));
		p1_color.setForeground(Color.BLACK);
		p1_color.setBounds(150, 18, 25, 16);
		getContentPane().add(p1_color);
		
		// Player 2
		
		lbPlayer2 = new JLabel();
		lbPlayer2.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p2.png")));
		lbPlayer2.setBounds(404, 11, 90, 45);
		getContentPane().add(lbPlayer2);
		
		// VS
		
		lblVs = new JLabel();
		lblVs.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/vs.png")));
		lblVs.setBounds(225, 15, 80, 40);
		getContentPane().add(lblVs);
		
		// KHỞI TẠO MẶC ĐỊNH : PLAYER VS COM
		
		lbCOM.setVisible(true);
		lbPlayer2.setVisible(false);
		turn =1;
		
		// NÚT MENU
		
		bMenu = new JButton();
		bMenu.setBorder(BorderFactory.createEmptyBorder());
		bMenu.setContentAreaFilled(false);
		bMenu.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bMenu.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/bmenu.png")));
		bMenu.setBounds(592, 13, 90, 45);
		getContentPane().add(bMenu);
		bMenu.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
			   new MenuForm().setVisible(true);	
			}
		});
		
		// NÚT ABOUT
		
		bAbout = new JButton();
		bAbout.setBorder(BorderFactory.createEmptyBorder());
		bAbout.setContentAreaFilled(false);
		bAbout.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bAbout.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/about.png")));
		bAbout.setBounds(592, 310, 90, 45);
		getContentPane().add(bAbout);
		bAbout.addMouseListener(new MouseAdapter() {
			
			public void mouseExited(MouseEvent e) {
				bAbout.setIcon(new ImageIcon(YouLose.class.getResource("/data/img/about.png")));
			}
							
			public void mouseEntered(MouseEvent e) {
				bAbout.setIcon(new ImageIcon(YouLose.class.getResource("/data/img/aboutenter.png")));
			}
			public void mouseClicked(MouseEvent e) {
				new About().setVisible(true);
			}
			
		});
		
		// NÚT NEW GAME
		
		bnewgame = new JButton();
		bnewgame.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bnewgame.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/bnewgame.png")));
		bnewgame.setBounds(592, 150, 90, 45);
		bnewgame.setBorder(BorderFactory.createEmptyBorder());
		bnewgame.setContentAreaFilled(false);
		getContentPane().add(bnewgame);
		bnewgame.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				reset();
				create_square();
				turn =1;
				sec = 0;
				min = 0;
				hour = 0;
				timer.setText("00:00:00");
				t.start();
			}
			public void mouseEntered(MouseEvent e) {
				bnewgame.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/bnewgameenter.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				bnewgame.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/bnewgame.png")));
			}
		});
		
		// NÚT QUIT
		
		bquit = new JButton();
		bquit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bquit.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/bquit.png")));
		bquit.setBounds(592, 230, 90, 45);
		bquit.setBorder(BorderFactory.createEmptyBorder());
		bquit.setContentAreaFilled(false);
		getContentPane().add(bquit);
		bquit.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				new ExitForm().setVisible(true);
			}
			public void mouseEntered(MouseEvent e) {
				bquit.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/bquitenter.png")));
			}
			public void mouseExited(MouseEvent e) {
				bquit.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/bquit.png")));
			}
		});
		
		create_square();
		
		sync();
		
		create_line_col();
		
		create_line_row();
		
		createBox();
		
		// TẠO HÌNH NỀN
		
		lbbg = new JLabel();
		lbbg.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/br.png")));
		lbbg.setBounds(0, 0, 694, 615);
		getContentPane().add(lbbg);
		
		// HIỂN THỊ CÁC DẤU CHẤM
		
		Canvas = new Canvas();
		Canvas.setBounds(25, 100, 521, 511);
		lbbg.add(Canvas);
		
		setVisible(true);
	}
	
	/**
	 * Khởi tạo mảng hai chiều để đánh dấu các đường line và các thuộc tính như là tô màu hay chưa, đã tạo ra vô vuông hay chưa...
	 */
	public void create_square(){
		for(int i=1;i<=64;i++){
			for(int j=1;j<=6;j++){
				square[i][j]=0;
			}
			
			
		}
	}
	
	/**
	 * Kiểm tra xem đã tạo được ô vuông và tô màu hay chưa.  
	 * @return số lượng các ô vuông đã được tạo và tô màu
	 */
	public int check(){
		int i,j;
		Color a=null;
		
		if(turn %2 == 0 ) a = Color.blue;
		else a = Color.red;
		for(i=1;i<=64;i++){
			int d =0;
			for(j=1;j<=4;j++){
				if(square[i][j]==1) d++;
			}
			if(d==4) square[i][5]=1;
		}
		int dem=0;
		for(i=1;i<=64;i++){
			if((square[i][5]==1)&&(square[i][6]==0)){
				square[i][6]=1;
				Box[i].setOpaque(true);
				Box[i].setBackground(a);
				dem++;
			}
		}
		return dem;
    }
	
	/**
	 * Kiểm tra đã kết thúc game hay chưa
	 * @return true nếu đã kết thúc, false nếu chưa kết thúc
	 */
	public boolean checkend(){
		int d=0;
		for(int i =1 ;i<=64;i++){
			if((Box[i].getBackground()==Color.red) ||(Box[i].getBackground()==Color.blue)) d++;
		}
		if (d ==64)
		return true;
		else return false;
	}
	
	/**
	 *  Hiển thị điểm khi người chơi tạo được ô vuông 
	 */
	public void setDiem(){
		int d_red=0 ,d_blue=0;
		for (int i =1;i<=64;i++){
			if((Box[i].getBackground()==Color.red)){
				d_red++;
				p1_score.setText(String.valueOf(d_red));
			}
			if((Box[i].getBackground()==Color.blue)){
				d_blue++;
				p2_score.setText(String.valueOf(d_blue));
			}
		}
	}
	
	/**
	 * Hiển thị người chơi nào thắng, thua hay hòa
	 */
	public void diem(){
		int d_red=0,d_blue=0;
		for (int i =1;i<=64;i++){
			if((Box[i].getBackground()==Color.red)){
				d_red++;
				p1_score.setText(String.valueOf(d_red));
			}
			if((Box[i].getBackground()==Color.blue)){
				d_blue++;
				p2_score.setText(String.valueOf(d_blue));
			}
		}
		if(d_red>d_blue){
			if(N==1){
				YouWin a = new YouWin();
				a.setVisible(true);
			}
			else{
				Player1 a = new Player1();
				a.setVisible(true);
			}
		}
		if(d_red<d_blue) {
			if(N==1){
				YouLose a = new YouLose();
				a.setVisible(true);
			}
			else{
			    Player2 a = new Player2();
			    a.setVisible(true);
			}
		}
		if(d_red==d_blue) {
			Draw a = new Draw();
			a.setVisible(true);
		}
	}
	
	/**
	 * Đồng bộ hóa các click chuột để đưa vào mảng square
	 */
	public void sync(){
		// Đồng bộ chiều ngang theo cạnh bên phải 
		for (int i =1;i<=64;i++)
			if((i==8)||(i==16)||(i==24)||(i==32)||(i==40)||(i==48)||(i==56)||(i==64))  continue;
			else  {
				if(square[i+1][1]==0)
				square[i+1][1]= square[i][3];
				if(square[i][3]==0)
					square[i][3]= square[i+1][1];
			}
			// Đồng bộ chiều dọc theo cạnh dưới
			for (int i =1;i<=64;i++)
			if(i>56)  continue;
			else {
				if(square[i+8][2]==0)
					square[i+8][2]= square[i][4];
				if(square[i][4]==0)
					square[i][4]=square[i+8][2];
			}

	}
	
	/**
	 * Trả về vị trí location của đường line
	 * @param x hoành độ của đường line
	 * @param y tung độ của đường line
	 */
	public void setcheck(int x , int y){
		int i =0 ,j = 0 ;
		// Kiem tra theo x  
	{	
		if(x==37){
			j=1;
			if(y==119) i =1;
			if(y==175) i =9;
			if(y==231) i =17;
			if(y==287) i =25;
			if(y==343) i =33;
			if(y==399) i =41;
			if(y==455) i =49;
			if(y==511) i =57;
		}
		if(x==95){
			j=1;
			if(y==119) i =2;
			if(y==175) i =10;
			if(y==231) i =18;
			if(y==287) i =26;
			if(y==343) i =34;
			if(y==399) i =42;
			if(y==455) i =50;
			if(y==511) i =58;
			
		}
		if(x==153){
			j=1;
			if(y==119) i =3;
			if(y==175) i =11;
			if(y==231) i =19;
			if(y==287) i =27;
			if(y==343) i =35;
			if(y==399) i =43;
			if(y==455) i =51;
			if(y==511) i =59;
			
		}
		if(x==211){
			j=1;
			if(y==119) i =4;
			if(y==175) i =12;
			if(y==231) i =20;
			if(y==287) i =28;
			if(y==343) i =36;
			if(y==399) i =44;
			if(y==455) i =52;
			if(y==511) i =60;
			
		}
		if(x==269){
			j=1;
			if(y==119) i =5;
			if(y==175) i =13;
			if(y==231) i =21;
			if(y==287) i =29;
			if(y==343) i =37;
			if(y==399) i =45;
			if(y==455) i =53;
			if(y==511) i =61;
			
		}
		if(x==327){
			j=1;
			if(y==119) i =6;
			if(y==175) i =14;
			if(y==231) i =22;
			if(y==287) i =30;
			if(y==343) i =38;
			if(y==399) i =46;
			if(y==455) i =54;
			if(y==511) i =62;
			
		}
		if(x==385){
			j=1;
			if(y==119) i =7;
			if(y==175) i =15;
			if(y==231) i =23;
			if(y==287) i =31;
			if(y==343) i =39;
			if(y==399) i =47;
			if(y==455) i =55;
			if(y==511) i =63;
			
		}
		if(x==443){
			j=1;
			if(y==119) i =8;
			if(y==175) i =16;
			if(y==231) i =24;
			if(y==287) i =32;
			if(y==343) i =40;
			if(y==399) i =48;
			if(y==455) i =56;
			if(y==511) i =64;
			
		}
		if(x==501){
			j=3;
			if(y==119) i =8;
			if(y==175) i =16;
			if(y==231) i =24;
			if(y==287) i =32;
			if(y==343) i =40;
			if(y==399) i =48;
			if(y==455) i =56;
			if(y==511) i =64;
		}
		if(x==44){
			j=2;
			if(y==113) i =1;
			if(y==169) i =9;
			if(y==225) i =17;
			if(y==281) i =25;
			if(y==337) i =33;
			if(y==393) i =41;
			if(y==449) i =49;
			if(y==505) i =57;
		}
		if(x==102){
			j=2;
			if(y==113) i =2;
			if(y==169) i =10;
			if(y==225) i =18;
			if(y==281) i =26;
			if(y==337) i =34;
			if(y==393) i =42;
			if(y==449) i =50;
			if(y==505) i =58;
		}
		if(x==160){
			j=2;
			if(y==113) i =3;
			if(y==169) i =11;
			if(y==225) i =19;
			if(y==281) i =27;
			if(y==337) i =35;
			if(y==393) i =43;
			if(y==449) i =51;
			if(y==505) i =59;
		}
		if(x==218){
			j=2;
			if(y==113) i =4;
			if(y==169) i =12;
			if(y==225) i =20;
			if(y==281) i =28;
			if(y==337) i =36;
			if(y==393) i =44;
			if(y==449) i =52;
			if(y==505) i =60;
		}
		if(x==276){
			j=2;
			if(y==113) i =5;
			if(y==169) i =13;
			if(y==225) i =21;
			if(y==281) i =29;
			if(y==337) i =37;
			if(y==393) i =45;
			if(y==449) i =53;
			if(y==505) i =61;
		}
		if(x==334){
			j=2;
			if(y==113) i =6;
			if(y==169) i =14;
			if(y==225) i =22;
			if(y==281) i =30;
			if(y==337) i =38;
			if(y==393) i =46;
			if(y==449) i =54;
			if(y==505) i =62;
		}
		if(x==392){
			j=2;
			if(y==113) i =7;
			if(y==169) i =15;
			if(y==225) i =23;
			if(y==281) i =31;
			if(y==337) i =39;
			if(y==393) i =47;
			if(y==449) i =55;
			if(y==505) i =63;
		}
		if(x==450){
			j=2;
			if(y==113) i =8;
			if(y==169) i =16;
			if(y==225) i =24;
			if(y==281) i =32;
			if(y==337) i =40;
			if(y==393) i =48;
			if(y==449) i =56;
			if(y==505) i =64;
		}
		if(x==450){
			j=2;
			if(y==113) i =8;
			if(y==169) i =16;
			if(y==225) i =24;
			if(y==281) i =32;
			if(y==337) i =40;
			if(y==393) i =48;
			if(y==449) i =56;
			if(y==505) i =64;
		}
		if(y==561){
			j=4;
			if(x==44) i=57;
			if(x==102) i=58;
			if(x==160) i=59;
			if(x==218) i=60;
			if(x==276) i=61;
			if(x==334) i=62;
			if(x==392) i=63;
			if(x==450) i=64;
		}

	}
		square[i][j]=1;
	}
	
	/**
	 * Tìm vị trí mà nếu đánh vào đó thì sẽ tạo được một ô vuông
	 * @return 1 nếu tồn tại vị trí thỏa yêu cầu, 0 nếu không tồn tại vị trí đó
	 */
	public int find4(){
		int i,j;
		int x = 0,y = 0;
		for( i =1;i<=64;i++){
			int d=0;
			
			for( j=1;j<=4;j++){
				if((square[i][j]==0)&&(square[i][6]==0)) {
					d++;
					x=i;
					y=j;
				}
			}
			if(d==1){
				tmpI = x ;
				tmpJ = y ;
				return 1;
			}
		}
		return 0;
	}
	
	/**
	 * Phương thức nâng cấp cho trí tuệ nhân tạo
	 * kiểm tra các vị trí liên quan gần ô vuông đầu vào sẽ thỏa điều kiện tạo thành ô Box hay không .
	 * @param x vị trí ô vuông
	 * @param y vị trí cạnh
	 * @return 1 nếu thỏa điều kiện
	 * 
	 */
	public int area(int x , int y){
		// X > 1 < 8 
		if((x>1)&&(x<8)){
			
		}
		if((x==9)||(x==17)||(x==25)||(x==33)||(x==41)||(x==49)){
			
		}
		if((x>57)&& (x<64)){
			
		}
		if((x==1)||(x==8)||(x==57)||(x==64)){
			
		}
		else{
			
		}
		return 1;
		
	}
	
	/**
	 * Tìm đường đi nếu find4() trả về 0
	 * @return 1 nếu tìm được đường đi, 0 nếu không tìm được đường đi
	 */
	public int find2(){
		int i,j;
		for( i =1;i<=64;i++){
			int d=0;
			int x = 0,y = 0;
			for( j=1;j<=4;j++){
				if((square[i][j]==1)&&(square[i][6]==0))
				{
					d++;
					x = i ;
					y = j ;
				}
			}
			if(d==1){
				if(y==4){
					tmpI = x;
					tmpJ = y-1;
				}
				else {
					tmpI = x;
					tmpJ = y+1;
				}
				return 1;
			}
		}
		return 0;
	}
	
	/**
	 * Tìm vị trí đường đi nếu find2() trả về 0
	 * @return 1 nếu tìm được đường đi, 0 nếu không tìm được đường đi
	 * 
	 */
	public int rand(){
		int i ,j;
		for(i=1;i<=64;i++){
			for(j=1;j<=4;j++){
				if(square[i][j]==0){
					tmpI = i;
					tmpJ = j;
					return 1;
				}
			}
		}
		return 0;
		
	}
	
	/**
	 * Hiện đường line với các tham số đầu vào x,y
	 * @param x vị trí ô vuông
	 * @param y số thứ tự các cạnh
	 */
	public void setline(int x, int y){
		int X;
		if(y==1){
			if(x%8==0){
				X= ((x/8)) + (((x-1)%8)*8) ;
			}
			else{
				X= ((x/8)+1) + (((x-1)%8)*8) ;
			}
			
			line_row[X].setIcon(new ImageIcon(GameForm.class.getResource("/data/img/row_click.png")));
			line_row[X].setIconTextGap(1);
		}
		if((y==3)){
			if(x%8!=0){
				X= (((x)/8)+1+((x%8)*8));
				// Line doc
			}
			else{
				X= x/8 + 64;
			}
			
			line_row[X].setIcon(new ImageIcon(GameForm.class.getResource("/data/img/row_click.png")));
			line_row[X].setIconTextGap(1);
		}
		if((y==2)){
			X = ((x/8) *8 + (x%8));
			// Line ngang
			
			line_col[X].setIcon(new ImageIcon(GameForm.class.getResource("/data/img/col_click.png")));
			line_col[X].setIconTextGap(1);
		}
		if(y==4){
			X=(((x/8)+1) *8 + (x%8));
			
			line_col[X].setIcon(new ImageIcon(GameForm.class.getResource("/data/img/col_click.png")));
			line_col[X].setIconTextGap(1);
		}
		square[x][y]= 1;
		sync();
		}
	
	/**
	 * Tạo và xử lý sự kiện cho các đường line ngang
	 */
	public void create_line_col(){
		int x=44,y=113;
		for( int i =1;i<=72;i++){
					line_col[i] = new JLabel();
					line_col[i].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					line_col[i].setBounds(x, y, 50, 5);
					line_col[i].addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent e)
							{
									JLabel tmp = (JLabel) e.getSource();
									if(tmp.getIconTextGap()!=1){
										tmp.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/col_click.png")));
										tmp.setIconTextGap(1);
										setcheck(tmp.getLocation().x , tmp.getLocation().y);
										sync();
										if(N==1){
											// Kiểm tra
											if(check()==0){
												turn++;
												// Hàm trí tuệ nhân tạo
												int temp=turn;
												{
													while(temp == turn){
														// Tìm vị trí line cần dùng
														if(find4()==1){
															setline(tmpI, tmpJ);
															sync();
															check();
														}
														else{
															
															// Tìm những ô đã có 1 cạnh 
															if(find2()==1){
																setline(tmpI, tmpJ);
																sync();
															}
															else{
																if(rand()==1){
																	setline(tmpI, tmpJ);
																	sync();
																}
															}
															turn++;
														}
														
													}
												}
											}
										}
										if(N==2){
											if(check()==0){
												turn++;
												if(turn % 2 == 0){
													lbPlayer1.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p1.png")));
													lbPlayer2.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p2turn.png")));
												}
												
												else{
													lbPlayer1.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p1turn.png")));
													lbPlayer2.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p2.png")));
												}
											}
										}
									}
								if(checkend()==false){
									setDiem();
								}
								// kiểm tra kết thúc
								if(checkend()== true){
									t.stop();
									diem();
								}	
							}
							
						public void mouseEntered(MouseEvent e) {
								JLabel tmp = (JLabel) e.getSource();
									if(tmp.getIcon()==null){
										tmp.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/col_enter.png")));
										tmp.setIconTextGap(2);
									}
						}
						public void mouseExited(MouseEvent e) {
							JLabel tmp = (JLabel) e.getSource();
							if(tmp.getIconTextGap()== 2  )tmp.setIcon(null);
						}
					});
					getContentPane().add(line_col[i]);
					if(x<408)x+=58;
					else {
						y+=56;
						x=44;
					}
		}
	}
	
	/**
	 *  Tạo và xử lý sự kiện cho các đường line dọc
	 */
	public void create_line_row(){
		int x=37,y=119;
		for( int i =1;i<=72;i++){
					line_row[i] = new JLabel();
					line_row[i].setIcon(null);
					line_row[i].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					line_row[i].setBounds(x, y, 5, 48);
					line_row[i].addMouseListener(new MouseAdapter() {
						public void mouseClicked(MouseEvent e) {
							JLabel tmp = (JLabel) e.getSource();
							if(tmp.getIconTextGap()!=1){
								tmp.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/row_click.png")));
								tmp.setIconTextGap(1);
								setcheck(tmp.getLocation().x , tmp.getLocation().y);
								sync();
								if(N==1){
									// Kiem tra
									if(check()==0){
										turn++;
										// Ham trí tuệ nhân tạo
										int temp=turn;
										{
											while(temp == turn){
												// Tìm vị trí line cần dùng
												if(find4()==1){
													setline(tmpI, tmpJ);
													sync();
													check();
												}
												else{
													// Tìm những ô đã có 1 cạnh 
													if(find2()==1){
														setline(tmpI, tmpJ);
														sync();
													}
													else{
														if(rand()==1){
															setline(tmpI, tmpJ);
															sync();
														}
													}
													turn++;
												}
											}
										}
									}
									else{
									}
								}
								if(N==2){
									if(check()==0){
										turn++;
										if(turn % 2 == 0){
											lbPlayer1.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p1.png")));
											lbPlayer2.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p2turn.png")));
										}
										
										else{
											lbPlayer1.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p1turn.png")));
											lbPlayer2.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p2.png")));
										}
									}
									
								}
							}
							
							if(checkend()==false){
								setDiem();
							}
							// Kiểm tra kết thúc
							if(checkend()== true)
								{
								    t.stop();
									diem();
									
								}
						}
						public void mouseEntered(MouseEvent e) {
							JLabel tmp = (JLabel) e.getSource();
							if(tmp.getIcon()==null)
							{
								tmp.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/row_enter.png")));
								tmp.setIconTextGap(2);
							}
						}
						public void mouseExited(MouseEvent e) {
							JLabel tmp = (JLabel) e.getSource();
							if(tmp.getIconTextGap()== 2  )							
							tmp.setIcon(null);
						}
					});
						
					getContentPane().add(line_row[i]);
					if(y<500)y+=56;
					else {
						y=119;
						x+=58;
					}
		}
	}
	
	
	/**
	 * Thay đổi chế độ chơi, hiện label liên quan
	 * @param a số người chơi
	 */
	public void change(int a){
		if(a==1){
			lbCOM.setVisible(true);
			lbPlayer2.setVisible(false);
		}
		if(a==2){
			lbCOM.setVisible(false);
			lbPlayer2.setVisible(true);
		}
	}
	
	/**
	 * Tạo mới game, xóa toàn bộ dữ liệu hiện tại
	 */
	
	public void reset(){
		create_square();
		sync();
		for(int i =1;i<=64;i++){
			Box[i].setOpaque(false);
			Box[i].setBackground(null);
		}
		for(int i = 1; i<=72;i++){
			line_col[i].setIcon(null);
			line_col[i].setIconTextGap(0);
			line_row[i].setIcon(null);
			line_row[i].setIconTextGap(0);
		}
		p1_score.setText("0");
		p2_score.setText("0");
		turn =1;
		
		if(N==1){
			lbPlayer1.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p1.png")));
		}
		
		if(N==2){
			lbPlayer1.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p1turn.png")));
			lbPlayer2.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/p2.png")));
		}
   }
}
