import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * 
 * Form hiển thị khi player 2 có số điểm lớn hơn player 1.
 *
 */

public class Player2 extends JFrame {
	
	/**
	 * Tạo form, set size, set background
	 */
	
	public Player2(){
		
		// SET FORM
		
		 setUndecorated(true);
		 setAlwaysOnTop(true);
		 setBounds(395, 300, 508, 225);
		 setLayout(null);
				
		// CLOSE
				
		 final JLabel bClose = new JLabel();
		 bClose.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		 bClose.setIcon(new ImageIcon(Player1.class.getResource("/data/img/close1.png")));
		 bClose.setBounds(297, 138, 100, 50);
		 getContentPane().add(bClose);
		 bClose.addMouseListener(new MouseAdapter() {
					
			 public void mouseExited(MouseEvent e) {
					bClose.setIcon(new ImageIcon(Player2.class.getResource("/data/img/close1.png")));
			 }
			
			public void mouseEntered(MouseEvent e) {
					bClose.setIcon(new ImageIcon(Player2.class.getResource("/data/img/closeenter.png")));
			 }
			public void mouseClicked(MouseEvent e) {
					dispose();
			}
					
		 });
				
		// PLAYER 2 WIN
				
		JLabel lbWin = new JLabel();
		lbWin.setIcon(new ImageIcon(Player2.class.getResource("/data/img/player2win.gif")));
		lbWin.setBounds(0, 0, 508, 225);
		getContentPane().add(lbWin);
				
		// TẠO HÌNH NỀN
				
		JLabel lb = new JLabel();
		lb.setIcon(new ImageIcon(Player2.class.getResource("/data/img/bg.png")));
		lb.setBounds(0, 0, 508, 225);
		getContentPane().add(lb);
	}
}
