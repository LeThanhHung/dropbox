import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * 
 * Hiển thị kết quả nếu người chơi thua (chế độ chơi với com)
 *
 */

public class YouLose extends JFrame {
	
	/**
	 * Tạo form, set background, hiện dòng chữ "You lose"
	 *
	 */
	public YouLose(){
		
		// Thiet lap form
		
		setUndecorated(true);
		setAlwaysOnTop(true);
		setBounds(395, 300, 508, 225);
		setLayout(null);
						
		// CLOSE
						
		final JLabel bClose = new JLabel();
		bClose.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bClose.setIcon(new ImageIcon(Player1.class.getResource("/data/img/close1.png")));
		bClose.setBounds(309, 132, 100, 50);
		getContentPane().add(bClose);
		bClose.addMouseListener(new MouseAdapter() {
							
			public void mouseExited(MouseEvent e) {
				bClose.setIcon(new ImageIcon(YouLose.class.getResource("/data/img/close1.png")));
			}
					
			public void mouseEntered(MouseEvent e) {
				bClose.setIcon(new ImageIcon(YouLose.class.getResource("/data/img/closeenter.png")));
			}
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
							
		});
						
		// YOU LOSE
						
		JLabel lbLose = new JLabel();
		lbLose.setIcon(new ImageIcon(YouLose.class.getResource("/data/img/youlose.gif")));
		lbLose.setBounds(0, 0, 508, 225);
		getContentPane().add(lbLose);
						
		// TAO HINH NEN
						
		JLabel lb = new JLabel();
		lb.setIcon(new ImageIcon(YouLose.class.getResource("/data/img/bg.png")));
		lb.setBounds(0, 0, 508, 225);
		getContentPane().add(lb);
	}
}
