import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * Form hiển thị khi hai người chơi đều bằng điểm nhau
 */

public class Draw extends JFrame {
	
	/**
	 * Tạo form, set background, hiện dòng chữ "Draw"
	 *
	 */
	
	public Draw(){
		
		// THIET LAP FORM
		
		setUndecorated(true);
		setAlwaysOnTop(true);
		setSize(508, 225);
		setLocationRelativeTo(null);
		setLayout(null);
		
		// CLOSE
		
		final JLabel bClose = new JLabel();
		bClose.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bClose.setIcon(new ImageIcon(Player1.class.getResource("/data/img/close1.png")));
		bClose.setBounds(297, 138, 100, 50);
		getContentPane().add(bClose);
		bClose.addMouseListener(new MouseAdapter() {
					
			 public void mouseExited(MouseEvent e) {
					bClose.setIcon(new ImageIcon(Draw.class.getResource("/data/img/close1.png")));
			 }
			
			public void mouseEntered(MouseEvent e) {
					bClose.setIcon(new ImageIcon(Draw.class.getResource("/data/img/closeenter.png")));
			 }
			public void mouseClicked(MouseEvent e) {
					dispose();
			}
					
		 });
		
		// DRAW
		
		JLabel lbDraw = new JLabel();
		lbDraw.setIcon(new ImageIcon(Draw.class.getResource("/data/img/draw.png")));
		lbDraw.setBounds(0, 0, 508, 225);
		getContentPane().add(lbDraw);
		
		// TAO HINH NEN
		
		JLabel lb = new JLabel();
		lb.setIcon(new ImageIcon(Draw.class.getResource("/data/img/bg.png")));
		lb.setBounds(0, 0, 508, 225);
		getContentPane().add(lb);
	}
}
