import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Tạo menu các chức năng của game
 *
 */

public class MenuForm extends JFrame{
	
	public static GameForm 	a = new GameForm();
	
	public static HTPform 	b = new HTPform();
	
	private JButton bResume;
	
	private JButton bHowToPlay;
	
	private JButton bPvp;
	
	private JButton bPvc;
	
	private JButton bQuit;
	
	private JLabel lbbg;
	
	public MenuForm(){
		
		// SET FORM
		setAlwaysOnTop(true);
		setUndecorated(true);
		setSize(300, 400);
		setLocationRelativeTo(null);
		setLayout(null);
		
		// RESUME
		bResume = new JButton();
		bResume.setBounds(90, 20, 114, 46);
		bResume.setBorder(BorderFactory.createEmptyBorder());
		bResume.setContentAreaFilled(false);
		bResume.setIcon(new ImageIcon(MenuForm.class.getResource("/data/img/resume.png")));
		bResume.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		getContentPane().add(bResume);
		bResume.addActionListener(new ActionListener() {
	
			public void actionPerformed(ActionEvent e) {
				MenuForm.this.dispose();
			}
		});
		
		// PVC
		bPvc = new JButton();
		bPvc.setBounds(90, 90, 114, 46);
		bPvc.setBorder(BorderFactory.createEmptyBorder());
		bPvc.setContentAreaFilled(false);
		bPvc.setIcon(new ImageIcon(MenuForm.class.getResource("/data/img/vsCom.png")));
		bPvc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		getContentPane().add(bPvc);
		bPvc.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						
						a.change(1);
						a.resetTimer();
						a.N =1;
						a.reset();
						MenuForm.this.dispose();
						
					}
				});
		
		// PVP
		bPvp = new JButton();
		bPvp.setBounds(90, 160, 114, 46);
		bPvp.setBorder(BorderFactory.createEmptyBorder());
		bPvp.setContentAreaFilled(false);
		bPvp.setIcon(new ImageIcon(MenuForm.class.getResource("/data/img/vsplayer.png")));
		bPvp.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		getContentPane().add(bPvp);
		bPvp.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				a.change(2);
				a.resetTimer();
				a.N = 2;
				a.reset();
				MenuForm.this.dispose();
			}
		});

		// How To Play
		bHowToPlay = new JButton();
		bHowToPlay.setBounds(90, 229, 114, 46);
		bHowToPlay.setBorder(BorderFactory.createEmptyBorder());
		bHowToPlay.setContentAreaFilled(false);
		bHowToPlay.setIcon(new ImageIcon(MenuForm.class.getResource("/data/img/htp.png")));
		bHowToPlay.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		getContentPane().add(bHowToPlay);
		bHowToPlay.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				b.setVisible(true);
				MenuForm.this.dispose();
			}
		});
		
		// Quit
		bQuit= new JButton();
		bQuit.setBounds(90, 300, 114, 46);
		bQuit.setBorder(BorderFactory.createEmptyBorder());
		bQuit.setContentAreaFilled(false);
		bQuit.setIcon(new ImageIcon(MenuForm.class.getResource("/data/img/quit.png")));
		bQuit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		getContentPane().add(bQuit);
		bQuit.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
				
			}
		});
		
		// set Background
		lbbg = new JLabel();
		lbbg.setIcon(new ImageIcon(MenuForm.class.getResource("/data/img/menuform.png")));
		lbbg.setBounds(0, 0, 300, 400);
		getContentPane().add(lbbg);
	}
}
