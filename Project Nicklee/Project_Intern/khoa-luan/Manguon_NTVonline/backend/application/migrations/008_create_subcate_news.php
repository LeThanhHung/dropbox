<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_subcate_news extends CI_Migration {

  public function up() {
    $this->dbforge->add_field(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'subcate_id' => array(
        'type' => 'INT',
        'constraint' => '11'
      ),
      'news_id' => array(
        'type' => 'INT',
        'constraint' => '11'
      ),
    ));
    $this->dbforge->add_key('id', TRUE);
    $this->dbforge->create_table('subcate_news');
  }

  public function down()
  {
    $this->dbforge->drop_table('subcate_news');
  }
}
 ?>