<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_hot_key extends CI_Migration {

  public function up() {
    $this->dbforge->add_field(array(
      'key_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'hot_key' => array(
        'type' => 'VARCHAR',
        'constraint' => '255'
      ),
      'status' => array(
        'type' => 'TINYINT'
      ),
    ));
    $this->dbforge->add_key('key_id', TRUE);
    $this->dbforge->create_table('hot_keys');
  }

  public function down()
  {
    $this->dbforge->drop_table('hot_keys');
  }
}
 ?>