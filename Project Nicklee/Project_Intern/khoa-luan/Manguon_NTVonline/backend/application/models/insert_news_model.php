<?php 
  class Insert_news_model extends CI_Model {
    public function __construct(){
      parent::__construct();
      $this->load->database();
    }

    // function insert news table 
    public function insert_news($data){
      $this->db->insert('news', $data);
    }

    // function insert cate_news table
     public function insert_cate_news($data){
      $this->db->insert('cate_news', $data);
    }

    // function insert subcate_news table
    public function insert_subcate_news($data){
      $this->db->insert('subcate_news', $data);
    }
    
    // function check image for source_id =2
    public function check_image(){
      $this->db->select('image');
      $this->db->from('news');
      $this->db->where('source_id',2);
      $this->db->like('image','http:');
      $query = $this->db->get();
      return $query ->result_array();
    }
    //function insert hot keys
    public function insert_hot_key($data){
      $this->db->insert('hot_keys',$data);
    }

    // function insert xpath to database
    public function insert_xpath($data){
      $this->db->insert('raws',$data);
    }
    //function check hot news
    public function check_hot_news($key,$currentDay){
      $this->db->select('hot_news,news_id,title');
      $this->db->from('news');
      $this->db->where('hot_news !=',1);
      $this->db->where('create_date >',$currentDay);     
      $this->db->like('title', $key);
      $query = $this->db->get();
      return $query->result_array();
    }

    // function remove update hot_news =1
    public function remove_hot_news($currentDay){
      $this->db->select('hot_news,news_id,title');
      $this->db->from('news');
      $this->db->where('hot_news =',1);
      $this->db->where('create_date <',$currentDay);     
      $query = $this->db->get();
      return $query->result_array();
    }


    // function check hot key
    public function check_hot_keys($key){
      $this->db->select('hot_key');
      $this->db->where('hot_key',$key);
      $query = $this->db->get('hot_keys');
      return $query ->result_array();
    }
   // function check same news 
    public function check_news($title){
      $this->db->from('news');
      $this->db->where('title',$title);
      $query = $this->db->get();
      if($query->num_rows() > 0) return 1;
        else return 0;
    }
    // function check day number on month
    public function checkMonth($month){
      switch ($month){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
          return 31;
          break;
        case 4:
        case 6:
        case 9:
        case 11:
          return 30;
          break;
        case 2:
          if(date('L', strtotime('2010-01-01'))){
           return 29;
          } else {
           return 28;
          }
          break;
      default:
        echo "Khong xac dinh";
      }
    }
}
?>



