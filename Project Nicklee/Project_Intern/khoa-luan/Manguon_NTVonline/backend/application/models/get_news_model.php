<?php 
  class Get_news_model extends CI_Model {
    public function __construct(){
      parent::__construct();
      $this->load->database();
    }

    // function get content news table 
    public function get_news_details($news_id) {
      $this->db->select('title,content,create_date,image,source_name');
      $this->db->from('news');
      $this->db->join('sources','news.source_id = sources.source_id');
      $this->db->where('news_id',$news_id);
      $query = $this->db->get();
      return $query ->row_array();
    }

    //function get xpath
    public function get_xpath(){
      $query = $this->db->get('raws');
      return $query->result_array();
    }

    //function get hot keys
    public function get_hot_key(){
      $this->db->select('hot_key');
      $query = $this->db->get('hot_keys');
      return $query->result_array();
    }

    //function get title for content.
    public function get_title_content(){
      $this->db->select('title');
      $query = $this->db->get('news');
      return $query->row_array();
    }
    //function get cate_name for news categoryes
    public function get_cate_name() {
      $this->db->select('cate_name,cate_id');
      $query = $this->db->get('categories');
      return $query->result_array();
    }

    // Get all subcategories name by categories.
    public function get_sub_cate_name($id) {
      $this->db->select('subcate_name,subcate_id,subcategories.cate_id');
      $this->db->from('subcategories');
      $this->db->join('categories','categories.cate_id = subcategories.cate_id');
       $this->db->where('categories.cate_id',$id);
      $query = $this->db->get();
      return $query->result_array();
    }

    //function get news's by subcategory
    public function get_news_subcate($subcate_id,$id) {
      $this->db->select('title,image,source_name,news.news_id');
      $this->db->from('subcategories');
      $this->db->join('subcate_news','subcategories.subcate_id = subcate_news.subcate_id');      
      $this->db->join('news','subcate_news.news_id = news.news_id');
      $this->db->join('sources','news.source_id = sources.source_id ');      
      $this->db->where('subcategories.subcate_id',$subcate_id);
      $this->db->limit(6,$id);
      $this->db->order_by('news_id', 'desc');
      $query = $this->db->get();
      return $query ->result_array(); 
    }
 //function get news's by sources
    public function get_news_source($source_id,$id) {
      $this->db->select('title,image,source_name,news_id');
      $this->db->from('news');
      $this->db->join('sources','news.source_id = sources.source_id ');
      $this->db->where('news.source_id',$source_id);
      $this->db->limit(6,$id);
      $this->db->order_by('news_id', 'desc');
      $query = $this->db->get();
      return $query ->result_array(); 
    }
    //function get news's by categories
    public function get_news_cate($cate_id, $id) {
      $this->db->select('title,image,source_name,news.news_id');
      $this->db->from('categories');
      $this->db->join('cate_news','categories.cate_id = cate_news.cate_id');      
      $this->db->join('news','cate_news.news_id = news.news_id');
      $this->db->join('sources','news.source_id = sources.source_id ');      
      $this->db->where('categories.cate_id',$cate_id);
      $this->db->limit(6,$id);
      $this->db->order_by('news_id', 'desc');
      $query = $this->db->get();
      return $query ->result_array(); 
    }

    // function get hot news by news table
    public function get_hot_news($id) {
      $this->db->select('title,image,source_name,news_id,create_date');
      $this->db->from('news');
      $this->db->join('sources','news.source_id = sources.source_id');
      $this->db->where('hot_news',1);      
      $this->db->order_by('news_id','desc');
      $this->db->limit(6,$id);
      $query = $this->db->get();
      return $query ->result_array();
    }

    // function get news for cate_id =1
    public function get_title_cateid(){
      $this->db->select('title,cate_id');
      $this->db->from('news');
      $this->db->where('cate_id',1);
      $query = $this->db->get();
      return $query ->result_array();
    }

    //function get image for source_id =2;
    public function get_image_news(){
      $this->db->select('image,news_id');
      $this->db->from('news');
      $this->db->join('sources','news.source_id = sources.source_id');
      $this->db->where('news.source_id',2);
      $query = $this->db->get();
      return $query ->result_array();
    }

    //count all hot news
    public function count_hot_news(){
      $this->db->from('news');
      $this->db->where('hot_news',1);
      $query =$this->db->count_all_results();
      return $query;
    }

    //count all news subcategory
    public function count_news_sub($id){
      $this->db->from('news');
      $this->db->join('subcate_news','news.news_id = subcate_news.news_id');
      $this->db->where('subcate_id',$id);
      $query =$this->db->count_all_results();
      return $query;
    }

     //count all source news
    public function count_source_news($id){
      $this->db->from('news');
      $this->db->where('source_id',$id);
      $query =$this->db->count_all_results();
      return $query;
    }


    public function numberRead($id){
      $this->db->select('number_read');
      $this->db->where('news_id',$id);
      $query = $this->db->get('news');
      return $query ->result_array();
    }

    public function get_number_read($id){
      $this->db->select('title,image,source_name,news_id,number_read');
      $this->db->from('news');
      $this->db->join('sources','news.source_id = sources.source_id');
      $this->db->limit(8,$id);
      $this->db->order_by('number_read','desc');
      $query = $this->db->get();
      return $query->result_array();
    }
    
    public function count_news(){
      $this->db->from('news');
      $query =$this->db->count_all_results();
      return $query;
    }

    // function get all source name
    public function get_source_name() {
      $this->db->select('source_name,source_id');
      $query = $this->db->get('sources');
      return $query->result_array();
    }
}
?>
 