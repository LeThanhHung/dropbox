<?php 
  class Update_news_model extends CI_Model {
    public function __construct(){
      parent::__construct();
      $this->load->database();
    }

     // function update news for hot news
    public function update_hot_news($data,$id){
      $this->db->where('news_id', $id);
      $this->db->update('news', $data); 
    }

    // function update image for soure_id = 2
    public function update_image_tienphong($data,$id){
      $this->db->select('image');
      $this->db->where('news_id',$id);
      $this->db->update('news',$data);
    }

    // update number read for news.
    public function numberRead($id,$data){
      $this->db->where('news_id',$id);
      $this->db->update('news',$data); 
    }

}
?>