<?php 
class Number_read extends CI_Controller {
  public function __construct(){
    parent::__construct();
   $this->load->model('Get_news_model');
    $this->load->model('Update_news_model');
  }
  public function index(){
  	$id = $_GET['id'];
  	$data_number = $this->Get_news_model->numberRead($id);
  	foreach ($data_number as $data) {
  		$number_read = $data['number_read']+1;
  	}
    $data = array(
    	'number_read' => $number_read,
    );
    $this->Update_news_model->numberRead($id,$data);
  }
}
?> 