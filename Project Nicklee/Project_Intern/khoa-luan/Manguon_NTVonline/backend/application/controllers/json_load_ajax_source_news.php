<?php 
class Json_load_ajax_source_news extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Get_news_model');
	}
	public function index(){
		$source_id = $_GET["source_id"];
      	$id = $_GET["id"];
      	$data = $this->Get_news_model->get_news_source($source_id,$id);
      	echo json_encode($data);
	}
}
 ?>