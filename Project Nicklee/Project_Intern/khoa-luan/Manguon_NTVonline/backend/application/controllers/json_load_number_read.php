<?php 
class Json_load_number_read extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Get_news_model');
	}
	public function index(){
		$id = $_GET["id"];
      	$data = $this->Get_news_model->get_number_read($id);
       	echo json_encode($data);
	}
}
?>