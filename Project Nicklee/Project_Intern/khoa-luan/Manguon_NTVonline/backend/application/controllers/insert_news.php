<meta charset ='utf-8'>
<?php
class Insert_news extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Insert_news_model');
    $this->load->model('Get_news_model');
    $this->load->model('Update_news_model');
  }
  
  public function index()
  {
    error_reporting(0);
    $data = $this->Get_news_model->get_xpath();
    foreach ($data as $dataarr) {
      $url = $dataarr['url'];
      $dom = new DOMDocument('1.0', 'utf-8');
      $dom->loadHTMLFile(trim($url));
      $xpath      = new DOMXPath($dom);
      $items      = $xpath->query($dataarr['items']);
      $data_array = array();
      foreach ($items as $item) {
        $data['title']       = $xpath->query($dataarr['title'], $item)->item(0)->nodeValue;
        $data['description'] = $xpath->query($dataarr['description'], $item)->item(0)->nodeValue;
        $data['image']       = $xpath->query($dataarr['image'], $item)->item(0)->nodeValue;
        $url2                = $xpath->query($dataarr['url2'], $item)->item(0)->nodeValue;
        $data['link']        = $url2;
        $dom2                = new DOMDocument('1.0', 'utf-8');
        $dom2->loadHTMLFile(trim($url2));
        $xpath2             = new DOMXPath($dom2);
        $content            = $xpath2->query($dataarr['content'])->item(0);
        $data['content']    = $data['short_content'] . $dom2->saveHTML($content);
        $data['source_id']  = $dataarr['source_id'];
        $data['subcate_id'] = $dataarr['subcate_id'];
        $data['cate_id']    = $dataarr['cate_id'];
        $data_array[]       = $data;
      }
      $count_news = $this->Get_news_model->count_news();
      foreach ($data_array as $data) {
        $count_news++;
        $title = strip_tags($data['title']);
        // check news coincidence.
        $count = $this->Insert_news_model->check_news($title);
        if ($count == 0) {
          $image       = $data['image'];
          $link        = $data['link'];
          $subcate_id  = $data['subcate_id'];
          $cate_id     = $data['cate_id'];
          $source_id   = $data['source_id'];
          $description = $data['description'];
          $content     = strip_tags($data['content'], '<p><img>');
          
          // add image default for news no image.
          if (empty($image)) {
            $image = 'http://i.imgur.com/h06lQki.png';
          }
          // set date UTC +7
          date_default_timezone_set("Asia/Bangkok");
          
          if ($source_id == 2) {
            $image   = "http:" . $image;
            $content = str_replace('src="//', 'src="http://', $content);
            $str     = strrpos($content, 'Video đang được xem nhiều');
            $content = substr($content, 0, $str);
          }
          $data = array(            
            'source_id' => $source_id,
            'title' => $title,
            'image' => $image,
            'link' => $link,
            'content' => $content,
            'description' => $description,
            'create_date' => date('Y-m-d H:i:s')
          );
          $data_cate = array(
            'cate_id' => $cate_id,
            'news_id' => $count_news,
          );
          $data_subcate = array(
            'subcate_id' => $subcate_id,
            'news_id' => $count_news,
          );

          $this->Insert_news_model->insert_news($data);
          $this->Insert_news_model->insert_cate_news($data_cate);
          $this->Insert_news_model->insert_subcate_news($data_subcate);
          echo "insert:  " . $title . "<br/>";
        } else {
          echo "not insert: " . $title . "<br/>";
        }
        
      }
    }
    // check news hot and save news for hot_news =1
    $day        = date('d');
    $month      = date('m');
    $year       = date('Y');
    $currentDay = "";
    
    if ($day > 5) {
      $day -= 5;
      $currentDay = $year . "-" . $month . "-" . $day;
      $key_hot    = $this->Get_news_model->get_hot_key();
      foreach ($key_hot as $key) {
        $key      = $key['hot_key'];
        $hot_data = $this->Insert_news_model->check_hot_news($key, $currentDay);
        foreach ($hot_data as $data) {
          $id   = $data['news_id'];
          $data = array(
            'hot_news' => 1
          );
          $this->Update_news_model->update_hot_news($data, $id);
        }
      }
      $day+=1;
      $removeDay = $year . "-" . $month . "-" . $day;
      $remove = $this->Insert_news_model->remove_hot_news($removeDay);
      foreach ($remove as $news) {
        $id   = $news['news_id'];
        $data = array(
          'hot_news' => 0
        );
        echo "remove thanh cong";
        $this->Update_news_model->update_hot_news($data, $id);
      }
    } elseif ($day <= 5) {
      if ($month == 1) {
        $month      = 12;
        $numberDay  = $this->Insert_news_model->checkMonth($month);
        $day        = $numberDay - 5 + $day;
        $year       = $year - 1;
        $currentDay = $year . "-" . $month . "-" . $day;
        $key_hot    = $this->Get_news_model->get_hot_key();
        foreach ($key_hot as $key) {
          $key      = $key['hot_key'];
          $hot_data = $this->Insert_news_model->check_hot_news($key, $currentDay);
          foreach ($hot_data as $data) {
            $id   = $data['news_id'];
            $data = array(
              'hot_news' => 1
            );
            $this->Update_news_model->update_hot_news($data, $id);
          }
        }
        //remove hot_new =1
        $day+=1;
        $removeDay = $year . "-" . $month . "-" . $day;
        $remove = $this->Insert_news_model->remove_hot_news($removeDay);
        foreach ($remove as $news) {
          $id   = $news['news_id'];
          $data = array(
            'hot_news' => 0
          );
          $this->Update_news_model->update_hot_news($data, $id);
        }
      } else { 
        $month      = $month - 1;
        $numberDay  = $this->Insert_news_model->checkMonth($month);
        echo "numberDay".$numberDay."<br/>";
        $day        = $numberDay - 5 + $day;
        $currentDay = $year . "-" . $month . "-" . $day;
        echo "currentDay".$currentDay."<br/>";
        $key_hot    = $this->Get_news_model->get_hot_key();
        foreach ($key_hot as $key) {
          $key      = $key['hot_key'];
          $hot_data = $this->Insert_news_model->check_hot_news($key, $currentDay);
          foreach ($hot_data as $data) {
            $id   = $data['news_id'];
            $data = array(
              'hot_news' => 1
            );
            $this->Update_news_model->update_hot_news($data, $id);
          }
        }
        //remove hot_new =1
        $day+=1;
        $removeDay = $year . "-" . $month . "-" . $day;
        $remove = $this->Insert_news_model->remove_hot_news($removeDay);
        foreach ($remove as $news) {
          $id   = $news['news_id'];
          $data = array(
            'hot_news' => 0
          );
          $this->Update_news_model->update_hot_news($data, $id);
        }
      }
    }
  }
}
?>