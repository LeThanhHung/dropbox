<?php 
class Json_load_ajax extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Get_news_model');
	}
	public function index(){
		$news_id = $_GET["news_id"];
      	$data = $this->Get_news_model->get_news_details($news_id);
       	echo json_encode($data);
	}
}
?>