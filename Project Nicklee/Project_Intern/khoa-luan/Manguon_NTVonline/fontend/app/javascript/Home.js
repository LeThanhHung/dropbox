var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var currentSelectedIndexLeft = 0;

var Home = {

};


function loadDate(){
var d = new Date();
var days = new Array("Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy");
document.getElementById("days").innerHTML = days[d.getDay()];
}
Home.onLoad = function() {
    this.enableKeys();
    widgetAPI.sendReadyEvent();
    
    //Display days
    loadDate();
};

Home.onUnload = function() {

};

Home.enableKeys = function() {
    document.getElementById("anchor").focus();

};

Home.keyDown = function() {
    var keyCode = event.keyCode;
    alert("Key pressed: " + keyCode);

    switch (keyCode) {
        case tvKey.KEY_RETURN:
        case tvKey.KEY_PANEL_RETURN:
            alert("RETURN");
            widgetAPI.sendReturnEvent();
            break;
        case tvKey.KEY_UP:
            alert("UP");
            break;
        case tvKey.KEY_DOWN:
            alert("DOWN");
            break;
        case tvKey.KEY_RED:
            alert("RED");
            break;
        case tvKey.KEY_GREEN:
            alert("GREEN");
            break;
        case tvKey.KEY_YELLOW:
            alert("YELLOW");
            break;
        case tvKey.KEY_BLUE:
            alert("BLUE");
            break;
        case tvKey.KEY_ENTER:
        case tvKey.KEY_PANEL_ENTER:
            alert("ENTER");
             window.location = 'categories.html';
            break;
        default:
            alert("Unhandled key");
            break;
    }
};