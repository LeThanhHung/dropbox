var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var indexLink = 3;
var Details = {

};

//write cookie
function writeCookie(name, value, days) {
        var date, expires;
        if (days) {
            date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

// reading Cookie
function readCookie(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for (i = 0; i < ca.length; i++) {
        c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return '';
}


// function load news details
function loadNewsDetails(news_id) {
    $.ajax({
        type: 'GET',
        url: 'http://192.168.56.1/backend/Json_load_ajax',
        data: 'news_id=' + news_id,
        dataType: 'json',
        success: function(data) {
            document.getElementById("anchor").focus();
            alert('success');
            var html;
            html = '<div class="content row">';
            html += '<div class="col-sm-9 title-post">';
            html += '<h3>' + data.title + '</h3>';
            html += '<a href="javascript:void(0)" class="pull-left">' + data.source_name + '</a>';
            html += '<a href="javascript:void(0)" class="pull-right">' + data.create_date + '</a>';
            html += '</div>';
            html += '<div class="col-sm-3 image-post">';
            html += '<img alt="image post" src="' + data.image + '">';
            html += ' </div>';
            html += ' </div>';
            html += '<div class="row">';
            html += '<div class="col-sm-12 text-news">';
            html += data.content;
            html += '</div>';
            html += '</div>';
            $('#Default').append($(html));
        }
    });
}

function numberRead(id) {
    $.ajax({
        type: 'GET',
        url: 'http://192.168.56.1/backend/Number_read',
        data: 'id=' + id,
        success: function(data) {
           alert('success:');
        },
        error: function() {
            alert('Error function newsCount1');
        }
    });

}

Details.onLoad = function() {
    // Enable key event processing
    this.enableKeys();
    widgetAPI.sendReadyEvent();
    indexLink = readCookie('indexLink');
    var linkDtails = window.location.href;
    var news_id = linkDtails.split('=');
    news_id = news_id[1];
    numberRead(news_id);
    loadNewsDetails(news_id);

};


Details.onUnload = function() {

};

Details.enableKeys = function() {
    document.getElementById("anchor").focus();
};

Details.keyDown = function() {
    var keyCode = event.keyCode;
    alert("Key pressed: " + keyCode);
    switch (keyCode) {
        case tvKey.KEY_RETURN:
        case tvKey.KEY_PANEL_RETURN:
            alert("RETURN");
            widgetAPI.blockNavigation(event);
            history.go(-1);
            
            break;

        case tvKey.KEY_UP:
            alert("UP");
            if ($("#Default").scrollTop() - 50 < 0) {
                $("#Default").scrollTop(0);
            } else {
                $("#Default").scrollTop($("#Default").scrollTop() - 50);
            }
            $('#Default').perfectScrollbar('update');
            break;
        case tvKey.KEY_DOWN:
            alert("DOWN");
            if ($("#Default").scrollTop() + 50 > $("#Default").prop('scrollHeight') - $("#Default").height()) {
                $("#Default").scrollTop($("#Default").prop('scrollHeight') - $("#Default").height());
            } else {
                $("#Default").scrollTop($("#Default").scrollTop() + 50);
            }
            $('#Default').perfectScrollbar('update');
            break;
        case tvKey.KEY_RED:
            alert("RED");
            window.location = 'categories.html';
            break;
        case tvKey.KEY_GREEN:
            alert("GREEN");
            window.location = 'numberRead.html';
            break;
        case tvKey.KEY_YELLOW:
            alert("YELLOW");
            window.location = 'sources.html';
            break;
        case tvKey.KEY_BLUE:
            alert("BLUE");
            break;
        case tvKey.KEY_ENTER:
        case tvKey.KEY_PANEL_ENTER:
            alert("ENTER");
            writeCookie(indexLink++, window.location, null);
            writeCookie('indexLink', indexLink, null);
            break;
        default:
            alert("Unhandled key");
            break;
    }
};