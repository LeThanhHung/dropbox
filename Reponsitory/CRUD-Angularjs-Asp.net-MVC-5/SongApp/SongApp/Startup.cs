﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SongApp.Startup))]
namespace SongApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
