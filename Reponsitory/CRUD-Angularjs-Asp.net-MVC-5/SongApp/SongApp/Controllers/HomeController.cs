﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SongApp.Controllers
{
    public class HomeController : Controller
    {
        public MusicDBEntities MusicModel { get; set; }

        public HomeController()
        {
            if (MusicModel == null)
            {
                MusicModel = new MusicDBEntities();
            }
        }

        public ActionResult Index()
        {
            var artistList = MusicModel.Artists;

            return View(artistList.ToList());
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var artist = MusicModel.Artists.Where(i => i.ArtistId == id).FirstOrDefault();

            if (artist !=  null)
            {
                return View(artist);
            }

            throw new Exception("Artist no found");
        }

        [HttpPost]
        public ActionResult Edit(Artist artist)
        {
            if (artist.ArtistId == 0)
            {
                // Since it didn't have a ProductID, we assume this
                // is a new Product
                MusicModel.Artists.Add(artist);
            }
            else
            {
                // Since EF doesn't know about this product (it was instantiated by
                // the ModelBinder and not EF itself, we need to tell EF that the
                // object exists and that it is a modified copy of an existing row
                MusicModel.Entry(artist).State = System.Data.Entity.EntityState.Modified;
                MusicModel.SaveChanges();
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Add()
        {
            if (MusicModel == null)
            {
                MusicModel = new MusicDBEntities();
            }

            //MusicModel.Artists.Add(new Artist { Name = "David" });

            //MusicModel.Songs.Add(new Song { Name = "Hello", Artist = new Artist { Name = "Linda" } });
            MusicModel.Songs.Add(new Song { Name = "Hello1", ArtistId = 1 });

            MusicModel.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}