﻿var ViewReportScanController = function ($scope, $http, $rootScope, users, histories, $route, $routeParams, signalRHubProxy) {
    $scope.$routeParams = $routeParams;
    $scope.historyId = $routeParams.historyId;
    $scope.history;
    $scope.listhistory;
    $scope.$parent.IsProfile = false;
    $scope.SearchValue = "";

    histories.viewreport($scope.historyId).then(function (result) {
        if (result != undefined) {
            $scope.history = result.History;
            $scope.listhistory = result;
        }
    });
    $scope.categoryData = [];
    histories.getSummnaryReport($scope.historyId).then(function (result) {
        $scope.categoryData = result;
    })

    InitTable();
    function InitTable() {
        $scope.aoColumns = [
            {
                "sTitle": "Arquivo",
                "sClass": "col-md-1",
                "mRender": function (data, type, obj)
                {
                    return "<span class=\"pd-l-sm\"></span>"+data;
                }
            },
            {
                "sTitle": "Volume",
                "sClass": "col-md-1",
            },
             {
                 "sTitle": "Estado",
                 "sClass": "col-md-1",
                 "mRender": function (data, type, oObj) { 
                     var returnValue = oObj.ScanResult != undefined && oObj.ScanResult.Scan_all_result_i > 0 ? "<span class=\"label label-danger\">Infectado</span>" : oObj.ScanResult != undefined && oObj.ScanResult.In_queue ? "<span class=\"label label-warning\">Na Fila</span>" : "<span class=\"label label-success\">Limpo</span>";
                     return returnValue;
                 }
             },
             {
                 "sTitle": "Progresso",
                 "sClass": "col-md-2",
                 "mRender": function (data, type, oObj) {
                     //if (oObj.ScanResult != undefined && oObj.ScanResult.In_queue) {
                     return "<div class=\"progress\"><div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"" + data + "\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: " + data + "%\"><span class=\"sr-only\">" + data + "% Complete (success)</span></div></div>";
                     //}
                     //return "<div class=\"progress progress-sm no-m\"><div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"00\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 00%\"><span class=\"sr-only\">0% Complete (success)</span></div></div>";
                 }
             },
           {
               "sTitle": "Relatório Unitário",
               "sClass": "col-md-2",
               "mRender": function (data, type, oObj) {
                   var action='';
                   if (oObj.ScanResult != undefined && !oObj.ScanResult.In_queue) {
                       var result = oObj.ScanResult.Scan_all_result_a != '' ? oObj.ScanResult.Scan_all_result_a : 'In Queue';
                       action = "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-primary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().ViewDetail('" + data + "','" + oObj.ScanDetail.FileName + "'," + oObj.ScanDetail.Volume + ",'" + result + "')\"><i class=\"ti-more\"></i> Ver</button>";
                   }
                   if (oObj.ScanResult != undefined && (oObj.ScanResult.In_queue || oObj.ScanResult.Progress_percentage < 100)) {
                       action += "<button   id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-success btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().ReScan('" + oObj.ScanDetail.Id + "','" + oObj.ScanResult.Id + "')\"><i class=\"ti-reload\"></i> Digitalizar Novamente</button>";
                   }

                   return action;
               }
           }
        ];

        $scope.aoColumnDefs = [
                                { "mDataProp": "ScanDetail.FileName", "aTargets": [0] },
                                { "mDataProp": "ScanDetail.Volume", "aTargets": [1] },
                                 { "mDataProp": "ScanDetail.IsScaned", "aTargets": [2] },
                                { "mDataProp": "ScanResult.Progress_percentage", "aTargets": [3] },
                                { "mDataProp": "ScanResult.Id", "aTargets": [4] }
        ];
        //end config
    }

    $scope.ViewDetail = function (idfile,filename,filesize,scanresult)
    {
        window.location.href = "/Home/Index#/Home/ViewDetailScan/" + idfile + "/p/" + $scope.historyId + "/n/" + filename + "/s/" + filesize + '/r/' + scanresult;
    }

    $scope.ReScan = function (scandetailId, scanResultId)
    {
        histories.rescan(scandetailId, scanResultId).then(function (result) {
            //if (result == "true")
            {
                histories.viewreport($scope.historyId).then(function (result) {
                    if (result != undefined) {
                        $scope.history = result.History;
                        $scope.listhistory = result;
                    }
                });
            }
        })
    }

    $scope.$watch('Master.IsChange', function () {
        //load result again
        histories.viewreport($scope.historyId).then(function (result) {
            if (result != undefined) {
                $scope.history = result.History;
                $scope.listhistory = result;
            }
        });
    }, true);

    $scope.Cancel = function ()
    {
        window.location.href = "/Home/Index#/Home/ScanProgress";
    }

};

window.app.directive('chartCategory', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            $.plot(el, [scope.$eval(attrs.source)], {
                colors: ['#24ACE5'],
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.5,
                        align: 'center',
                        fill: 1,
                    },
                    shadowSize: 0
                },
                grid: {
                    color: '#c2c2c2',
                    borderColor: '#f0f0f0',
                    borderWidth: 1
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0,
                    xLabelAngle:  -90,
                }
            });
            // watch for any changes to our data, rebuild the DataTable
            scope.$watch(attrs.source, function (value) {
                //var val = value !=undefined ;
                if (value != undefined) {
                    $.plot(el, [value], {
                        colors: ['#24ACE5'],
                        series: {
                            bars: {
                                show: true,
                                barWidth: 0.5,
                                align: 'center',
                                fill: 1,
                            },
                            shadowSize: 0
                        },
                        grid: {
                            color: '#c2c2c2',
                            borderColor: '#f0f0f0',
                            borderWidth: 1
                        },
                        xaxis: {
                            mode: "categories",
                            tickLength: 0,
                            xLabelAngle: -90,
                        }
                    });
                }
            });
        }
    };
})

