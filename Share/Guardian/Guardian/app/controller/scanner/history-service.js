﻿
app.factory('histories', ['$http', '$q', function ($http, $q) {
    var cachegentity;
    return {
        getEntity: function () { return cachegentity; },
        setEntity: function (entity) { cachegentity = entity; }, 
        insert: function (entity) {
            var path = '/ScannerFeature/InsertHistory';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        edit: function (entity) {
            var path = '/ScannerFeature/UpdateHistory';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        del: function (entity) {
            var path = '/ScannerFeature/DeleteHistory';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        list: function () {
            var path = '/ScannerFeature/GetListHistory';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path, 
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        checkesixtName: function (name) {
            var path = '/ScannerFeature/CheckExistName';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'name': name },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        execscan: function (entity) {
            var path = '/ScannerFeature/ExecuteScan';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                console.log(data);
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data); 
            });
            return qdefer.promise;
        },
        viewreport: function (historyId) {
            var path = '/ScannerFeature/GetViewReportById';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'historyId': historyId },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        getSummnaryReport: function (historyId) {
            var path = '/ScannerFeature/GetSummaryReportScanById';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'historyId': historyId },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        getScanResultById: function (fileId) {
            var path = '/ScannerFeature/GetScanResultById';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'scanresultid': fileId },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        getScanResultDetails: function (fileId) {
            var path = '/ScannerFeature/GetScanResultDetailByParent';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'scanresultid': fileId },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        rescan: function (scandetailId,scanresultId)
        {
            var path = '/ScannerFeature/Rescan';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'scanDetailId': scandetailId, 'scanResultId': scanresultId },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        deleteAll: function (list)
        {
            var path = '/ScannerFeature/DeleteHistories';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'histories': list },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        }
    }

}]);