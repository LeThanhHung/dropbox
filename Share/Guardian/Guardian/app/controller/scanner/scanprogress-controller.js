﻿var ScanProgressController = function ($scope, $http, $rootScope, users, histories, $modal) {
    $scope.SearchValue = "";
    $scope.Submitted = false;
    $scope.IsLookingup = false;
    $scope.listhistory;
    $scope.$parent.IsProfile = false;
    var minDate = new Date();
    minDate.setFullYear(1, 0, 1);
    //convert min to string to show on the completed time
    function ConvertMintoHourString(totalMIn, oObj) {
        if (oObj.History.IsCompleted) {
            if (totalMIn != undefined) {
                if (totalMIn < 60 && totalMIn > 0) {
                    //   return 'em ' + totalMIn + ' min';
                }
                else if (totalMIn > 60 && totalMIn > 0) {
                    var totalHours = Math.floor(totalMIn / 60);
                    var remaindMin = totalMIn % 60;
                    //    return 'em ' + totalHours + 'Horas ' + remaindMin + ' min';
                }
            }
            //  return ' em menos de 1 min';
        }
        return '';
    }
    //intital the table result
    function InitTable() {
        $scope.aoColumns = [
            {
                "sTitle": "ID",
                "sClass": "col-md-2"
            },
               {
                   "sTitle": "",
                   "sClass": "col-md-1",
                   "mRender": function (data, type, oObj) {
                       var returnValue = " <input type=\"checkbox\" class=\"rows-check\" id='"+oObj.History.Id+"'  onclick=\"angular.element(this).scope().CheckDeleteChange('" + oObj.History.Id + "')\"/>";
                       return returnValue;
                   }
               },
            {
                "sTitle": "Data",
                "sClass": "col-md-2"
            },

             {
                 "sTitle": "Estado",
                 "sClass": "col-md-1",
                 "mRender": function (data, type, oObj) {
                     var returnValue = '';
                     if (oObj.History.IsScanning) {
                         returnValue = !data ? " <span class=\"label label-warning\">Carregando</span>  <img id=\"" + oObj.History.Id + "\"   src=\"/Themes/img/loading.gif\" >" : "";
                     }
                     if (oObj.History.IsCompleted) {
                         returnValue = "<span class=\"label label-success\">Concluído " + ConvertMintoHourString(oObj.History.TotalTimeScan, oObj) + "</span>";
                     }
                     return returnValue;
                 }
             },
           {
               "sTitle": "Relatório",
               "mRender": function (data, type, oObj) {
                   var action = "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-primary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().ViewReport('" + data + "')\"><i class=\"ti-search\"></i> Ver</button>";

                   if (!oObj.History.IsCompleted) {
                       if (oObj.History.IsScanning) {
                           action = "<button disabled=\"disabled\" id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-trimary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().Scan('" + data + "')\"><i class=\"ti-search\"></i> Ver</button>";
                       }
                       else {
                           action = "<button   id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-trimary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().Scan('" + data + "')\"><i class=\"ti-search\"></i> Ver</button>";
                       }
                   }

                   return action;
               }
           }
        ];

        $scope.aoColumnDefs = [
                                { "mDataProp": "History.Name", "aTargets": [0] },
                                { "mDataProp": "History.Id", "aTargets": [1] },
                                { "mDataProp": "History.CreatedDate", "aTargets": [2] },
                                { "mDataProp": "History.IsCompleted", "aTargets": [3] },
                                { "mDataProp": "History.Id", "aTargets": [4] }
        ];
        //end config
    }
    //load list history to show to table
    function loadlist() {
        //load list history
        histories.list().then(function (result) {
            if (result != undefined && result.length > 0) {
                $scope.listhistory = result;
                for (var i = 0 ; i < $scope.listhistory.length ; i++) {
                    $scope.listhistory[i].History.IsCheck = false;
                }
            }
            else { $scope.listhistory = null; }
        });
    }
    //verifi ftp account
    function VerifyAccount(currentUser) {
        if (currentUser == undefined && (currentUser != undefined && !currentUser.IsVerifiedFtpAccount)) {
            alert('Conta inválida , tente novamente.');
            window.location.href = "/Home/Index#/Home/RegisterOnlineScan"
        }
    }

    $scope.CheckDeleteChange = function (id) {
        for (var i = 0 ; i < $scope.listhistory.length; i++) {
            if ($scope.listhistory[i].History.Id == id) {
                $scope.listhistory[i].History.IsCheck = !$scope.listhistory[i].History.IsCheck;
                return;
            }
        }
    }

    $scope.CallBack = function(event , sender)
    {
        if (sender != undefined)
        {
            var id = $(sender).attr("id");
            if (id != undefined && id.length > 0)
            {
                $scope.CheckDeleteChange(id);
            }
        }
    }

    

    $scope.DeleteAll = function ()
    {
        var list = [];
        for (var i = 0 ; i < $scope.listhistory.length; i++)
        {
            if ($scope.listhistory[i].History.IsCheck)
            {
                list.push($scope.listhistory[i].History);
            }
        }
        if (list != undefined && list.length > 0) {
            histories.deleteAll(list).then(function (result) {
                if (result) {   
                    loadlist();
                }
            });
        }
        else {
            alert("please choose at least one item.");
        }
    }

    $scope.Refresh = function ()
    {
        loadlist();
    }

    //view detail information of scan
    $scope.ViewDetail = function (id) {
        var u = $.grep($scope.listhistory, function (e) { return e.Id == id; });
        var entity = u[0];
        if (entity != undefined) {
        }
    }
    //current user
    $scope.currentuser = {
    };
    $scope.entity = { Name: '' };
    //get current user
    users.getCurrentUser().then(function (result) {
        $scope.currentuser = result;
        VerifyAccount(result);
    });

    $scope.AddHistory = function () {
        var modalInstance = $modal.open({
            templateUrl: '/Home/AddNewHistory',
            controller: 'HistoryModalController'
        });
        //modal opened
        modalInstance.opened.then(function () {
            //ComponentsFormTools.init();
        });
        //reload list
        modalInstance.result.then(function (result) {
            if (result != undefined) {
                var entityId = result.Id;
                //reload list History 
                histories.list().then(function (result) {
                    $scope.listhistory = result;
                    //execute scan immediater 
                    $scope.Scan(entityId);
                });
            }
        });
    }

    //execute a scan which is chose by user
    $scope.Scan = function (id) {
        var u = $.grep($scope.listhistory, function (e) { return e.History.Id == id; });
        var entity = u[0].History;
        if (entity != undefined) {
            //update status
            entity.IsScanning = true;
            //remove this load =  
            $("#site_statistics_loading").remove();
            histories.edit(entity).then(function (result) {
                loadlist();
                //execute scan 
                histories.execscan(entity).then(function (result) {
                    if (result.IsSuccess) {
                        //go to summary scan
                        $scope.ViewReport(entity.Id);
                    }
                    else {

                        loadlist();
                    }
                    //load notification
                    $scope.$parent.LoadListNotification();
                    $("body").append("<div id='site_statistics_loading' style='display: none;'>   <img alt='loading' src=\"/Themes/img/loading.gif\" style=\"position: absolute; left: 50%; top: 50%;\"></div>");
                });
            });
        }
    }
    // view result of scan to user
    $scope.ViewReport = function (id) {
        window.location.href = "/Home/Index#/Home/ViewReportScan/" + id;
    }
    //call initial data 
    InitTable();
    loadlist();
}

//this controller apply to modal create new scan
var HistoryModalController = function ($scope, $http, $modalInstance, histories) {
    $scope.entity = { Name: '' };
    $scope.Submitted = false;
    //save the new scan schedule
    $scope.Save = function (isvalid) {
        $scope.Submitted = true;
        if (isvalid) {
            histories.insert($scope.entity).then(function (result) {
                if (result != undefined && result != "false") {
                    $modalInstance.close(result);
                }
                else {
                    alert("Ocorreu erro durante a digitalização criar uma lista de discussão , por favor, feche o formulário e criar novamente ou administrador de contato para suporte.");
                }
            });
        }
    }

    $scope.Cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

