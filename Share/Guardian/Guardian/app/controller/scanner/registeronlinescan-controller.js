﻿var RegisterOnlineScanController = function ($scope, $http, users) {
    $scope.$parent.IsProfile = false;
    $scope.Submitted = false;
    $scope.FirstTimeVerify = true;
    //current user
    $scope.currentuser = { 
    };
    //get current user
    users.getCurrentUser().then(function (result) {
        $scope.currentuser = result;
        $scope.FirstTimeVerify = !(($scope.currentuser.FtpPort != undefined && $scope.currentuser.FtpPort != '') && ($scope.currentuser.FtpServer != undefined && $scope.currentuser.FtpServer != '')
            && ($scope.currentuser.FtpAccount != undefined && $scope.currentuser.FtpAccount != '') && ($scope.currentuser.FtpPassword != undefined && $scope.currentuser.FtpPassword != ''));
        //set default port 21
        if ($scope.currentuser.FtpPort == undefined || $scope.currentuser.FtpPort == '') {
            $scope.currentuser.FtpPort =21;
        }
    });



    $scope.finish = function (isvalid) {
        $scope.Submitted = true;
        if (isvalid) { 
            users.validateFtpAccount($scope.currentuser).then(function (res) {
                $scope.FirstTimeVerify = false;
                if (res !== 'true') { 
                    $scope.currentuser.IsVerifiedFtpAccount = false;
                }
                else { $scope.currentuser.IsVerifiedFtpAccount = true; }
                //update user
                users.edit($scope.currentuser).then(function (data) {
                    if ($scope.currentuser.IsVerifiedFtpAccount) {
                        //redirect to scan form 
                        window.location.href = "/Home/Index#/Home/ScanProgress";
                    }
                });
                //end update user
            });
        }
    }

    $scope.scan = function () {
        var url = '/ScannerFeature/ScanFile'
        users.getListFTPFiles($scope.currentuser).then(function (result) {
            var listfiles = result;
        });
    } 
 
}


app.directive('stepy', ['users',function (users) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs, ctrl) {
            demoFormWizard.init(attrs.id, scope,users);
        }
    }
}]);

var demoFormWizard = function (id, scope,users) {
    return {
        init: function (id, scope,users) {
            $("#" + id).stepy({
                backLabel: "Previous",
                nextLabel: "Next",
                errorImage: false,
                block: true,
                validate: false,
                next: function (obj, index) {
                    //next step to scan 
                    if (obj == 2)
                    {
                       
                    }
                },
                 finish: function (obj, index) {
                     return false;
                 }
            });
           
        }
    };
}();