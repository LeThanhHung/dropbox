﻿app.factory('scandetails', ['$http', '$q', function ($http, $q) {
    var cachegentity;
    return {
        getEntity: function () { return cachegentity; },
        setEntity: function (entity) { cachegentity = entity; },
        insert: function (entity) {
            var path = '/ScannerFeature/InsertScanDetail';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        edit: function (entity) {
            var path = '/ScannerFeature/UpdateScanDetail';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        del: function (entity) {
            var path = '/ScannerFeature/DeleteScanDetail';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'entity': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        list: function () {
            var path = '/ScannerFeature/GetListScanDetail';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path, 
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
    }

}]);