﻿app.factory('restorehistorydetails', ['$http', '$q', function ($http, $q) {
    return {
        list: function (id) {
            var path = '/ImageChecker/GetListRestoreHistoryDetailByRestoreId';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { imageCheckerRestoreId: id },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
    }
}]);