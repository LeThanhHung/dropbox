﻿app.factory('imagecheckerhistories', ['$http', '$q', function ($http, $q) {
    return {
        list: function () {
            var path = '/ImageChecker/GetListImageCheckerRestore';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        }, 
    }
}]);