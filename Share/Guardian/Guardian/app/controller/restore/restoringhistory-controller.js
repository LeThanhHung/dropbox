﻿var RestoringHistoryController = function ($scope, $http, imagecheckerhistories)
{
    $scope.$parent.IsProfile = false;
    $scope.listimagecheckerrestore;
    $scope.SearchValue = "";
    loadlist();
    InitTable();

    function loadlist()
    {
        imagecheckerhistories.list().then(function (result) {
            if(result!=undefined && result.length>0)
            $scope.listimagecheckerrestore = result;
        });
    }    
    //intital the table result
    function InitTable() {
        $scope.aoColumns = [
            {
                "sTitle": "Data",
                "sClass": "col-ms-3"
            },
             {
                 "sTitle": "Estado",
                 "sClass": "col-ms-3",
                 "mRender": function (data, type, oObj) {
                     var returnValue = oObj.IsSuccess ? " <span class=\"label label-success\">Concluído</span>" : " <span class=\"label label-danger\">Erro</span>";
                     return returnValue;
                 }
             },
              {
                  "sTitle": "Retorno",
                  "sClass": "col-ms-3"
              },
            {
                "sTitle": "Relatório",
                "sClass": "col-lg-3",
                "mRender": function (data, type, oObj) {
                    var action = "<button id=\"btn_" + data + "\" type=\"button\" class=\"btn btn-primary btn-sm loading-demo mr5\" onclick=\"angular.element(this).scope().ViewDetailRestore('" + data + "')\"><i class=\"ti-search\"></i> Exibir</button>";
                    return action;
                }
            }
        ];

        $scope.aoColumnDefs = [
                                { "mDataProp": "RestoredDate", "aTargets": [0] },
                                { "mDataProp": "IsSuccess ", "aTargets": [1] },
                                { "mDataProp": "ErrorMessage", "aTargets": [2] },
                                { "mDataProp": "Id", "aTargets": [3] }
       
        ];
        //end config
    }

    $scope.ViewDetailRestore = function (id)
    {
        window.location.href = "/Home/Index#/Home/RestoreHistoryDetail/" + id;
    }
}

