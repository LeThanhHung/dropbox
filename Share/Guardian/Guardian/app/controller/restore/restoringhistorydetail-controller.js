﻿var RestoreHistoryDetailController = function ($scope, $http, restorehistorydetails,$route, $routeParams)
{
    $scope.restoreHistoryId = $routeParams.restoreId;
    $scope.$parent.IsProfile = false;
    $scope.SearchValue = "";
    $scope.restorehistorydetailList;

    InitTable();
    restorehistorydetails.list($scope.restoreHistoryId).then(function (result) {
        if (result != undefined && result.length > 0) {
            $scope.restorehistorydetailList = result;
        }
    });

    //intital the table result
    function InitTable() {
        $scope.aoColumns = [
            {
                "sTitle": "Data",
                "sClass": "col-ms-3"
            },
             {
                 "sTitle": "Estado",
                 "sClass": "col-ms-1",
                 "mRender": function (data, type, oObj) {
                     var returnValue = oObj.IsSuccess ? " <span class=\"label label-success\">Sucesso</span>" : " <span class=\"label label-danger\">Erro</span>";
                     return returnValue;
                 }
             },
             {
                 "sTitle": "Propriedade",
                 "sClass": "col-ms-3"
             },
              {
                  "sTitle": "Mensagem",
                  "sClass": "col-ms-3"
              },  
        ];

        $scope.aoColumnDefs = [
                                { "mDataProp": "RestoreDate", "aTargets": [0] },
                                { "mDataProp": "IsSuccess ", "aTargets": [1] },
                                 { "mDataProp": "PathFileServer", "aTargets": [2] },
                                { "mDataProp": "ErrorMessage", "aTargets": [3] },
                                

        ];
        //end config
    }
}