﻿var LoginController = function ($http, $scope, users)
{
    $scope.LoginUser = {username:'', password:'', IsSuccess:true ,IsRemember:false};
    $scope.Submitted = false;

    //execute login for user
    $scope.Login = function(valid)
    {
        $scope.Submitted = true;
        if (valid)
        {
            users.login($scope.LoginUser).then(function (data) {
                if (data === 'false') {
                    $scope.LoginUser.IsSuccess = false;
                }
                else {
                    //redirect to Index page
                    window.location.href = "/";
                }
            });
        }
    }
    
}