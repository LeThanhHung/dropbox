﻿app.factory('users', ['$http', '$q', function ($http, $q) {
    var cachegentity;
    return {
        getEntity: function () { return cachegentity; },
        setEntity: function (entity) { cachegentity = entity; },
        logoff: function () {
            var path = '/Account/LogOff';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        login: function (entity) {
            var path = '/Account/Login';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: entity,
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        insert: function (entity) {
            var path = '/Account/RegisterUser';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'model': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        edit: function (entity) {
            var path = '/Account/EditProfile';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'user': entity },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        getCurrentUser: function () {
            var path = '/Account/GetCurrentUser';
            var qdefer = $q.defer();
            $http({
                method: 'GET',
                url: path,
                async: false,
            }).success(function (data, status, headers, config) {
                if (data != undefined && (data.PathLogo == undefined || (data.PathLogo != undefined && data.PathLogo.length <= 0))) {
                    data.PathLogo = '/Scripts/SecondTheme/images/users/user-35.png'
                }
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        getUserInfo: function (accesstoken, domain) {
            var path = '/Account/GetSealInfo';
            var qdefer = $q.defer();
            $http({
                method: 'POST',
                url: path,
                data: { 'accesstoken': accesstoken, 'domain': domain },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(data);
            });
            return qdefer.promise;
        },
        validateFtpAccount: function (currentuser) {
            var qdefer = $q.defer();
            if (currentuser != undefined) {
                $http({
                    method: 'POST',
                    url: '/ScannerFeature/VerifyFtpAccount',
                    data: { servername: currentuser.FtpServer, username: currentuser.FtpAccount, password: currentuser.FtpPassword, port: currentuser.FtpPort, initalFolder: currentuser.FtpFolder },
                    async: false,
                }).success(function (data, status, headers, config) {
                    qdefer.resolve(data);
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
            }
            return qdefer.promise;
        },
        getListFTPFiles: function (currentuser) {
            var qdefer = $q.defer();
            if (currentuser != undefined) {
                $http({
                    method: 'POST',
                    url: '/ScannerFeature/GetFTPListFile',
                    data: { servername: currentuser.FtpServer, username: currentuser.FtpAccount, password: currentuser.FtpPassword, port: currentuser.FtpPort, initalFolder: currentuser.FtpFolder },
                    async: false,
                }).success(function (data, status, headers, config) {
                    qdefer.resolve(data);
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
            }
            return qdefer.promise;
        },
        getListNotification: function () {
            var qdefer = $q.defer();

            $http({
                method: 'POST',
                url: '/ImageChecker/GetNotificationByCurrentUser',
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });

            return qdefer.promise;
        },
        updateIsReadNotification: function (imagecheckerrestore) {
            var qdefer = $q.defer();

            $http({
                method: 'POST',
                url: '/ImageChecker/MarkIsReadNotification',
                data: { entity: imagecheckerrestore },
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });

            return qdefer.promise;
        } ,  
        clearNotifications:function()
        {      
            var qdefer = $q.defer();    
            $http({
                method: 'POST',
                url: '/ImageChecker/ClearNotifications',   
                async: false,
            }).success(function (data, status, headers, config) {
                qdefer.resolve(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });

            return qdefer.promise;
        }
    }

}]);