﻿var RegisterController = function ($http, $scope, users, masters) {
    $scope.Member = { 'TypeOfRegister': 1, 'SealType': 'Standard' };
    $scope.$parent.IsProfile = false;
    $scope.Submitted = false;
    $scope.SealTypes = masters.getsealtype();
    $scope.Save = function (valid) {
        $scope.Submitted = true;
        if (valid) {
            //save to db 
            users.insert($scope.Member).then(function (data) {
                if (data) {
                    //redirect to home page and login for user.
                    window.location.href = '/Account/Index#/Account/Login';
                }
                else {
                    alert('occurred error during create new user, please contact to administrator for supporting.');
                }
            })
             
        }
    }
    
}

//validate Email existed
app.directive('validEmail', function ($http, $resource, $timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            var stop_timeout;
            return scope.$watch(function () {
                return ngModel.$modelValue;
            }, function (name) {
                $timeout.cancel(stop_timeout);

                if (name === '')
                    return ngModel.$setValidity('validemail', true);

                //validate format email 
                var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var partt = new RegExp(regex);
                if (partt.test(name)) {
                    //validate exist email
                    $http({
                        method: 'POST',
                        url: '/Account/CheckExistEmail',
                        data: { email: name },
                        async: false,
                    }).success(function (data, status, headers, config) {
                        stop_timeout = $timeout(function () {
                            return ngModel.$setValidity('validemail', data==='true');
                        })
                    }).error(function (data, status, headers, config) {
                        return ngModel.$setValidity('validemail', false);
                    });
                }
                else {
                    return ngModel.$setValidity('validemail', false);
                }
            });
        }
    }
});

//compare password
app.directive('comparePassword', function ($http, $resource, $timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            var stop_timeout;
            return scope.$watch(function () {
                return ngModel.$modelValue;
            }, function (name) {
                $timeout.cancel(stop_timeout);

                if (name === '')
                    return ngModel.$setValidity('comparepassword', true);

                var comparewith = scope.$eval(attrs.comparewith);
                if (comparewith === '' || comparewith== undefined)
                    return ngModel.$setValidity('comparepassword', true); 

                return ngModel.$setValidity('comparepassword',comparewith === name);
                 
            });
        }
    }
});