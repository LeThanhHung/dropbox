﻿var SealController = function ($http, $scope, users, $route, $routeParams) {
    $scope.$routeParams = $routeParams;
    $scope.UserInfo;
    $scope.IsShow = false;
    
    var accesstoken = $routeParams.accesstoken;
    var domain = $routeParams.domain;
    if (accesstoken != '' && domain != '') {
        users.getUserInfo(accesstoken, domain).then(function (result) {
            $scope.UserInfo = result;            
            $scope.IsShow =  result == undefined && $scope.UserInfo.SealStatus != 'Invalid' ;
            if (!$scope.IsShow) { this.close();}
        })
    }
}