﻿var GenerateScriptController = function ($http, $scope, users, $route) {
    $scope.Submitted = false;
    $scope.Script = "";
    $scope.$parent.IsProfile = false;

    users.getCurrentUser().then(function (result) { $scope.currentUser = result; });
    //execute login for user
    $scope.Generate = function () {
        if ($scope.currentUser != undefined) {
            $scope.Script = generatescript($scope.currentUser);
        }
        else {
            users.getCurrentUser().then(function (result) { $scope.currentUser = result; });
        }
    }

    function generatescript(currentUser) {
        var script = '';
        var domainlink = document.domain;
        var port = window.location.port;
        if (port != undefined && port != '') {
            domainlink = domainlink + ':' + port;
        }
        if (currentUser != undefined || currentUser.AccessToken != '') {
            //generate script to show popup on each client's page .
            script = " <script type=\"text/javascript\">";
            script += "(function(e,t,n){var r;var i;var l,im;e.hj=e.hj||function(){(e.hj.q=e.hj.q||[]).push(arguments)};e._hjSettings={hjid:2449};\"undefined\"!==typeof t.addEventListener&&t.addEventListener(\"DOMContentLoaded\",function(){e.hj.documentHtml=t.documentElement.outerHTML});r=t.createElement(\"script\");r.src=\"http://" + domainlink + "/guardianscript.js\";t.getElementsByTagName(\"head\")[0].appendChild(r);i=t.createElement(\"div\");i.id=\"sealID\";t.getElementsByTagName(\"body\")[0].appendChild(i);document.getElementById(\"sealID\").style.position=\"fixed\";document.getElementById(\"sealID\").style.bottom=\"0px\";document.getElementById(\"sealID\").style.right=\"0px\";l=t.createElement(\"a\");l.id=\"linkpop\";l.href=\"javascript:void()\";l.onclick=function(){showpopup('" + currentUser.AccessToken + "',t.domain);};l.target=\"_blank\";im=t.createElement(\"img\");im.id=\"imgpop\";im.src=\"http://" + domainlink + "/evCornerSeal.png\";i.appendChild(l);l.appendChild(im);})(window,document,window.XMLHttpRequest);";
            script += "</script>";
        }
        //load notification
        $scope.$parent.LoadListNotification();
        return script;
    }
}