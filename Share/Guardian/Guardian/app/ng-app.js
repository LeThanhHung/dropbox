﻿app = angular.module('GuardianApp', ['ui.bootstrap', 'ngRoute', 'ngResource' ]);
 
//loading 
app.config(function ($httpProvider) {
    $httpProvider.responseInterceptors.push('myHttpInterceptor');

    var spinnerFunction = function (data, headersGetter, $rootScope) {
        // todo start the spinner here
        var loading = "#site_statistics_loading";        
        $(loading).show();
        return data;
    };
    //show loading 
    $httpProvider.defaults.transformRequest.push(spinnerFunction);
    //convert datetime json to normal datetime to display on form
    $httpProvider.defaults.transformResponse.push(function (responseData) {
        convertDateStringsToDates(responseData);
        return responseData;
    });
    //check session timeout
    $httpProvider.interceptors.push(['$injector',
    function ($injector) {
        return $injector.get('AuthInterceptor');
    }]);

})
//app.config([
//            '$httpProvider', 'fileUploadProvider',
//            function ($httpProvider, fileUploadProvider) {
//                delete $httpProvider.defaults.headers.common['X-Requested-With'];
//                fileUploadProvider.defaults.redirect = window.location.href.replace(
//                    /\/[^\/]*$/,
//                    '/cors/result.html?%s'
//                );
//                //if (isOnGitHub)
//                {
//                    // Demo settings:
//                    angular.extend(fileUploadProvider.defaults, {
//                        // Enable image resizing, except for Android and Opera,
//                        // which actually support image resizing, but fail to
//                        // send Blob objects via XHR requests:
//                        disableImageResize: /Android(?!.*Chrome)|Opera/
//                            .test(window.navigator.userAgent),
//                        maxFileSize: 999000,
//                        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|zip)$/i
//                    });
//                }
//            }
//])
app.factory('AuthInterceptor', function ($q) {
        return {
            responseError: function (response) {
                if (response.status === 401) {
                    //The user is not logged in
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated,
                                          response);
                }
                if (response.status === 403) {
                    //The user is logged in but isn’t allowed access
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthorized,
                                          response);
                }
                if (response.status === 419 || response.status === 440) {
                    //Session has expired
                    $rootScope.$broadcast(AUTH_EVENTS.sessionTimeout,
                                          response);
                }
                return $q.reject(response);
            }
        };
    })
// register the interceptor as a service, intercepts ALL angular ajax http calls
 app.factory('myHttpInterceptor', function ($q, $window) {

        return function (promise) {
            return promise.then(function (response) {
                // do something on success
                // todo hide the spinner
                $("#site_statistics_loading").hide();
                return response;

            }, function (response) {
                // do something on error
                // todo hide the spinner
                $("#site_statistics_loading").hide();
                return $q.reject(response);
            });
        };
    });
 
//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider
        .when('/Home/NewVulnerScan',
            {
                controller: 'NewVulController',
                templateUrl: '/Home/NewVulnerScan'
            })
         .when('/Home/Issues',
            {
                controller: 'VulIssueController',
                templateUrl: '/Home/Issues'
            })
         .when('/Home/Vulner',
            {
                controller: 'VulController',
                templateUrl: '/Home/Vulner'
            })
          .when('/Home/Profile',
            {
                controller: 'ProfileController',
                templateUrl: '/Home/Profile'
            }) 
        //Admin 
        .when('/Home/GenerateScript',
            {
                controller: 'GenerateScriptController',
                templateUrl: '/Home/GenerateScript'
            })
        .when('/Home/RegisterOnlineScan',
            {
                controller: 'RegisterOnlineScanController',
                templateUrl: '/Home/RegisterOnlineScan'
            })
         .when('/Home/ScanProgress',
            {
                controller: 'ScanProgressController',
                templateUrl: '/Home/ScanProgress'
            })
         .when('/Home/History',
            {
                controller: 'HistoryController',
                templateUrl: '/Home/History'
            })
         //profile 
        .when('/Home/Profile',
            {
                controller: 'ProfileController',
                templateUrl: '/Home/Profile'
            })
        //open seal info popup
        .when('/Seal/SealInfo/:accesstoken/d/:domain',
            {
                controller: 'SealController',
                templateUrl: '/Seal/SealInfo'
            })
        //open seal info popup
        .when('/Home/ViewReportScan/:historyId',
            {
                controller: 'ViewReportScanController',
                templateUrl: '/Home/ViewReportScan'
            })
            .when('/Home/ViewDetailScan/:fileId/p/:parentId/n/:filename/s/:filesize/r/:scanallresult',
            {
                controller: 'ViewDetailScanController',
                templateUrl: '/Home/ViewDetailScan'
            })

        //Image Checker routing 
          .when('/Home/ImageChecker',
            {
                controller: 'ImageCheckerController',
                templateUrl: '/Home/ImageChecker'
            })
          .when('/Home/AddNewImageChecker',
            {
                controller: 'ImageCheckerModalController',
                templateUrl: '/Home/AddNewImageChecker'
            })

        //Restoring 
        .when('/Home/RestoreHistoryView',
            {
                controller: 'RestoringHistoryController',
                templateUrl: '/Home/RestoreHistoryView'
            })
        .when('/Home/RestoreHistoryDetail/:restoreId',
            {
                controller: 'RestoreHistoryDetailController',
                templateUrl: '/Home/RestoreHistoryDetail'
            })
        .otherwise({
            controller: 'GenerateScriptController',
            templateUrl: '/Home/GenerateScript'
        });
});

var regexIso8601 = /\/Date\((.*?)\)\//gi;
function convertDateStringsToDates(input) {
    // Ignore things that aren't objects.
    if (typeof input !== "object") return input;

    for (var key in input) {
        if (!input.hasOwnProperty(key)) continue;

        var value = input[key];
        var match;
        // Check for string properties which look like dates.
        // TODO: Improve this regex to better match ISO 8601 date strings.
        if (typeof value === "string" && (match = value.match(regexIso8601))) {
            // Assume that Date.parse can parse ISO 8601 strings, or has been shimmed in older browsers to do so.
            var parsedDate = new Date(parseInt(value.substr(6)));

            var jsDate = new Date(parsedDate);
            input[key] = pad((jsDate.getMonth() + 1)) + '/' + pad(jsDate.getDate()) + '/' + pad(jsDate.getFullYear() + ' '+jsDate.getHours() +':'+jsDate.getMinutes()+':'+jsDate.getSeconds());

        } else if (typeof value === "object") {
            // Recurse into object
            convertDateStringsToDates(value);
        }
    }
}
function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}



