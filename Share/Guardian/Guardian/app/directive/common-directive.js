﻿app.directive('inputMask', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attrs, ctrl) {
            if (attrs.ngType == 'commonmask') {
                $(el).inputmask(scope.$eval(attrs.inputMask));
            }
            else if (attrs.ngType == 'CDF') {
                $(el).inputmask("Regex", {
                    regex: "([0-9][0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]-[0-9][0-9])|([0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]\/[0-9][0-9][0-9][0-9]-[0-9][0-9])",
                    isComplete: function (buffer, opts) {
                        return new RegExp(opts.regex).test(buffer.join(''));
                    }
                });
            }
            else if (attrs.ngType == 'currency' || attrs.ngType == 'decimal') {
                $(el).inputmask('decimal', {
                    rightAlignNumerics: false
                });
            }
            $(el).on('change', function (e) {
                if (attrs.ngType == 'commonmask') {
                    scope.$eval(attrs.ngModel + "='" + (el.val().replace(/[^\d]/g, '')) + "'");
                }
                else if (attrs.ngType == 'CDF') {
                    var pattern = /([0-9][0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]-[0-9][0-9])|([0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]\/[0-9][0-9][0-9][0-9]-[0-9][0-9])/;
                    var v = el.val();
                    if (pattern.test(v)) {
                        scope.$eval(attrs.ngModel + "='" + (el.val()) + "'");
                    }
                    else {
                        scope.$eval(attrs.ngModel + "=''");
                        //clear text in cdf texbox 
                        el.val('');
                    }
                }
                else if (attrs.ngType == 'decimal') {
                    scope.$eval(attrs.ngModel + "=" + parseFloat(el.val().replace(/[^\d\.]/g, '')) + "");
                    scope.$apply();
                }
                else if (attrs.ngType == 'currency' || attrs.ngType == 'decimal') {
                    var value = el.val().replace(/[^\d\.\-]/g, '');
                    if (value == NaN) { value = 0; }
                    //$(el).val(value);
                    scope.$eval(attrs.ngModel + "=" + value + "");
                    scope.$apply();
                }
            });
        }
    };
});




app.directive('datePicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, el, attrs, ctrl) {
            $(el).datepicker({
                format: 'dd/mm/yyyy', mask: '99/99/9999', autoclose: true, language: 'pt-BR'
            }).on('changeDate', function (event, value) {       
                ctrl.$setViewValue(event.date);
            }) ;
        }
    }
});


//validate scan name existed
app.directive('checkExistPackage', function ($http, $resource, $timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            var stop_timeout;
            return scope.$watch(function () {
                return ngModel.$modelValue;
            }, function (name) {
                //$timeout.cancel(stop_timeout);

                if (name === '')
                    return ngModel.$setValidity('validname', true);
                ngModel.$setValidity('validname',false);
                //validate exist email
                $http({
                    method: 'POST',
                    url: '/Vul/CheckExistedPackage',
                    data: { packageName: name },
                    async: false,
                }).success(function (data, status, headers, config) {
                    //stop_timeout = $timeout(function () {
                        return ngModel.$setValidity('validname', data === 'true');
                    //})
                }).error(function (data, status, headers, config) {
                    return ngModel.$setValidity('validname', false);
                });

            });
        }
    }
});


//validate scan name existed
app.directive('validScanName', function ($http, $resource, $timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            var stop_timeout;
            return scope.$watch(function () {
                return ngModel.$modelValue;
            }, function (name) {
                //$timeout.cancel(stop_timeout);

                if (name === '')
                    return ngModel.$setValidity('validscanname', true);
                 ngModel.$setValidity('validscanname', false);
                //validate exist email
                $http({
                    method: 'POST',
                    url: '/ScannerFeature/CheckExistName',
                    data: { name: name },
                    async: false,
                }).success(function (data, status, headers, config) {
                   // stop_timeout = $timeout(function () {
                        return ngModel.$setValidity('validscanname', data === 'true');
                    //})
                }).error(function (data, status, headers, config) {
                    return ngModel.$setValidity('validscanname', false);
                });

            });
        }
    }
});

//driective for paging table 
app.directive('pagingTable', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs) {
            //var oTable
            //if (attrs.aaPaging) {
            //    //registry for table 
            //     oTable = $(el).dataTable({
            //        "aoColumns": scope.$eval("aoColumns"),
            //        "aoColumnDefs": scope.$eval("aoColumnDefs"),
            //        "aaSorting": [[1, 'asc']],
            //        "bPaginate": false,
            //        "sSearch": "Pesquisar",
            //        "dom": '<"right"f>rt<"content data-table-toolbar left"p><"clear">',
            //        'bFilter': true,
            //        'bSort': false,
            //        //"iDisplayLength": 10,
            //        "fnCreatedRow": function (nRow, aData, iDataIndex) {
            //            ($compile(nRow)(scope)[0].innerHTML);// $compile(nRow)(scope);
            //        },
            //    });
            //} else
            {
                //registry for table 
                var oTable = $(el).dataTable({
                    "aoColumns": scope.$eval("aoColumns"),
                    "aoColumnDefs": scope.$eval("aoColumnDefs"),
                    "aaSorting": [[1, 'asc']],
                    "bPaginate": false,
                    "sSearch": "Pesquisar",
                    "dom": '<"left"p><"right"f><"clear">',
                    'bFilter': true,
                    'bSort': false,
                    "fnCreatedRow": function (nRow, aData, iDataIndex) {
                        ($compile(nRow)(scope)[0].innerHTML);// $compile(nRow)(scope);
                    },
                });
            }

            // watch for any changes to our data, rebuild the DataTable
            scope.$watch(attrs.aaData, function (value) {
                var val = value;
                if (val) {
                    oTable.fnClearTable();
                    oTable.fnAddData(scope.$eval(attrs.aaData));

                    $('input[type=checkbox]').iCheck({
                        checkboxClass: 'icheckbox_square-aero',
                        radioClass: 'iradio_square-aero',
                        increaseArea: '20%' // optional
                    });
                    $('input[type=checkbox]').on('ifClicked', function (event) {
                        scope.CallBack(event, $(this));
                    });
                }
                else {
                    oTable.fnClearTable();
                }
            });
            scope.$watch(attrs.aaSearch, function (value) {
                var val = value || null;
                if (val) {
                    if (val.length > 0)
                        oTable.fnFilter(value);

                }
                else {
                    oTable.fnFilterClear();
                }
            });
        }
    };
})

app.directive('chartGoogle', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs)
        {
            
            var htmlChart = "<div class='row placeholders'>" +
                                    "<div class='col-sm-12 placeholder'>     " +
                                        "<div id='chart_vulntype' style='height: 400px;'> loading... " +
                                        "</div>   " +
                                    "</div>   " +
                                    "<div class='col-sm-6 placeholder'>     " +
                                        "<div id='chart_lang' style='width: 100%; height: 400px;'>      " +
                                           "loading...  " +
                                        "</div>   " +
                                    "</div>   " +
                                   "<div class='col-sm-6 placeholder'>     " +
                                       "<div id='chart_severity' style='width: 100%; height: 400px;'>    " +
                                       " loading...   " +
                                       " </div>   " +
                                    "</div></div>";
            $(el).html(htmlChart);
            function checkExistItemArray(name,arr)
            {
                var index = -1;
                for (i = 0; i < arr.length; i++) {
                    if (arr[i].name == name) { index = i; return index }
                }
                return -1;
            }

            function generateDataChart(columns , arr)
            {
                var data = [];
                data.push(eval(columns));
                for (i = 0; i < arr.length; i++) {     
                        var val = "['" + arr[i].name + "', " + arr[i].value + "]";
                    console.log(val);
                    data.push(eval(val));
                }      
                return data;
            }
            // watch for any changes to our data, rebuild the DataTable
            scope.$watch(attrs.aaData, function (value) {
                var val = value;
                if (val) {
                    var chart_plugin_metrics = new Array();
                    var chart_severity_metrics = new Array();
                    var chart_vulntype_metrics = new Array();

                    for(var i = 0 ; i < val.length;i++)
                    {
                        var item = val[i];
                        if (item != undefined)
                        {
                            //for plugin
                            var indexPlugin = checkExistItemArray(item.Plugin , chart_plugin_metrics)
                            if (indexPlugin >= 0)
                            {
                                chart_plugin_metrics[indexPlugin].value += 1;
                            }
                            else {
                                chart_plugin_metrics.push({'name':item.Plugin,'value':1});
                            }

                            var indexServerity = checkExistItemArray(item.Severity, chart_severity_metrics);
                            //for serverity
                            if (indexServerity>=0) {
                                chart_severity_metrics[indexServerity].value += 1;
                            }
                            else {
                                chart_severity_metrics.push({ 'name': item.Severity, 'value': 1 });
                            }

                            var indexType = checkExistItemArray(item.Type, chart_vulntype_metrics);
                            //for vulntype
                            if (indexType>=0) {
                                chart_vulntype_metrics[indexType].value += 1;
                            }
                            else {
                                chart_vulntype_metrics.push({ 'name': item.Type, 'value': 1 });;
                            }
                        }
                    }
                    //draw the chart
                    google.load("visualization", "1", {
                        packages: ["corechart"]
                    });

                    //google.setOnLoadCallback(function () {
                        //draw chart plugin   
                        var dataplugin = google.visualization.arrayToDataTable(
                                     generateDataChart('["Plugin Type", "Issue Count"]', chart_plugin_metrics)
                       );

                        var optionsplugin = {
                            'title': 'Plugin-Type Metrics',
                            'pieHole': 0.5,
                            'width': 500,
                            'height': 300
                        };

                        var chartmetric = new google.visualization.PieChart(document.getElementById('chart_lang'));
                        chartmetric.draw(dataplugin, optionsplugin);

                        //draw chart serverity
                        var dataseverity = google.visualization.arrayToDataTable(
                             generateDataChart('["Severity Type", "Count"]', chart_severity_metrics) 

                        );

                        var optionsseverity = {
                            'title': 'Severity Metrics',
                            'pieHole': 0.5,
                            'width': 500,
                            'height': 300
                        };

                        var chartseverity = new google.visualization.PieChart(document.getElementById('chart_severity'));
                        chartseverity.draw(dataseverity, optionsseverity);

                        //draw Vulntype
                        var datavulntype = google.visualization.arrayToDataTable(
                         generateDataChart('["Vulnerability Type", "Count"]', chart_vulntype_metrics)    
                        );

                        var optionsvulntype = {
                            'title': 'Issue Type Metrics',
                            'pieHole': 0.5,
                            'width': 700,
                            'height': 400
                        };

                        var chartvulntype = new google.visualization.PieChart(document.getElementById('chart_vulntype'));
                        chartvulntype.draw(datavulntype, optionsvulntype);
                   // });

                }
            });
        }
    }
});

app.directive('fileUpload', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el, attrs)
        {
            $(el).fileupload(scope.$eval(attrs.aaOption));
        }
    }
})
