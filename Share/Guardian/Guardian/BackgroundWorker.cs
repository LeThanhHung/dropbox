﻿using Guardian.Hubs;
using log4net;
using Microsoft.AspNet.SignalR;
using Oas.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Web.Hosting;

namespace Guardian
{
    public class BackgroundWorker : IRegisteredObject
    {
        public static readonly Timer RescanTimer = new Timer();
        private static bool IsRunning = false;
        IHistoryService historyService { get; set; }
        IScanDetailService scanDetailService { get; set; }
        IScanResultService scanResultService { get; set; }
        IScanResultDetailService scanResultDetailService { get; set; }
        IScanService ScanService { get; set; }
        public int Period { get; set; }
        ILog log { get; set; }
        public string UserId { get; set; }
        private readonly IHubContext _uptimeHub;
        public BackgroundWorker(IHistoryService historyService, IScanDetailService scanDetailService, IScanResultService scanResultService, IScanResultDetailService scanResultDetailService, IScanService ScanService)
        {
            this.historyService = historyService;
            this.scanDetailService = scanDetailService;
            this.scanResultService = scanResultService;
            this.scanResultDetailService = scanResultDetailService;
            this.ScanService = ScanService;
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);  
        }

        public BackgroundWorker()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            _uptimeHub = GlobalHost.ConnectionManager.GetHubContext<ServiceHub>(); 
            
        }

        public void Start()
        {
            RescanTimer.Enabled = true;
            RescanTimer.Interval = 100; 
            RescanTimer.Elapsed += (o, e) => { RunRescan(); };
            RescanTimer.AutoReset = false;
        }

        

        public void RunRescan()
        {
            RescanTimer.AutoReset = false;
            log.Info("=================================================== Start Rescan at "+DateTime.Now.ToLongDateString()+"=======================================================");
            if (RescanTimer != null)
            {
                //RescanTimer.Stop();
                try
                {
                    List<RescanModel> list = new List<RescanModel>();
                    List<Guid> listHistoryCompleted = historyService.Get().Where(c => c.IsCompleted && c.IsActive).Select(c => c.Id).ToList();
                    if (listHistoryCompleted != null && listHistoryCompleted.Count > 0)
                    { 
                        var listScanDetailID  = scanDetailService.Get().Where(c=>listHistoryCompleted.Contains(c.HistoryID) && c.IsActive).Select(c=>c.Id).ToList();
                        list = scanResultService.Get().Where(c=> c.IsActive && listScanDetailID.Contains(c.ScanDetailId) && c.Progress_percentage<100).Select(c=>new RescanModel(){ScanDetailId = c.ScanDetailId , ScanResultId = c.Id}).ToList();
                        foreach (RescanModel model in list)
                        {
                           Task task = ScanService.ExcuteRescanAsync(model.ScanDetailId, model.ScanResultId);
                        }
                    }
                }
                catch (Exception ce)
                {
                    IsRunning = false;
                    log.Info("=================================================== Start Error in Timer=======================================================");
                    log.Error(ce);
                    log.Info("=================================================== End Error in Timer=======================================================");
                }

                RescanTimer.AutoReset = true;
            }
            log.Info("=================================================== End Rescan " + DateTime.Now.ToLongDateString() + "=======================================================");
            
        }

        public void Stop(bool immediate)
        {
            RescanTimer.Dispose();

            HostingEnvironment.UnregisterObject(this);
        }
    }

    public class RescanModel {
        public Guid ScanDetailId { get; set; }
        public Guid ScanResultId { get; set; }
    }
}