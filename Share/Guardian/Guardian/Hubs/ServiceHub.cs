﻿using Microsoft.AspNet.SignalR;
using Oas.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Microsoft.AspNet.Identity;
using log4net;
using System.Threading.Tasks;
using Guardian.Models;
using System.IO;
using Oas.Infrastructure.Model;
using Oas.Infrastructure.Domain;
namespace Guardian.Hubs
{
    public class ServiceHub : Hub
    {
        public IUserService userService { get; set; }
        public IHistoryService historyService { get; set; }
        public IScanDetailService scanDetailService { get; set; }
        public IScanService scanService { get; set; }
        public IScanResultService scanResultService { get; set; }
        public IScanResultDetailService scanResultDetailService { get; set; }

        public IImageCheckerService imageCheckerService { get; set; }
        public ILog log { get; set; }

        public ServiceHub(IUserService userService, IImageCheckerService imageCheckerService, IHistoryService historyService, IScanDetailService scanDetailService, IScanService scanService, IScanResultService scanResultService, IScanResultDetailService scanResultDetailService)
        {
            this.historyService = historyService;
            this.scanDetailService = scanDetailService;
            this.scanService = scanService;
            this.scanResultService = scanResultService;
            this.scanResultDetailService = scanResultDetailService;
            this.imageCheckerService = imageCheckerService;
            this.userService = userService;
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        public ServiceHub()
        { }

        /// <summary>
        /// Execute rescan in background for specificate client
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="connectionId"></param>
        public void ExecuteRescanByUserID(string userId, string connectionId)
        {
            log.Info("============== Start Rescan " + DateTime.Now.ToLongDateString() + "=============");
            if (String.IsNullOrEmpty(userId)) return;
            try
            {

                List<RescanModel> list = new List<RescanModel>();
                List<Guid> listHistoryCompleted = historyService.Get().Where(c => c.IsCompleted && c.UserId == userId).Select(c => c.Id).ToList();
                if (listHistoryCompleted != null && listHistoryCompleted.Count > 0)
                {
                    var listScanDetailID = scanDetailService.Get().Where(c => listHistoryCompleted.Contains(c.HistoryID)).Select(c => c.Id).ToList();
                    list = scanResultService.Get().Where(c => listScanDetailID.Contains(c.ScanDetailId) && c.Progress_percentage < 100).Select(c => new RescanModel() { ScanDetailId = c.ScanDetailId, ScanResultId = c.Id }).ToList();
                    log.Info("List has " + list.Count + " Items");
                    if (list.Count == 0)
                    {

                        NotificateResultScanClient(userId, connectionId, "NoChange");
                    }
                    else
                    {
                        foreach (RescanModel model in list)
                        {
                            Task task = scanService.ExcuteRescanAsync(model.ScanDetailId, model.ScanResultId);
                        }

                        //call notificate to all client 
                        NotificateResultScanClient(userId, Context.ConnectionId, "HasChange");
                    }
                }
            }
            catch (Exception ce)
            {
                log.Info("=========  Start Error in Timer ======================");
                log.Error(ce);
                log.Info("============ End Error in Timer =====================");
                NotificateResultScanClient(userId, Context.ConnectionId, "NoChange");
            }
            log.Info("================ End Rescan " + DateTime.Now.ToLongDateString() + " ==============");
        }


        /// <summary>
        /// Notificate to specify client has change 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="connectionId"></param>
        /// <param name="action"></param>
        public void NotificateResultScanClient(string userId, string connectionId, string action)
        {
            if (String.IsNullOrEmpty(userId)) return;
            Clients.Client(connectionId).receiveNotificateRescanResult(userId, action);

        }

        #region Restore
        public void ExecuteRestore(string userId, string connectionId)
        {
            Guid lastestId = Guid.Empty;
            try
            {
                if (String.IsNullOrEmpty(userId))
                {
                    NotitficationProcessRestore(userId, connectionId, "Não foi possível iniciar o processo de restauração.", 100, true, true);
                }
                else
                {
                    var currentUser = userService.Get(userId); //getCurrentUser();
                    if (currentUser != null)
                    {
                        string localFile = Context.Request.GetHttpContext().Server.MapPath(String.Format("~/Upload/test{0:MMddyyyyHHmmss}.txt", DateTime.Now));
                        FileInfo file = new FileInfo(localFile);
                        if (!file.Exists)
                        {
                            using (FileStream stream = System.IO.File.Create(localFile))
                            {

                            }
                        }


                        if (scanService.VerifyPermissionAccount(currentUser.FtpServer, currentUser.FtpPort, currentUser.FtpAccount, currentUser.FtpPassword, localFile))
                        {
                            //get lastest version of backup base on userId
                            IList<ImageChecker> lst = imageCheckerService.GetByUserId(currentUser.Id);
                            if (lst.Count > 0)
                            {
                                ImageChecker lastest = (from i in lst
                                                        let maxId = lst.Max(m => m.LastModify)
                                                        where i.LastModify == maxId
                                                        select i).FirstOrDefault();
                                if (lastest != null)
                                {
                                    int totalFolder, totalFile = 0;
                                    //update is notification is Restoring
                                    lastestId = lastest.Id;
                                    //update IsRestoring 
                                    lastest.IsRestore = true;
                                    imageCheckerService.Update(lastest);
                                    //notificate to client for reporting the processing
                                    NotitficationProcessRestore(userId, connectionId, "Filtrando diretórios", 10);
                                    string localLocationBackup = Context.Request.GetHttpContext().Server.MapPath(lastest.Location);
                                    //get architecture of folder on the local 
                                    FolderLocalInfo folderLocal = scanService.ReadArtchitectFolder(localLocationBackup, out totalFolder, out totalFile);
                                    //start restore
                                    if (folderLocal != null)
                                    {
                                        //notificate to client for reporting the processing
                                        NotitficationProcessRestore(userId, connectionId, "Restaurando", 20);
                                        int index = 0;
                                        foreach (FolderLocalInfo f in folderLocal.FolderLocalInfos)
                                        {
                                            RestoreToFTP(userId, connectionId, currentUser.FtpServer, currentUser.FtpPort, currentUser.FtpAccount, currentUser.FtpPassword, folderLocal, totalFolder + totalFile, out index, "/");
                                        }
                                        //exist the files 
                                        if (folderLocal.FileLocalInfos.Count > 0)
                                        {
                                            foreach (FileLocalInfo f in folderLocal.FileLocalInfos)
                                            {
                                                string remoteFile = String.Format("/{0}",  f.FileName);
                                                string messageError = string.Empty;
                                                //upload the file to folder 
                                                if (scanService.UploadFtpFile(remoteFile, f.PathFile,  currentUser.FtpAccount, currentUser.FtpPassword,currentUser.FtpServer,ref messageError))
                                                {
                                                    index = +1;
                                                    NotitficationProcessRestore(userId, connectionId, "Created file: " + remoteFile, ((index / (totalFolder+totalFile)) * 100));
                                                }
                                            }
                                        }
                                    }
                                    //update IsRestoring 
                                    lastest.IsRestore = false;
                                    imageCheckerService.Update(lastest);
                                    //notificate to client for reporting the processing
                                    NotitficationProcessRestore(userId, connectionId, "Completo", 100, true, true);
                                }

                            }

                        }
                        else
                        {
                            //return to client to notification Fail restore
                            NotitficationProcessRestore(userId, connectionId, "Acesso não autorizado. Confira a conta de FTP e tente novamente.", 100, true, true);
                        }

                    }
                }
            }
            catch (Exception ce)
            {
                if (lastestId != Guid.Empty)
                {
                    var lstImage = imageCheckerService.Get(lastestId);
                    if (lstImage != null)
                    {
                        lstImage.IsRestore = false;
                        imageCheckerService.Update(lstImage);
                    }
                }
                //return to client to notification Fail restore
                NotitficationProcessRestore(userId, connectionId, "Ocorreu um erro durante a restauração, contate o suporte para uma restauração manual.", 100, true, true);
            }

        }

        private void RestoreToFTP(string userId, string connectionId, string servername, int port, string username, string password, FolderLocalInfo folder, int totalitem, out int index, string serverpath)
        {

            if (folder != null)
            {
                string pathCreateFolder = serverpath.Equals("/") ? folder.FolderName : String.Format("{0}/{1}", serverpath, folder.FolderName);
                //create folder 
                if (scanService.CreateDirectory(pathCreateFolder, username, password, servername))
                {
                    index = +1;
                    NotitficationProcessRestore(userId, connectionId, "Created folder: " + pathCreateFolder, ((index / totalitem) * 100));
                    if (folder.FolderLocalInfos.Count > 0)
                    {
                        foreach (FolderLocalInfo f in folder.FolderLocalInfos)
                        {
                            //create child folder 
                            RestoreToFTP(userId, connectionId, servername, port, username, password, f, totalitem, out index, pathCreateFolder);
                        }
                    }
                    //exist the files 
                    if (folder.FileLocalInfos.Count > 0)
                    {
                        foreach (FileLocalInfo f in folder.FileLocalInfos)
                        {
                            string remoteFile = String.Format("{0}/{1}", pathCreateFolder, f.FileName);
                            string messageError = string.Empty;
                            //upload the file to folder 
                            if (scanService.UploadFtpFile(remoteFile, f.PathFile, username, password, servername,ref messageError))
                            {
                                index = +1;
                                NotitficationProcessRestore(userId, connectionId, "Created file: " + remoteFile, ((index / totalitem) * 100));
                            }
                        }
                    }
                }
            }
            index = +0;
        }



        public void CancelRestore(string userId, string connectionId)
        {

        }

        public void NotitficationProcessRestore(string userId, string connectionId, string textprocess, decimal percent, bool isCompleted = false, bool isError = false)
        {
            ProcessRestoreViewModel model = new ProcessRestoreViewModel()
            {
                ClientId = connectionId,
                UserId = userId,
                Percent = percent,
                TextProcess = textprocess,
                IsCompleted = isCompleted,
                IsError = isError
            };
            Clients.Client(connectionId).receiveNotificateRestoreResult(model);
        }
        #endregion
    }
}