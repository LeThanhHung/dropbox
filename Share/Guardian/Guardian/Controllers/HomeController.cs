﻿using Oas.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
namespace Guardian.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController( )
            : base( )
        {
           
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// View for Generate Script module
        /// </summary>
        /// <returns></returns>
        public ActionResult GenerateScript()
        {
             
            return PartialView();
        }

        [AllowAnonymous]
        public ActionResult RegisterOnlineScan()
        {
            return PartialView();
        }

        /// <summary>
        /// View for showing the profile of customer
        /// </summary>
        /// <returns></returns>
        public ActionResult Profile()
        {
            return PartialView();
        }


        public ActionResult ScanProgress()
        {
            return PartialView();
        }

        public ActionResult History()
        {
            return PartialView();
        }

        public ActionResult ViewReportScan()
        {
            return PartialView();
        }

        public ActionResult ViewDetailScan()
        {
            return PartialView();
        }

        public ActionResult AddNewHistory()
        {
            return PartialView();                
        }

        public ActionResult ImageChecker()
        {
            return PartialView();
        }

        public ActionResult AddNewImageChecker()
        {
            return PartialView();
        }

        public ActionResult ProcessRestore()
        {
            return PartialView();
        }

        public ActionResult RestoreHistoryView()
        {
            return PartialView();
        }

        public ActionResult RestoreHistoryDetail()
        {
            return PartialView();
        }

        public ActionResult Vulner()
        {
            return PartialView();
        }

        public ActionResult Issues()
        {
            return PartialView();
        }

        public ActionResult NewVulnerScan()
        {
            return PartialView();
        }
    }
}
