﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using Guardian.Filters;
using Guardian.Models;
using Oas.Infrastructure.Services;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Oas.Infrastructure.Domain;
using Microsoft.AspNet.Identity.EntityFramework;
using Oas.Infrastructure;
using System.Threading.Tasks;

namespace Guardian.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        //
        // GET: /Account/Login
        
        private UserManager<User> userManager;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public AccountController() :base()
        {     
            userManager = new UserManager<User>(new UserStore<User>(new DatabaseContext()));
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public UserManager<User> UserManager { get { return userManager; } set { userManager = value; } }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return PartialView();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(string userName, string password)
        {

            var user = await UserManager.FindAsync(userName, password);
            if (user != null)
            {
                await SignInAsync(user, false);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task SignInAsync(User user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return PartialView();
        }

        //
        // POST: /Account/Register 
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> RegisterUser(UserViewModel model)
        {
            if (model != null)
            {
                try
                {
                    var user = new User()
                    {      
                        UserName = model.Email,
                        Name = model.Name,
                        LastName = model.LastName,
                        Email = model.Email,
                        Zipcode = model.Zipcode,
                        SealType = model.SealType,
                        SealStatus = SealStatus.Invalid.ToString(),
                        ExpireDate = DateTime.Now,
                        Domain = model.Domain,
                        CreatedDate = DateTime.Now,
                        CPF = model.CPF,
                        CNPJ = model.CNPJ,
                        Company = model.Company,
                        Address = model.Address,
                        AccessToken = Guid.NewGuid().ToString().Replace("-", ""),
                        RG = model.RG,
                        //extra more fields  
                        DateOfBirth = model.DateOfBirth,
                        Gender = model.Gender,
                        ENDERECO = model.ENDERECO,
                        NUMERO = model.NUMERO,
                        COMPLEMENTO = model.COMPLEMENTO,
                        BAIRRO = model.BAIRRO,
                        CIDADE = model.CIDADE,
                        ESTADO = model.ESTADO,
                        TELEFONE = model.TELEFONE,
                        CELULAR = model.CELULAR   ,
                        OutroTelefone = model.OutroTelefone, 
                        Passaporte = model.Passaporte , 
                        PontodeReferencia = model.PontodeReferencia,
                        RazaoSocial = model.RazaoSocial,
                        TypeOfRegister = model.TypeOfRegister,
                        Contribuinte = false,//model.Contribuinte,
                        InscricaoEstadual = model.InscricaoEstadual,
                        Isento =false,//model.Isento,
                        Pais = model.Pais
                    };
                    var result = await UserManager.CreateAsync(user, model.Password);
                    return Json(result.Succeeded);
                    // Attempt to register the user                   
                }
                catch (Exception e)
                {
                    Json(false);
                }
            }

            // If we got this far, something failed, redisplay form
            return Json(true);
        }

        /// <summary>
        /// save profile of current user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditProfile(User user)
        {
            if (user != null)
            {
                userService.UpdateUserInfo(user);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult CheckExistEmail(string email)
        {
            if (!String.IsNullOrEmpty(email))
            {
                return Json(userService.GetUserByUserName(email) == null, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetCurrentUser()
        {
            var id = this.User.Identity.GetUserId();
            if (id != null)
            {
                //get accesstoken of current user  
                var currentuser = userService.Get(id);
                if (currentuser != null) return Json(currentuser, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetSealInfo(string accesstoken, string domain)
        {
            return Json(userService.GetUserInfoByAccessTokenDomani(accesstoken, domain), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ForgotPassword()
        {
            return PartialView();
        }
    }
}
