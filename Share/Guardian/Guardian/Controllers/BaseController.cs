﻿using log4net;
using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Oas.Infrastructure.Services;
using Oas.Infrastructure;
using System.IO;
using Guardian.App_Start;
using Ninject;
using System.Threading.Tasks;
namespace Guardian.Controllers
{
    public class BaseController : Controller
    {
        private DatabaseContext dbContext;

        public DatabaseContext DbContext
        {
            get { return dbContext ?? (dbContext = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<DatabaseContext>()); }

        }
        private IVulnerScanService _vulnerscanservice;
        public IVulnerScanService VulnerScanService { get { return _vulnerscanservice ?? (_vulnerscanservice = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IVulnerScanService>()); } }
        private IHistoryService _historyService;
        public IHistoryService historyService { get { return _historyService ?? (_historyService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IHistoryService>()); } }
        private IScanDetailService _scanDetailService;
        public IScanDetailService scanDetailService { get { return _scanDetailService ?? (_scanDetailService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IScanDetailService>()); } }
        private IScanService _scanService;
        public IScanService scanService { get { return _scanService ?? (_scanService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IScanService>()); } }
        private IUserService _userService;
        public IUserService userService
        {
            get
            {
                _userService = _userService ?? (_userService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IUserService>());
                return _userService;
            }
        }
        private IScanResultService _scanResultService;
        public IScanResultService scanResultService { get { return _scanResultService ?? (_scanResultService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IScanResultService>()); } }
        private IScanResultDetailService _scanResultDetailService;
        public IScanResultDetailService scanResultDetailService { get { return _scanResultDetailService ?? (_scanResultDetailService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IScanResultDetailService>()); } }
        private IImageCheckerService _imageCheckerService;
        public IImageCheckerService imageCheckerService { get { return _imageCheckerService ?? (_imageCheckerService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IImageCheckerService>()); } }
        private IImageCheckerRestoreService _imageCheckerRestoreService;
        public IImageCheckerRestoreService imageCheckerRestoreService { get { return _imageCheckerRestoreService ?? (_imageCheckerRestoreService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IImageCheckerRestoreService>()); } }
        private IRestoreHistoryService _restoreHistoryService;
        public IRestoreHistoryService restoreHistoryService { get { return _restoreHistoryService ?? (_restoreHistoryService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IRestoreHistoryService>()); } }
        private IImageCheckerDetailService _imageCheckerDetailService;
        public IImageCheckerDetailService imageCheckerDetailService { get { return _imageCheckerDetailService ?? (_imageCheckerDetailService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<IImageCheckerDetailService>()); } }
        private ILoggingActionClientService _loggingActionClientService;
        public ILoggingActionClientService loggingActionClientService { get { return _loggingActionClientService ?? (_loggingActionClientService = (NinjectWebCommon.bootstrapper.Kernel as StandardKernel).Get<ILoggingActionClientService>()); } }

        public readonly ILog log;

        public BaseController()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }
        public User getCurrentUser()
        {
            var id = this.User.Identity.GetUserId();
            if (id != null)
            {
                //get accesstoken of current user  
                var currentuser = userService.Get(id);
                return currentuser;
            }
            return null;
        }

        public bool HasPersmissionFTP()
        {
            User user = getCurrentUser();
            if (user != null)
            {
                try
                {
                    string localFile = Server.MapPath(String.Format("~/Upload/test{0:MMddyyyyHHmmss}.txt", DateTime.Now));
                    FileInfo file = new FileInfo(localFile);
                    if (!file.Exists)
                    {
                        using (FileStream stream = System.IO.File.Create(localFile))
                        {

                        }
                    }
                    return scanService.VerifyPermissionAccount(user.FtpServer, user.FtpPort, user.FtpAccount, user.FtpPassword, localFile);

                }
                catch { return false; }
            }
            return false;
        }

         

        public bool UpdateImageCheckerRestore(Guid imageCheckerRestoreId, bool isSuccess,string errorMesssage)
        {
            try
            {
                var item = imageCheckerRestoreService.Get(imageCheckerRestoreId);
                item.IsSuccess = isSuccess;
                item.ErrorMessage = errorMesssage;
                imageCheckerRestoreService.Update(item);
                return true;
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return false;
        }
        public ImageCheckerRestore TrackImageCheckerRestore(Guid imageCheckerId, bool isSuccess, Guid userId, string action, string errorMessage = "", bool isRead = false)
        {
            try
            {
                ImageCheckerRestore restore = new ImageCheckerRestore();
                restore.Id = Guid.NewGuid();
                restore.ImageCheckerId = imageCheckerId;
                restore.IsSuccess = isSuccess;
                restore.ErrorMessage = errorMessage;
                restore.RestoredDate = DateTime.Now;
                restore.Action = action;
                restore.UserId = userId;
                restore.IsRead = isRead;
                imageCheckerRestoreService.Add(restore);

                return restore;
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return null;
        }

        public ImageCheckerRestore UpdateImageCheckerRestore(ImageCheckerRestore imageCheckerRestore)
        {
            try
            {
                imageCheckerRestoreService.Update(imageCheckerRestore);

                return imageCheckerRestore;
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return null;
        }
    }
}
