﻿using Oas.Infrastructure.Domain;
using Oas.Infrastructure.Services;
using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Threading.Tasks;
using Oas.Infrastructure.Model;
using Guardian.Models;

namespace Guardian.Controllers
{
    public class ImageCheckerController : BaseController
    {
        public ImageCheckerController()
            : base()
        {

        }
        //
        // GET: /ImageChecker/

        public ActionResult Index()
        {
            return View();
        }

        #region ImageChecker
        [HttpPost]

        public ActionResult InsertImageChecker(ImageChecker entity)
        {
            try
            {
                if (entity != null)
                {
                    var userId = this.User.Identity.GetUserId();
                    if (userId != null)
                    {
                        entity.Id = Guid.NewGuid();
                        entity.UserId = userId;
                        entity.IsDownloaded = false;
                        entity.IsStop = false;
                        entity.CreatedDate = DateTime.Now;
                        entity.LastModify = DateTime.Now;
                        imageCheckerService.Add(entity);
                        return Json(entity, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateImageChecker(ImageChecker entity)
        {
            try
            {
                if (entity != null)
                {
                    entity.LastModify = DateTime.Now;
                    imageCheckerService.Update(entity);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DelImageChecker(ImageChecker entity)
        {
            try
            {
                if (entity != null)
                {
                    imageCheckerService.Remove(entity.Id);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetImageChecker()
        {
            List<ImageChecker> list = new List<ImageChecker>();
            try
            {
                var id = this.User.Identity.GetUserId();
                if (id != null)
                {
                    list = imageCheckerService.Get().Where(c => c.UserId == id).ToList();
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetImageCheckerById(Guid Id)
        {
            try
            {
                if (Id != null)
                {
                    var entity = imageCheckerService.Get(Id);

                    return Json(entity, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult VerifyScope()
        {
            try
            {
                var user = getCurrentUser();
                if (user != null)
                {
                    var list = imageCheckerService.Get().Where(c => c.UserId == user.Id).ToList();
                    switch (user.SealType.Trim())
                    {
                        case "Standard":
                            return Json((list == null || (list != null && list.Count <= 0)), JsonRequestBehavior.AllowGet);
                        case "Premium":
                            return Json((list == null || (list != null && list.Count <= 10)), JsonRequestBehavior.AllowGet);
                        case "Plus":
                            return Json((list == null || (list != null && list.Count <= 20)), JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckExistBackup(int runin, string timetorun)
        {
            try
            {
                var user = getCurrentUser();
                if (user != null)
                {
                    var list = imageCheckerService.Get().Where(c => c.UserId == user.Id && c.RunInMothOrWeek == runin && c.StartTime.Equals(timetorun)).ToList();
                    return Json(list.Count == 0, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Restore()
        {
            return ExecuteRestore();
        }

        /// <summary>
        /// get lastest version to restore and mark it as Restoring  
        /// this function will help show Restoring icon loading on the form 
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChangeStatusLastestRestoreVersion(bool status)
        {
            Guid lastestId = Guid.Empty;
            var currentUser = getCurrentUser();
            if (!HasPersmissionFTP()) return Json(new ResponeResult() { IsSuccess = false, ErrorMessage = "Not Authentication to FTP Folder." }, JsonRequestBehavior.AllowGet);
            try
            {
                if (currentUser != null)
                {
                    IList<ImageChecker> lst = imageCheckerService.GetByUserId(currentUser.Id);
                    if (lst.Count > 0)
                    {
                        ImageChecker lastest = (from i in lst
                                                let maxId = lst.Max(m => m.LastModify)
                                                where i.LastModify == maxId
                                                select i).FirstOrDefault();
                        if (lastest != null)
                        {
                            lastestId = lastest.Id;
                            lastest.IsRestore = status;
                            imageCheckerService.Update(lastest);
                            return Json(new ResponeResult() { IsSuccess = true, Data = true }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
                if (lastestId != Guid.Empty)
                {
                    //update Isrestore to false
                    var igc = imageCheckerService.Get(lastestId);
                    if (igc != null) { igc.IsRestore = false; imageCheckerService.Update(igc); }
                }
            }
            return Json(new ResponeResult() { IsSuccess = true, Data = false }, JsonRequestBehavior.AllowGet);
        }

        private void TrackRestoreProcess(Guid imagecheckerRestoreId, string fromFile, string toFile, bool isSuccess, User currentUser)
        {
            try
            {
                RestoreHistory history = new RestoreHistory();
                history.Id = Guid.NewGuid();
                history.ImageCheckerRestoreId = imagecheckerRestoreId;
                history.IsSuccess = isSuccess;
                history.PathFileLocal = fromFile;
                history.PathFileServer = toFile;
                history.UserId = currentUser.Id.ToString();
                history.RestoreDate = DateTime.Now;
                history.FtpAccount = currentUser.FtpAccount;
                history.FtpPassword = currentUser.FtpPassword;
                history.FtpPort = currentUser.FtpPort;
                history.FtpServer = currentUser.FtpServer;
                restoreHistoryService.Add(history);
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
        }
        private ImageCheckerRestore UpdateImageCheckerRestore(ImageCheckerRestore restore, Guid imageCheckerId, bool isSuccess, string errorMessage = "")
        {
            try
            {

                restore.ImageCheckerId = imageCheckerId;
                restore.IsSuccess = isSuccess;
                restore.ErrorMessage = errorMessage;
                imageCheckerRestoreService.Update(restore);

                return restore;
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return null;
        }
        public JsonResult ExecuteRestore()
        {
            Guid lastestId = Guid.Empty;
            var currentUser = getCurrentUser();
            try
            {
                if (!HasPersmissionFTP())
                {

                    throw new Exception("No permission on your FTP Folder.");
                }
                if (String.IsNullOrEmpty(currentUser.Id))
                {
                    //save error not get current userId to imagecheckerrestore
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (currentUser != null)
                    {
                        string localFile = Server.MapPath(String.Format("~/Upload/test{0:MMddyyyyHHmmss}.txt", DateTime.Now));
                        FileInfo file = new FileInfo(localFile);
                        if (!file.Exists)
                        {
                            using (FileStream stream = System.IO.File.Create(localFile))
                            {

                            }
                        }

                        //get lastest version of backup base on userId
                        IList<ImageChecker> lst = imageCheckerService.GetByUserId(currentUser.Id);
                        if (lst.Count > 0)
                        {
                            ImageChecker lastest = (from i in lst
                                                    let maxId = lst.Max(m => m.LastModify)
                                                    where i.LastModify == maxId
                                                    select i).FirstOrDefault();
                            if (lastest != null)
                            {
                                int totalFolder, totalFile = 0;
                                //update is notification is Restoring
                                lastestId = lastest.Id;
                                //update IsRestoring 
                                lastest.IsRestore = true;
                                imageCheckerService.Update(lastest);

                                //create new ImageCheckerRestore 
                                var imageCheckerRestore = TrackImageCheckerRestore(Guid.Empty,false, Guid.Parse(currentUser.Id), Guardian.Models.Action.Restore.ToString(), "Restorring.");

                                string localLocationBackup = Server.MapPath(lastest.Location);
                                //get architecture of folder on the local 
                                FolderLocalInfo folderLocal = scanService.ReadArtchitectFolder(localLocationBackup, out totalFolder, out totalFile);
                                //start restore
                                if (folderLocal != null)
                                {
                                    //notificate to client for reporting the processing
                                    int index = 0;
                                    foreach (FolderLocalInfo f in folderLocal.FolderLocalInfos)
                                    {
                                        RestoreToFTP(imageCheckerRestore, currentUser.FtpServer, currentUser.FtpPort, currentUser.FtpAccount, currentUser.FtpPassword, f, totalFolder + totalFile, out index, "/", currentUser);
                                    }
                                    //exist the files 
                                    if (folderLocal.FileLocalInfos.Count > 0)
                                    {
                                        foreach (FileLocalInfo f in folderLocal.FileLocalInfos)
                                        {
                                            string remoteFile = String.Format("/{0}", f.FileName);
                                            string messageError = string.Empty;
                                            //upload the file to folder 
                                            bool isSuccess = scanService.UploadFtpFile(remoteFile, f.PathFile, currentUser.FtpAccount, currentUser.FtpPassword, currentUser.FtpServer, ref messageError);
                                            RestoreHistory restoreHistory = new RestoreHistory();
                                            restoreHistory.Id = Guid.NewGuid();
                                            restoreHistory.ErrorMessage = messageError;
                                            restoreHistory.FtpAccount = currentUser.FtpAccount;
                                            restoreHistory.FtpPassword = currentUser.FtpPassword;
                                            restoreHistory.FtpPort = 21;
                                            restoreHistory.FtpServer = currentUser.FtpServer;
                                            if (imageCheckerRestore != null)
                                                restoreHistory.ImageCheckerRestoreId = imageCheckerRestore.Id;
                                            restoreHistory.IsActive = true;
                                            restoreHistory.IsSuccess = isSuccess;
                                            restoreHistory.PathFileLocal = f.PathFile;
                                            restoreHistory.PathFileServer = remoteFile;
                                            restoreHistory.RestoreDate = DateTime.Now;
                                            restoreHistory.UserId = currentUser.Id;
                                            restoreHistory.Volume = f.Volumne.ToString();

                                            restoreHistoryService.Add(restoreHistory);
                                            if (isSuccess)
                                            {
                                                index = +1;
                                            }
                                        }
                                    }
                                }
                                //update IsRestoring 
                                lastest.IsRestore = false;
                                imageCheckerService.Update(lastest);
                                UpdateImageCheckerRestore(imageCheckerRestore.Id, true, "Restored Successful.");
                                return Json(new ResponeResult() { IsSuccess = true, Data = true }, JsonRequestBehavior.AllowGet);
                            }

                        }


                    }
                }
            }
            catch (Exception ce)
            {
                if (lastestId != Guid.Empty)
                {
                    var lstImage = imageCheckerService.Get(lastestId);
                    if (lstImage != null)
                    {
                        lstImage.IsRestore = false;
                        imageCheckerService.Update(lstImage);
                    }
                }
                TrackImageCheckerRestore(Guid.Empty, false, Guid.Parse(currentUser.Id), Guardian.Models.Action.Restore.ToString(), ce.Message);
                return Json(new ResponeResult() { IsSuccess = false, ErrorMessage = ce.Message, Data = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new ResponeResult() { IsSuccess = false, ErrorMessage = "Occured exception during restore, please contract with adminstrator to restore manual.", Data = true }, JsonRequestBehavior.AllowGet);
        }
        private void RestoreToFTP(ImageCheckerRestore restore, string servername, int port, string username, string password, FolderLocalInfo folder, int totalitem, out int index, string serverpath, User currentUser)
        {

            if (folder != null)
            {
                string pathCreateFolder = serverpath.Equals("/") ? folder.FolderName : String.Format("{0}/{1}", serverpath, folder.FolderName);
                //create folder 
                bool isSuccessCreateFolder = scanService.CreateDirectory(pathCreateFolder, username, password, servername);
                if (isSuccessCreateFolder)
                {
                    index = +1;
                    if (folder.FolderLocalInfos.Count > 0)
                    {
                        foreach (FolderLocalInfo f in folder.FolderLocalInfos)
                        {
                            //create child folder 
                            RestoreToFTP(restore, servername, port, username, password, f, totalitem, out index, pathCreateFolder, currentUser);
                        }
                    }
                    //exist the files 
                    if (folder.FileLocalInfos.Count > 0)
                    {
                        foreach (FileLocalInfo f in folder.FileLocalInfos)
                        {
                            string remoteFile = String.Format("{0}/{1}", pathCreateFolder, f.FileName);
                            string messageError = string.Empty;
                            //upload the file to folder 
                            bool isSuccess = scanService.UploadFtpFile(remoteFile, f.PathFile, username, password, servername, ref messageError);
                            RestoreHistory restoreHistory = new RestoreHistory();
                            restoreHistory.Id = Guid.NewGuid();
                            restoreHistory.ErrorMessage = messageError;
                            restoreHistory.FtpAccount = username;
                            restoreHistory.FtpPassword = password;
                            restoreHistory.FtpPort = 21;
                            restoreHistory.FtpServer = servername;
                            if(restore!=null)
                            restoreHistory.ImageCheckerRestoreId =  restore.Id;

                            restoreHistory.IsActive = true;
                            restoreHistory.IsSuccess = isSuccess;
                            restoreHistory.PathFileLocal = f.PathFile;
                            restoreHistory.PathFileServer = remoteFile;
                            restoreHistory.RestoreDate = DateTime.Now;
                            restoreHistory.UserId = currentUser.Id;
                            restoreHistory.Volume = f.Volumne.ToString();

                            restoreHistoryService.Add(restoreHistory);
                            if (isSuccess)
                            {
                                index = +1;
                            }
                        }
                    }
                }
            }
            index = +0;
        }
        #endregion

        #region History Restore
        [HttpPost]
        public ActionResult GetListImageCheckerRestore()
        {
            List<ImageCheckerRestore> list = new List<ImageCheckerRestore>();
            try
            {
                var id = this.User.Identity.GetUserId();
                if (id != null)
                {
                    list = imageCheckerRestoreService.GetRestoreHistoryUserId(id);
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetListRestoreHistoryDetailByRestoreId(Guid imageCheckerRestoreId)
        {
            List<RestoreHistory> list = new List<RestoreHistory>();
            try
            {
                if (imageCheckerRestoreId != null)
                {
                    list = restoreHistoryService.GetByImageCheckerRestoreId(imageCheckerRestoreId);
                }
            }
            catch (Exception ce)
            {
                log.Error(ce);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MarkIsReadNotification(ImageCheckerRestore entity)
        {
            bool isSuccess = UpdateImageCheckerRestore(entity) != null;
            return Json(new ResponeResult() { IsSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetNotificationByCurrentUser()
        {
            var currentUser = getCurrentUser();
            List<ImageCheckerRestore> list = new List<ImageCheckerRestore>();
            if (currentUser != null)
            {
                Guid userId = Guid.Parse(currentUser.Id);
                list = imageCheckerRestoreService.Get().Where(c => c.UserId.Equals(userId) && !c.IsRead).ToList();
                if (list != null && list.Count > 0)
                    list = list.OrderByDescending(c => c.RestoredDate).ToList();
            }
            return Json(new ResponeResult() { IsSuccess = true, Data = list }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ClearNotifications()
        {
            var currentUser = getCurrentUser();
            List<ImageCheckerRestore> list = new List<ImageCheckerRestore>();
            if (currentUser != null)
            {
                Guid userId = Guid.Parse(currentUser.Id);
                list = imageCheckerRestoreService.Get().Where(c => c.UserId.Equals(userId) && !c.IsRead).OrderByDescending(c => c.RestoredDate).ToList();
            }
            foreach (ImageCheckerRestore r in list)
            {
                r.IsRead = true;
                imageCheckerRestoreService.Update(r);
            }

            return Json(new ResponeResult() { IsSuccess = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
