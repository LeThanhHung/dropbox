﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Guardian.Models
{
    public enum Action
    { 
        Backup, 
        Restore , 
        Scan, 
        Generate ,
    }

    public enum Status
    { 
        Completed , 
        Running , 
        Error , 
        Infected,
        Backedup,
        Restored 
    }
    public class ProcessRestoreViewModel
    {
        public string UserId { get; set; }
        public string ClientId { get; set; }
        public decimal Percent { get; set; }
        public string TextProcess { get; set; }
        public bool IsCompleted { get; set; }

        public bool IsError { get; set; }
    }
}