[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Guardian.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Guardian.App_Start.NinjectWebCommon), "Stop")]

namespace Guardian.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Oas.Infrastructure.Services;
    using Oas.Infrastructure;
    using Oas.Infrastructure.Domain;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Data.Entity;

    public static class NinjectWebCommon 
    {
        public static readonly Bootstrapper bootstrapper = new Bootstrapper();
         

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel); 
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<DbContext>().To<DatabaseContext>();

            #region Service
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IHistoryService>().To<HistoryService>();
            kernel.Bind<IScanDetailService>().To<ScanDetailService>();
            kernel.Bind<IScanService>().To<ScanService>();
            kernel.Bind<IScanResultService>().To<ScanResultService>();
            kernel.Bind<IScanResultDetailService>().To<ScanResultDetailService>();
            kernel.Bind<IImageCheckerService>().To<ImageCheckerService>();
            kernel.Bind<IImageCheckerDetailService>().To<ImageCheckerDetailService>();
            kernel.Bind<IRestoreHistoryService>().To<RestoreHistoryService>();
            kernel.Bind<IImageCheckerRestoreService>().To<ImageCheckerRestoreService>();
            kernel.Bind<ILoggingActionClientService>().To<LoggingActionClientService>();
            kernel.Bind<IVulnerScanService>().To<VulnerScanService>();  
            #endregion

            #region Repositories

            kernel.Bind(typeof(IRepository<>)).To(typeof(Repository<>));
            kernel.Bind(typeof(IRepository<User>)).To(typeof(Repository<User>));
            kernel.Bind(typeof(IRepository<Role>)).To(typeof(Repository<Role>));
            kernel.Bind(typeof(IRepository<IdentityRole>)).To(typeof(Repository<Role>));
            kernel.Bind(typeof(IRepository<ScanDetail>)).To(typeof(Repository<ScanDetail>));
            kernel.Bind(typeof(IRepository<History>)).To(typeof(Repository<History>));
            kernel.Bind(typeof(IRepository<ScanResult>)).To(typeof(Repository<ScanResult>));
            kernel.Bind(typeof(IRepository<ScanResultDetail>)).To(typeof(Repository<ScanResultDetail>));
            kernel.Bind(typeof(IRepository<ImageChecker>)).To(typeof(Repository<ImageChecker>));
            kernel.Bind(typeof(IRepository<ImageCheckerDetail>)).To(typeof(Repository<ImageCheckerDetail>));
            kernel.Bind(typeof(IRepository<RestoreHistory>)).To(typeof(Repository<RestoreHistory>));
            kernel.Bind(typeof(IRepository<ImageCheckerRestore>)).To(typeof(Repository<ImageCheckerRestore>));
            kernel.Bind(typeof(IRepository<LoggingActionClient>)).To(typeof(Repository<LoggingActionClient>));
            kernel.Bind(typeof(IRepository<VulnerScan>)).To(typeof(Repository<VulnerScan>));  
            #endregion

            kernel.Bind<BackgroundWorker>().To<BackgroundWorker>();
        }        
    }
}
