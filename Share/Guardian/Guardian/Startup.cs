﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



using Microsoft.Owin;
using Owin;
using Oas.Infrastructure.Domain;
using Oas.Infrastructure;
using Oas.Infrastructure.Services;
using Microsoft.AspNet.SignalR;
using System.Linq;
using Guardian.Hubs;
using System.Web.Hosting;

[assembly: OwinStartup(typeof(Guardian.Startup))]
namespace Guardian
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            IRepository<History> historyRepository = new Repository<History>(new DatabaseContext());
            IRepository<ScanDetail> scanDetailRepository = new Repository<ScanDetail>(new DatabaseContext());
            IRepository<ScanResult> scanResultRepository = new Repository<ScanResult>(new DatabaseContext());
            IRepository<ScanResultDetail> scanResultDetailRepository = new Repository<ScanResultDetail>(new DatabaseContext());
            IRepository<ImageChecker> imageCheckerRepository = new Repository<ImageChecker>(new DatabaseContext());
            IRepository<User> userRepository = new Repository<User>(new DatabaseContext());
            IRepository<Role> roleRepository = new Repository<Role>(new DatabaseContext());

            IHistoryService historyService = new HistoryService(historyRepository , scanResultDetailRepository,scanResultRepository,scanDetailRepository);
            IScanDetailService scanDetailService = new ScanDetailService(scanDetailRepository);
            IScanResultService scanResultService = new ScanResultService(scanResultRepository);
            IScanResultDetailService scanResultDetailService = new ScanResultDetailService(scanResultDetailRepository);
            IScanService scanService = new ScanService(historyRepository,scanResultDetailRepository,scanResultRepository,scanDetailRepository);
            IImageCheckerService imageCheckerService = new ImageCheckerService(imageCheckerRepository);
            IUserService userService = new UserService(userRepository, roleRepository);

            GlobalHost.DependencyResolver.Register(typeof(ServiceHub), () => new ServiceHub(userService,imageCheckerService, historyService, scanDetailService, scanService, scanResultService, scanResultDetailService));
            app.MapSignalR();
        }
    }
}
 
