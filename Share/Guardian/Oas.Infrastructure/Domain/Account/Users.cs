﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel;

namespace Oas.Infrastructure.Domain
{
    public class User : IdentityUser
    {
        public int TypeOfRegister { get; set; } //1:Física 2 : Jurídica        3:Estrangeiro
        public string Name { get; set; }           // Nome
        public string LastName { get; set; }      //Sobrenome
        public string Email { get; set; }
        public string CPF { get; set; }
        //E-mail
        public string CNPJ { get; set; }       //CNPJ
        public bool Contribuinte { get; set; } //Contribuinte
        public string TELEFONE { get; set; }        // TELEFONE
        public string CELULAR { get; set; }       //CELULAR
        public string OutroTelefone { get; set; }    //OutroTelefone
        public string Gender { get; set; }       //Sexo

        public string Pais { get; set; }

        public string ENDERECO { get; set; }
        public string NUMERO { get; set; }
        public string COMPLEMENTO { get; set; }
        public string BAIRRO { get; set; }
        public string CIDADE { get; set; }
        public string ESTADO { get; set; }
        public string PontodeReferencia { get; set; }        
        public string RazaoSocial { get; set; }
        public string InscricaoEstadual { get; set; }
        public bool Isento { get; set; }

        public string Passaporte { get; set; }

        public string Company { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }          //CEP
        public string Domain { get; set; }
        public string SealStatus { get; set; }
        public string SealType { get; set; }
        public DateTime ExpireDate{ get; set; }
        public DateTime CreatedDate { get; set; }
        public string AccessToken { get; set; }
        public string PathLogo { get; set; }
        public string FtpServer { get; set; }
        public string FtpAccount { get; set; }
        public string FtpPassword { get; set; }
        [DefaultValue(false)]
        public bool IsVerifiedFtpAccount { get; set; }
        public int FtpPort { get; set; }
        public string FtpFolder { get; set; }

        //extra more fields
        public string RG { get; set; }
        public DateTime? DateOfBirth { get; set; }
        
       
    }
}
