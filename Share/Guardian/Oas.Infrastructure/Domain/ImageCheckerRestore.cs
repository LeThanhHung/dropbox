﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Domain
{
    public class ImageCheckerRestore
    {
        [Key]
        public Guid Id { get; set; }
        public Guid? ImageCheckerId { get; set; }
        public DateTime RestoredDate { get; set; }
        public bool IsSuccess { get; set; }
        public string Action { get; set;}
        public string ErrorMessage { get; set; }
        public Guid UserId { get; set; }

        public bool IsRead { get; set; }

        private bool isActive = true;
        [DefaultValue(true)]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
    }
}
