﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Domain
{
    public class VulnerScan
    {
        [Key] 
        public Guid Id { get; set; }   
        public string Name { get; set; }
        public string GitPath { get; set; }
        public string CreateDate { get; set; }      
        public string ReportPath { get; set; }    
        public string UserId { get; set; }

        public bool IsScanning { get; set; }
        public bool IsInQueue { get; set; }
        public bool IsCompleted { get; set; }

        public bool IsError { get; set; }

        public string ErrorMessage { get; set; }

        private bool isActive = true;
        [DefaultValue(true)]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
    }
}
