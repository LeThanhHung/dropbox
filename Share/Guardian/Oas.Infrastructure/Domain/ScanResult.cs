﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Domain
{
    public class ScanResult
    {
        public Guid Id { get; set; }
        public Guid ScanDetailId { get; set; }
        public bool Rescan_available { get; set; }
        public int Scan_all_result_i { get; set; }
        public DateTime Start_time { get; set; }
        public int Total_time { get; set; }
        public int Total_avs { get; set; }
        public decimal Progress_percentage { get; set; }
        public bool In_queue { get; set; }
        public string Scan_all_result_a { get; set; }

        //file Info
        public string MD5 { get; set; }
        public string Sha1 { get; set; }
        public string Sha256 { get; set; }
        public string FileTypeCategory { get; set; }
        public string FileTypeDescription { get; set; }
        public string FileTypeExtension { get; set; }

        [ForeignKey("ScanDetailId")]
        public ScanDetail ScanDetail { get; set; }

        private bool isActive = true;
        [DefaultValue(true)]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
    }
}
