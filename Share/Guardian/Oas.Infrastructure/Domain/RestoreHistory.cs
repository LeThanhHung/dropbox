﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Domain
{
    public class RestoreHistory
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ImageCheckerRestoreId { get; set; }
        public string PathFileLocal { get; set; }
        public string PathFileServer { get; set; }
        public string Volume { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime RestoreDate { get; set; }
        public string UserId { get; set; }
        public string FtpServer { get; set; }
        public string FtpAccount { get; set; }
        public string FtpPassword { get; set; }         
        public int FtpPort { get; set; }

        [ForeignKey("ImageCheckerRestoreId")]
        public ImageCheckerRestore ImageCheckerRestore { get; set; }

        private bool isActive = true;
        [DefaultValue(true)]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
         
    }
}
