﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Domain
{
    public class ImageChecker
    {
        [Key]
        public Guid Id { get; set; } 
        public string UserId { get; set; }
        public string Location { get; set; }
        /// <summary>
        /// 1- Moth , 2 run in week 
        /// </summary>
        public int RunInMothOrWeek { get; set; }        
        public string StartTime { get; set; } 
        public bool IsStop { get; set; }
        public bool IsDownloaded { get; set; }
        public bool IsDownloading { get; set; }
        public bool IsRestore { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModify { get; set; }

        private bool isActive = true;
        [DefaultValue(true)]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
    }
}
