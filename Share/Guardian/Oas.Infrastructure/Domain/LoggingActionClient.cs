﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Domain
{
    public class LoggingActionClient
    {
        [Key]
        public Guid Id { get; set; }
        public string Action { get; set; }  
        public string Module { get; set; }
        public Guid UserId { get; set; }
        public string IP { get; set; }

        private bool isActive = true;
        [DefaultValue(true)]
        public bool IsActive { get { return isActive; } set { isActive = value; } }
    }
}
