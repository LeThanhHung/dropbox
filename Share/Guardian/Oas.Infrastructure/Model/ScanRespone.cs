﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Model
{
    public class ScanRespone
    {
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
    }
}
