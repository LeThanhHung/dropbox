﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Model
{
    public class FolderLocalInfo
    {
        public string FolderName { get; set; }
        public List<FolderLocalInfo> FolderLocalInfos { get; set; }
        public List<FileLocalInfo> FileLocalInfos { get; set; }
    }
    public class FileLocalInfo
    {
        public string FileName { get; set; }
        public string PathFile { get; set; }
        public int Volumne{get;set;}
    }
}
