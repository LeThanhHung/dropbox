﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Model
{
    public class HistoryModel
    {
        public History History { get; set; }
        public List<ScanDetailModel> ScanDetails { get; set; }
    }

    public class ScanDetailModel
    {
        public ScanDetail ScanDetail { get; set; }
        public ScanResult ScanResult{ get; set; }
        public List<ScanResultDetail> ScanResultDetails { get; set; }
    }

    public class SummaryReportScan
    {
        public string Name { get; set; }
        public decimal PercentCompleted { get; set; }
    }

    public class HistoryLookupModel
    {
        public History History { get; set; }
        public List<ScanDetail> ScanDetails { get; set; }
        public List<ScanResult> ScanResults { get; set; }
    }
     
}
