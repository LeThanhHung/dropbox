﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Model
{
    public class VulHistoryModel
    {
        public string Name { get; set; }
        public string GitPath { get; set; }
        public string CreateDate { get; set; }
        public Guid Id { get; set; }
        public string ReportPath { get; set; }

        public bool IsCompleted { get; set; }
        public bool IsScanning { get; set; }
        public bool IsInQueue { get; set; }
        public bool IsError { get; set; }

    }

    public class VulDetail
    {
        public string RuleID { get; set; }
        public string Type { get; set; }
        public string File { get; set; }
        public string Description { get; set; }
        public string Snippet { get; set; }
        public string Plugin { get; set; }
        public string Severity { get; set; }
        public string Reference { get; set; }
        public string Location { get; set; }
        public string UserInput { get; set; }
        public string RenderPath { get; set; }
        public string Code { get; set; }
    }

    public class NewVulScan
    {
        public Guid Id { get; set; } 
        public string ScanName { get; set; }
        public string GitUrl { get; set; }

        public bool IsGit { get; set; }
    }
}
