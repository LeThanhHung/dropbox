﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Oas.Infrastructure.Domain;

namespace Oas.Infrastructure
{
    public class DatabaseContext : IdentityDbContext<User>
    {
        public DatabaseContext()
            : base(Constant.AppConnection)
        {
        }

        IDbSet<History> Histories { get; set; }
        IDbSet<ScanDetail> ScanDetails { get; set; }
        IDbSet<ScanResult> ScanResults { get; set; } 
        IDbSet<ScanResultDetail> ScanResultDetails { get; set; }
        IDbSet<ImageChecker> ImageCheckers { get; set; }
        IDbSet<ImageCheckerDetail> ImageCheckerDetails { get; set; }
        IDbSet<ImageCheckerRestore> ImageCheckerRestores { get; set; }       
        IDbSet<RestoreHistory> RestoreHistories { get; set; }
        IDbSet<Logging> Loggings { get; set; }
        IDbSet<LoggingActionClient> LoggingActionClients { get; set;}    
        IDbSet<VulnerScan> VulnerScans { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Remove unused conventions
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<IdentityUser>()
                .ToTable("Users");
            modelBuilder.Entity<User>()
                .ToTable("Users");

            modelBuilder.Entity<IdentityRole>()
            .ToTable("Roles");
            modelBuilder.Entity<Role>()
                .ToTable("Roles");

            modelBuilder.Entity<IdentityUserRole>()
                .ToTable("UserRoles");

        }


    }
}
