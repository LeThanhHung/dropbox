﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public class ImageCheckerDetailService :ServiceBase<ImageCheckerDetail>, IImageCheckerDetailService
    {
        private readonly IRepository<ImageCheckerDetail> ImageCheckerDetailRepository;

        public ImageCheckerDetailService(IRepository<ImageCheckerDetail> ImageCheckerDetailRepository)
            : base(ImageCheckerDetailRepository)
        {
            this.ImageCheckerDetailRepository = ImageCheckerDetailRepository;
        }

        protected override Guid GetKey(ImageCheckerDetail entity)
        {
            return entity.Id;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool BulkInsert(List<ImageCheckerDetail> list)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AppConnection"].ConnectionString;
            if (!String.IsNullOrEmpty(connectionString))
            {
                try
                {
                    var result = (from a in list select a);
                    
                    DataTable reader = EntityDataReaderExtensions.ToDataTable<ImageCheckerDetail>(result);
                    //bulk data to db 
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        using (SqlTransaction tran = con.BeginTransaction())
                        {

                            SqlBulkCopy bc = new SqlBulkCopy(con,
                            SqlBulkCopyOptions.CheckConstraints |
                            SqlBulkCopyOptions.FireTriggers |
                            SqlBulkCopyOptions.KeepNulls, tran);

                            bc.BatchSize = 1000;
                            bc.DestinationTableName = "ImageCheckerDetails";
                            bc.WriteToServer(reader);

                            tran.Commit();
                        }
                        con.Close();
                        return true;
                    }
                }
                catch
                { return false; }
            }
            return false;
        } 
       
    }
}
