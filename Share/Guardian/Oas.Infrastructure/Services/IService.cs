﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public interface IService<TEntity> where TEntity : class
    {
        IList<TEntity> Get();

        IList<TEntity> Get(params Expression<Func<TEntity, object>>[] includeProperties);

        TEntity Get(Guid Id);

        TEntity Get(Guid Id, params Expression<Func<TEntity, object>>[] includeProperties);

        TEntity Add(TEntity entity);

        TEntity Update(TEntity entity);

        bool Remove(Guid Id);

        TEntity Save(TEntity entity);

    }
}
