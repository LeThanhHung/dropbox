﻿using Oas.Infrastructure.Domain;
using Oas.Infrastructure.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public interface IVulnerScanService : IService<VulnerScan>
    {
        VulnerScan GetByNameAndUser(string packageName, string userId);
        List<VulnerScan> GetByUserId(string userId);
    }
}
