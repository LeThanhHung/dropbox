﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public class ImageCheckerService :ServiceBase<ImageChecker>, IImageCheckerService
    {
        private readonly IRepository<ImageChecker> ImageCheckerRepository;

        public ImageCheckerService(IRepository<ImageChecker> ImageCheckerRepository)  :base(ImageCheckerRepository)
        {
            this.ImageCheckerRepository = ImageCheckerRepository;
        }


        protected override Guid GetKey(ImageChecker entity)
        {
            return entity.Id;
        }
        
         
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool BulkInsert(List<ImageChecker> list)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AppConnection"].ConnectionString;
            if (!String.IsNullOrEmpty(connectionString))
            {
                try
                {
                    var result = (from a in list select a);
                    
                    IDataReader reader = result.AsDataReader();
                    //bulk data to db 
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        using (SqlTransaction tran = con.BeginTransaction())
                        {

                            SqlBulkCopy bc = new SqlBulkCopy(con,
                            SqlBulkCopyOptions.CheckConstraints |
                            SqlBulkCopyOptions.FireTriggers |
                            SqlBulkCopyOptions.KeepNulls, tran);

                            bc.BatchSize = 1000;
                            bc.DestinationTableName = "ImageCheckers";
                            bc.WriteToServer(reader);

                            tran.Commit();
                        }
                        con.Close();
                        return true;
                    }
                }
                catch
                { return false; }
            }
            return false;
        }


        public IList<ImageChecker> GetByUserId(string userId)
        {
            return ImageCheckerRepository.Get.Where(c=>c.UserId.Equals(userId)).ToList(); 
        }
    }
}
