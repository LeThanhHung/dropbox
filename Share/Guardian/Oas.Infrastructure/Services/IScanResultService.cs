﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public  interface IScanResultService      :IService<ScanResult>
    {   
        bool BulkInsert(List<ScanResult> list);

        ScanResult GetIncludeScanDetailById(Guid Id);
    }
}
