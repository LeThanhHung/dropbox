﻿using Oas.Infrastructure.Domain;
using Oas.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
namespace Oas.Infrastructure.Services
{
    public interface IScanService    
    {
        /// <summary>
        /// start backup folder
        /// </summary>
        /// <param name="servername"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="folder"></param>
        /// <param name="isRoot"></param>
        /// <param name="folderChange"></param>
        /// <param name="parentId"></param>
        /// <param name="folderLocal"></param>
        /// <param name="queueId"></param>
        /// <param name="serverLocalFolder"></param>
        /// <returns></returns>
        Task<List<ScanDetail>> BackupAllFileOfFTP(string servername, int port, string username, string password, string folder, bool isRoot, string folderChange, Guid parentId, string folderLocal, Guid queueId, string serverLocalFolder, bool isNeedDownload = true);
        
        
        /// <summary>
        /// get infomation of folder root on ftp
        /// </summary>
        /// <param name="servername"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="folder"></param>
        /// <param name="isRoot"></param>
        /// <param name="folderChange"></param>
        /// <param name="parentId"></param>
        /// <param name="forlderLocal"></param>
        /// <param name="queueId"></param>
        /// <param name="serverLocalFolder"></param>
        /// <returns></returns>
        Task<List<ScanDetail>> GetAllFileOfFTP(string servername, int port, string username, string password, string folder, bool isRoot, string folderChange, Guid parentId, string forlderLocal, Guid queueId, string serverLocalFolder, bool isNeedDownload = true);

        /// <summary>
        /// scan directly file from ftp folder 
        /// </summary>
        /// <param name="servername"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        string ScanFtpFile(string servername, int port, string username, string password, string filePath);

        /// <summary>
        /// Scan a file with an online scan service
        /// </summary>
        /// <param name="filePath">absolute path of a file on ftp server</param>
        /// <returns>feedback from online scan service</returns>
        string ScanFile(string servername, int port, string username, string password, string filePath);

        /// <summary>
        /// Get information of a currently being scanned file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        string GetScanningFileInfo(string rest_ip, string data_id, string fileName);

         
        /// <summary>
        /// Execute rescan async
        /// </summary>
        /// <param name="scanDetailId"></param>
        /// <param name="scanResultId"></param>
        /// <returns></returns>
        Task ExcuteRescanAsync(Guid scanDetailId, Guid scanResultId);
        /// <summary>
        /// get report async
        /// </summary>
        /// <param name="data_id"></param>
        /// <param name="rest_ip"></param>
        /// <param name="fileName"></param>
        /// <param name="ScanResultId"></param>
        /// <returns></returns>
        bool GetReportInfoAsync(string data_id, string rest_ip, string fileName, Guid ScanResultId);
        /// <summary>
        /// rollback result excescan if occured error
        /// </summary>
        /// <param name="historyId"></param>
        /// <returns></returns>
        bool RollbackResultScan(Guid historyId);

        /// <summary>
        /// verify ftp account
        /// </summary>
        /// <param name="servername"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="folderParent"></param>
        /// <param name="fileLocal"></param>
        /// <returns></returns>
        bool VerifyPermissionAccount(string servername, int port, string username, string password, string fileLocal);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="remoteFile"></param>
        /// <param name="localFile"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        bool UploadFtpFile(string remoteFile, string localFile, string username, string password, string host,ref string messageError);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="deleteFile"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        bool DeleteFileFtp(string deleteFile, string username, string password, string host);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentFileNameAndPath"></param>
        /// <param name="newFileName"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        bool RenameFtpFile(string currentFileNameAndPath, string newFileName, string username, string password, string host);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newDirectory"></param>
        /// <param name="newFileName"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        bool CreateDirectory(string newDirectory , string username, string password, string host);

        bool DeleteFtpDirectory(string deleteDirectory, string username, string password, string host);

        bool ZipCurrentSource(string pathFolderNeedZip, string pathzipedFolder);

        FolderLocalInfo ReadArtchitectFolder(string rootPath,out int totalFolder , out int totalFile);
         
    }
}