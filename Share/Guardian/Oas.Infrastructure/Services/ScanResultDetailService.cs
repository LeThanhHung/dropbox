﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public class ScanResultDetailService :ServiceBase<ScanResultDetail>, IScanResultDetailService
    {
        private readonly IRepository<ScanResultDetail> ScanResultDetailRepository;

        public ScanResultDetailService(IRepository<ScanResultDetail> ScanResultDetailRepository)  :base(ScanResultDetailRepository)
        {
            this.ScanResultDetailRepository = ScanResultDetailRepository;
        }

        protected override Guid GetKey(ScanResultDetail entity)
        {
            return entity.Id;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool BulkInsert(List<ScanResultDetail> list)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AppConnection"].ConnectionString;
            if (!String.IsNullOrEmpty(connectionString))
            {
                try
                {
                    var result = (from a in list select a);
                    
                    IDataReader reader = result.AsDataReader();
                    //bulk data to db 
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        using (SqlTransaction tran = con.BeginTransaction())
                        {

                            SqlBulkCopy bc = new SqlBulkCopy(con,
                            SqlBulkCopyOptions.CheckConstraints |
                            SqlBulkCopyOptions.FireTriggers |
                            SqlBulkCopyOptions.KeepNulls, tran);

                            bc.BatchSize = 1000;
                            bc.DestinationTableName = "ScanResultDetails";
                            bc.WriteToServer(reader);

                            tran.Commit();
                        }
                        con.Close();
                        return true;
                    }
                }
                catch
                { return false; }
            }
            return false;
        }

        /// <summary>
        /// Delete a batch scanresultdetail
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="nameTable"></param>
        /// <param name="parentField"></param>
        /// <returns></returns>
        public bool DeleteBatch(Guid parentId, string nameTable, string parentField)
        {
           return ScanResultDetailRepository.DeleteBatch(parentId, nameTable, parentField);
        }
    }
}
