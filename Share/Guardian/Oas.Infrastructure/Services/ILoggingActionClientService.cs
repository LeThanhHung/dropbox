﻿using Oas.Infrastructure.Domain;
using Oas.Infrastructure.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public interface ILoggingActionClientService : IService<LoggingActionClient>
    {     
    }
}
