﻿using Oas.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oas.Infrastructure.Services
{
    public interface IScanResultDetailService             :IService<ScanResultDetail>
    {
       
        bool BulkInsert(List<ScanResultDetail> list);

        bool DeleteBatch(Guid parentId, string nameTable, string parentField);
    }
}
