﻿using Ninject.Modules;
using Oas.Infrastructure;
using Oas.Infrastructure.Domain;
using Oas.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guardian.BackupTaskService
{
    public class ServiceNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<DbContext>().To<DatabaseContext>();

            #region Service
            Bind<IUserService>().To<UserService>();
            Bind<IHistoryService>().To<HistoryService>();
            Bind<IScanDetailService>().To<ScanDetailService>();
            Bind<IScanService>().To<ScanService>();
            Bind<IScanResultService>().To<ScanResultService>();
            Bind<IScanResultDetailService>().To<ScanResultDetailService>();
            Bind<IImageCheckerService>().To<ImageCheckerService>();
            Bind<IImageCheckerDetailService>().To<ImageCheckerDetailService>();
            Bind<IRestoreHistoryService>().To<RestoreHistoryService>();
            Bind<IImageCheckerRestoreService>().To<ImageCheckerRestoreService>();
            Bind<ILoggingActionClientService>().To<LoggingActionClientService>();
            Bind<IVulnerScanService>().To<VulnerScanService>();
            #endregion

            #region Repositories

            Bind(typeof(IRepository<>)).To(typeof(Repository<>));
            Bind(typeof(IRepository<User>)).To(typeof(Repository<User>));
            Bind(typeof(IRepository<Role>)).To(typeof(Repository<Role>)); 
            Bind(typeof(IRepository<ScanDetail>)).To(typeof(Repository<ScanDetail>));
            Bind(typeof(IRepository<History>)).To(typeof(Repository<History>));
            Bind(typeof(IRepository<ScanResult>)).To(typeof(Repository<ScanResult>));
            Bind(typeof(IRepository<ScanResultDetail>)).To(typeof(Repository<ScanResultDetail>));
            Bind(typeof(IRepository<ImageChecker>)).To(typeof(Repository<ImageChecker>));
            Bind(typeof(IRepository<ImageCheckerDetail>)).To(typeof(Repository<ImageCheckerDetail>));
            Bind(typeof(IRepository<RestoreHistory>)).To(typeof(Repository<RestoreHistory>));
            Bind(typeof(IRepository<ImageCheckerRestore>)).To(typeof(Repository<ImageCheckerRestore>));
            Bind(typeof(IRepository<LoggingActionClient>)).To(typeof(Repository<LoggingActionClient>));
            Bind(typeof(IRepository<VulnerScan>)).To(typeof(Repository<VulnerScan>));
            #endregion

            
        }
    }
}
