﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace DevexpressProjectGridView
{
    public class Users
    {
        private int _id;
        private string _name;
        private string _password;
        private string _address;
        private int _age;
        private string _birthday;
        private string _gender;

        public Users(int id, string name, string password, string address, int age, string gender, string birthday)
        {
            _id = id;
            _name = name;
            _password = password;
            _address = address;
            _age = age;
            _gender = gender;
            _birthday = birthday;
        }

        public Users()
        {

        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }
        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        public string Birthday
        {
            get { return _birthday; }
            set { _birthday = value; }
        }
    }
    public partial class GridView : System.Web.UI.Page
    {
        private readonly string _strConnetion = ConfigurationManager.ConnectionStrings["DBConnection"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {

            BindDataUser();
        }

        public void BindDataUser()
        {
            var result = ConvertData();
            gvDataUser.DataSource = result;
            gvDataUser.DataBind();
        }

        public List<Users> ConvertData()
        {
            var listUsers = new List<Users>();
            using (var con = new SqlConnection(_strConnetion))
            {
                con.Open();
                var viewData = new SqlCommand("selectUsers", con) { CommandType = CommandType.StoredProcedure };
                var reader = viewData.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var userInfo = new Users
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = Convert.ToString(reader["Name"]),
                            Password = Convert.ToString(reader["Password"]),
                            Address = Convert.ToString(reader["Address"]),
                            Age = Convert.ToInt32(reader["Age"]),
                            Gender = Convert.ToString(reader["Gender"]),
                            Birthday = Convert.ToString(reader["Birthday"])
                        };
                        listUsers.Add(userInfo);
                    }
                }
                con.Close();
            }
            return listUsers;
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            var userObj = new Users
            {
                Name = Convert.ToString(txtName.Text),
                Password = Convert.ToString(txtPassword.Text),
                Address = Convert.ToString(txtAddress.Text),
                Age = Convert.ToInt32(txtAge.Text),
                Gender = Convert.ToString(rblGender.SelectedItem.Value),
                Birthday = Convert.ToString(deBirthday.Text),
            };
            var result = InsertUsers(userObj);
            lblMessages.Text = result;
            BindDataUser();

        }

        public string InsertUsers(Users userInfo)
        {
            string messages;
            using (var con = new SqlConnection(_strConnetion))
            {
                con.Open();
                var insertUser = new SqlCommand("insertUsers", con) { CommandType = CommandType.StoredProcedure };
                insertUser.Parameters.AddWithValue("@Name", userInfo.Name);
                insertUser.Parameters.AddWithValue("@Password", userInfo.Password);
                insertUser.Parameters.AddWithValue("@Address", userInfo.Address);
                insertUser.Parameters.AddWithValue("@Age", userInfo.Age);
                insertUser.Parameters.AddWithValue("@Gender", userInfo.Gender);
                insertUser.Parameters.AddWithValue("@Birthday", userInfo.Birthday);

                var result = insertUser.ExecuteNonQuery();
                if (result == 1)
                {
                    messages = userInfo.Name + " Insert Successflly";
                }
                else
                {
                    messages = userInfo.Name + " Insert Not Successflly";
                }
                con.Close();
            }
            return messages;
        }
        protected void gvDataUser_OnRowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            lblMessages.Text = string.Empty;
            var id = Convert.ToInt32(e.NewValues[0]);
            var name = Convert.ToString(e.NewValues[1]);
            var password = Convert.ToString(e.NewValues[2]);
            var address = Convert.ToString(e.NewValues[3]);
            var age = Convert.ToInt32(e.NewValues[4]);
            var valueGender = Convert.ToString(e.NewValues[5]);
            var gender = true;
            switch (valueGender)
            {
                case "Female":
                    gender = false;
                    break;
            }
            var birthday = Convert.ToString(e.NewValues[6]);
            using (var con = new SqlConnection(_strConnetion))
            {
                con.Open();
                var updateUser = new SqlCommand("updateUsers", con) { CommandType = CommandType.StoredProcedure };
                updateUser.Parameters.AddWithValue("@Id", id);
                updateUser.Parameters.AddWithValue("@Name", name);
                updateUser.Parameters.AddWithValue("@Password", password);
                updateUser.Parameters.AddWithValue("@Address", address);
                updateUser.Parameters.AddWithValue("@Age", age);
                updateUser.Parameters.AddWithValue("@Gender", gender);
                updateUser.Parameters.AddWithValue("@Birthday", birthday);
                var result = updateUser.ExecuteNonQuery();
                if (result == 1)
                {
                    var grid = sender as ASPxGridView;
                    if (grid != null)
                    {
                        grid.JSProperties["cpText"] = string.Format("The record {0} was updated update successflly", e.Keys[0]);
                        grid.JSProperties["cpShowPopup"] = true;
                    }
                }
                con.Close();
            }
            e.Cancel = true;

            BindDataUser();
        }
        protected void gvDataUser_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            lblMessages.Text = string.Empty;
            var dataKey = e.Keys[0].ToString();
            using (var con = new SqlConnection(_strConnetion))
            {
                con.Open();
                var deleteUser = new SqlCommand("deleteUsers", con) { CommandType = CommandType.StoredProcedure };
                deleteUser.Parameters.AddWithValue("@Id", dataKey);
                var result = deleteUser.ExecuteNonQuery();
                lblMessages.Text = result == 1 ? "Delete successflly" : "Delete not successflly";
                con.Close();
            }
            e.Cancel = true;
            BindDataUser();
        }
    }
}