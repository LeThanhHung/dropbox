﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GridView.aspx.cs" Inherits="DevexpressProjectGridView.GridView" %>

<%@ Import Namespace="System.Security.Cryptography" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .form-layout {
            margin: 20px auto;
        }

        h1 {
            text-align: center;
        }

        #lblMessages {
            color: red;
            text-align: center;
        }
        .popup {
            margin: 20px auto;
            width: 200px;
            height: auto;
        }
    </style>
    <script>
        function OnGridEndCallback(s, e) {
            if (s.cpFail) {
                delete s.cpFail.puMessages.SetHeaderText(s.cpTitle);
                window.puMessages.show();
            }
        }
    </script>
</head>
<body style="width: 100%">
    <form id="form1" runat="server">
        <div style="width: 100%">
            <h1>Gridview Data Manager</h1>
            <dx:ASPxLabel ID="lblMessages" runat="server" Text="ASPxLabel"></dx:ASPxLabel>
            <br />
            <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server" Theme="SoftOrange"
                EnableTheming="True" Width="60%" CssClass="form-layout">
                <Items>
                    <dx:LayoutGroup Caption="Insert User Info">
                        <Items>
                            <dx:LayoutItem Caption="Name">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxTextBox ID="txtName"
                                            runat="server" Width="170px" />
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <%-- And other layout items --%>
                            <dx:LayoutItem Caption="Password">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer runat="server">
                                        <dx:ASPxTextBox ID="txtPassword" runat="server" Text="" Password="True">
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Address">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer runat="server">
                                        <dx:ASPxTextBox ID="txtAddress" runat="server" Width="170px">
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Age">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer runat="server">
                                        <dx:ASPxTextBox ID="txtAge" runat="server" Width="170px">
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Gender">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer runat="server">
                                        <dx:ASPxRadioButtonList ID="rblGender" runat="server" SelectedIndex="0">
                                            <Items>
                                                <dx:ListEditItem Text="Male" Value="true" Selected="True" />
                                                <dx:ListEditItem Text="Female" Value="false" />
                                            </Items>
                                        </dx:ASPxRadioButtonList>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Birthday">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer runat="server">
                                        <dx:ASPxDateEdit ID="deBirthday" runat="server">
                                        </dx:ASPxDateEdit>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer runat="server">
                                        <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit"
                                            OnClick="btnSubmit_OnClick">
                                        </dx:ASPxButton>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                        </Items>
                    </dx:LayoutGroup>
                </Items>

            </dx:ASPxFormLayout>
        </div>
         <dx:ASPxPopupControl ID="popup" runat="server" ClientInstanceName="popup" CssClass="popup">
             <ContentCollection>
                   <dx:PopupControlContentControl runat="server">
                        <dx:ASPxLabel ID="lblMsg" runat="server" ClientInstanceName="lblMsg" Text="ASPxLabel">
                        </dx:ASPxLabel>
                    </dx:PopupControlContentControl>
             </ContentCollection>
         </dx:ASPxPopupControl>
        <br />
        <dx:ASPxGridView ID="gvDataUser" OnRowUpdating="gvDataUser_OnRowUpdating"
            OnRowDeleting="gvDataUser_OnRowDeleting"  Width="100%" runat="server" 
            AutoGenerateColumns="False" EnableTheming="True" Theme="Metropolis" 
            KeyFieldName="Id">
            <Columns>
                <dx:GridViewCommandColumn ShowClearFilterButton="True"
                    ShowDeleteButton="True" ShowEditButton="True"
                    ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="Id" VisibleIndex="2" ReadOnly="True" >
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Password" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Address" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Age" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Gender" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Birthday" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
            </Columns>
            <ClientSideEvents EndCallback="function (s,e){
                if(s.cpShowPopup){
                    delete s.cpShowPopup;
                    lblMsg.SetText(s.cpText);
                    popup.Show();
                }
                }">
            </ClientSideEvents>
            <SettingsPager AlwaysShowPager="True" PageSize="5">
                <PageSizeItemSettings Items="5, 10, 15, 20, 25, 30" Position="Left" Visible="True" />
            </SettingsPager>
            <Settings ShowFilterRow="True" />

        </dx:ASPxGridView>
    </form>
</body>
</html>
