<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="UploadFile.UploadFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Upload file for web server</h1>
    <asp:FileUpload ID="FileUpload1" runat="server" Width="348" Height="27" />
   <asp:RequiredFieldValidator runat="server" ID="rqUploadFile" ControlToValidate="FileUpload1" ErrorMessage="Please choose file upload !">
       
   </asp:RequiredFieldValidator>
    <asp:Button ID="btnUpload" runat="server" Text="Upload" Height="27" OnClick="btnUpload_OnClick"/>
    <br />
    <br />
    <asp:Image ID="mgUploadFile" runat="server" Width="150" />
</asp:Content>
