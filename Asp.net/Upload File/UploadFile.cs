using System;
using System.IO;
namespace UploadFile
{
    public partial class UploadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpload_OnClick(object sender, EventArgs e)
        {
            if (Page.IsValid && FileUpload1.HasFile && CheckFileType(FileUpload1.FileName))
            {

                //FileUpload1.HasFile => kiem tra xem co file duoc chon hay ko
                var fileName = "Images/" + DateTime.Now.ToString("ddMMyy_hhmmss_tt") + FileUpload1.FileName;
               //Mappath => lay duong dan tuyen doi va luu vao file path
                var filePath = MapPath(fileName);
                // save file
                FileUpload1.SaveAs(filePath);
                //hien thi 
                mgUploadFile.ImageUrl = fileName;
            }
        }

        /// <summary>
        /// get extension file ".jpg" 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        protected bool CheckFileType(string fileName)
        {
            var ext = Path.GetExtension(fileName);
            if (ext != null)
            {
                switch (ext.ToLower())
                {
                    case ".gif":
                        return true;
                    case ".jpg":
                        return true;
                    case ".png":
                        return true;
                    case ".jpeg":
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        }
    }
}