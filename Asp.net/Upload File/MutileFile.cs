using System;
using System.IO;
using System.Web;

namespace UploadFile
{
    public partial class UploadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpload_OnClick(object sender, EventArgs e)
        {
            if (UploadImages.HasFiles)
            {
                foreach (HttpPostedFile httpPostedFile in UploadImages.PostedFiles)
                {
                    if (CheckFileType(httpPostedFile.FileName))
                    {
                        httpPostedFile.SaveAs(Path.Combine(Server.MapPath("~/Images/"),
                        httpPostedFile.FileName)); listofuploadedfiles.Text += String.Format("{0}<br />", httpPostedFile.FileName);
                    }
                }}
        }

        /// <summary>
        /// get extension file ".jpg" 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        protected bool CheckFileType(string fileName)
        {
            var ext = Path.GetExtension(fileName);
            if (ext != null)
            {
                switch (ext.ToLower())
                {
                    case ".gif":
                        return true;
                    case ".jpg":
                        return true;
                    case ".png":
                        return true;
                    case ".jpeg":
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        }
    }
}