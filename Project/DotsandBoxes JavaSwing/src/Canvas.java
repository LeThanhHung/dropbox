import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * Vẽ các dấu chấm bằng đồ họa.
 * Các dấu chấm được vẽ trên JPanel
 * 
 */

public class Canvas extends JPanel {
	
    private int Width = 522;
	
	private int Height = 511;
	
	private int dotSize = 9;
	
	private int cols = 9;
	
	private int rows = 9;
	
	private int bw = Width/cols;
	
	private int bh = Height/rows;
	
	public Canvas(){
		
		this.setSize(Width,Height);
		setOpaque(false);
	}
	
	/**
	 * Phương thức vẽ các dấu chấm (dots)
	 * @param g đối tượng đồ họa dùng để vẽ các dấu chấm (dots)
	 */
	
    public void paint(Graphics g){
    	
		g.setColor(Color.GRAY);
		for(int col=0;col<this.cols;col++){
			for(int row=0;row<this.rows;row++){
				int x = row*bw;
				int y = col*bh;
				g.drawOval(x+10, y+10, dotSize, dotSize);
				g.fillOval(x+10, y+10, dotSize, dotSize);
			}
		}
	}
}
