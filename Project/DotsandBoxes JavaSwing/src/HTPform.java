import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * 
 * Form hướng dẫn người chơi cách chơi game. Form được hiển thị khi người chơi click vào nút How To Play
 *
 */

public class HTPform extends JFrame {
	

	/**
	 * Tạo form, set size, set background
	 */

	public HTPform(){
		
		// SET FORM
		
		setSize(500,600);
		setAlwaysOnTop(true);
		setUndecorated(true);
		setLocationRelativeTo(null);
		
		// TẠO CÁC HÌNH ẢNH HƯỚNG DẪN
		
		JLabel lb1 = new JLabel();
		lb1.setIcon(new ImageIcon(HTPform.class.getResource("/data/img/1.gif")));
		lb1.setBounds(162, 134, 240, 80);
		getContentPane().add(lb1);
		
		JLabel lb2 = new JLabel();
		lb2.setIcon(new ImageIcon(HTPform.class.getResource("/data/img/2.gif")));
		lb2.setBounds(172, 317, 240, 120);
		getContentPane().add(lb2);
		
		// TẠO CÁC DÒNG CHỮ HƯỜNG DẪN
		
		JLabel lb3 = new JLabel();
		lb3.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/b.png")));
		lb3.setBounds(-13, 227, 488, 89);
		getContentPane().add(lb3);
		
		JLabel lb4 = new JLabel();
		lb4.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/a.png")));
		lb4.setBounds(0, 75, 488, 89);
		getContentPane().add(lb4);
		
		JLabel lb5 = new JLabel();
		lb5.setIcon(new ImageIcon(GameForm.class.getResource("/data/img/c.png")));
		lb5.setBounds(0, 472, 488, 89);
		getContentPane().add(lb5);
		
		// TẠO NÚT CLOSE
		
		JButton bclose = new JButton();
		bclose.setBounds(399, 562, 56, 25);
		bclose.setBorder(BorderFactory.createEmptyBorder());
		bclose.setContentAreaFilled(false);
		bclose.setIcon(new ImageIcon(About.class.getResource("data/img/close.png")));
		bclose.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		getContentPane().add(bclose);
		bclose.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				HTPform.this.dispose();
				new MenuForm().setVisible(true);
			}
		});
		
		
		// SET BACKGROUND
		
		JLabel lbbg = new JLabel();
		lbbg.setIcon(new ImageIcon(HTPform.class.getResource("/data/img/htpbg.png")));
		lbbg.setBounds(0, 0, 500, 600);
		getContentPane().add(lbbg);
		
	}
}
