<?php 
class Json_load_ajax_news extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Get_news_model');
	}
	public function index(){
		$cate_id = $_GET["cate_id"];
      	$id = $_GET["id"];
      	$data = $this->Get_news_model->get_news_cate($cate_id,$id);
      	echo json_encode($data);
	}
}
?>