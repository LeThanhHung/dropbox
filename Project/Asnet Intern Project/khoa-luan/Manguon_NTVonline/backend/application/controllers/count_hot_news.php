<?php
class Count_hot_news extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Get_news_model');
  }
  public function index()
  {
    $data = $this->Get_news_model->count_hot_news();
    echo json_encode($data);
  }
}
?>