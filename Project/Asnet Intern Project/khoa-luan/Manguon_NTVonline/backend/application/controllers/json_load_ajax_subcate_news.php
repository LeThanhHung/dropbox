<?php 
class Json_load_ajax_subcate_news extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Get_news_model');
	}
	public function index(){
		$subcate_id = $_GET["subcate_id"];
      	$id = $_GET["id"];
      	$data = $this->Get_news_model->get_news_subcate($subcate_id,$id);
      	echo json_encode($data);
      	// echo "<pre>";
      	// print_r($data);
      	// echo"</pre>";
	}
}
 ?>