<?php 
class Json_load_ajax_navi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Get_news_model');
	}
	public function index(){
		$data = $this->Get_news_model->get_cate_name();
		echo json_encode($data);
	}
}
?>