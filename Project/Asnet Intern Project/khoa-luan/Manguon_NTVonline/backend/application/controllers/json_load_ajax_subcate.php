<?php 
class Json_load_ajax_subcate extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Get_news_model');
	}
	public function index(){
		$id =$_GET['cate_id'];
		$data = $this->Get_news_model->get_sub_cate_name($id);
		echo json_encode($data);
	}
}

 ?>