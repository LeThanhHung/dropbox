<?php 
class Insert_xpath extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Insert_news_model');
		$this->load->model('Get_news_model');
	}
	public function index(){
		$raws_data = array(
			// thế giới vnexpress
			array(
			'url'=>'http://vnexpress.net/tin-tuc/the-gioi',
			'items'=>'.//ul [@id="news_home" ]/li | .//div [@class="box_hot_news"]',
			'title' =>'.//div [@class="title_news"]/a | .//a [@class="txt_link"]/@title',
			'description'=>'.//div [@class="news_lead"] | .//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src | .//img [@class="width_image_common"]/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>2,
			'source_id'=>1,
			'subcate_id'=>1
			),
			// thế giới tuổi trẻ
			array(
			'url'=>'http://tuoitre.vn/tin/the-gioi',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a/@title',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>2,
			'source_id'=>3,
			'subcate_id'=>1
			),
			//thế giới tiên phong
			array(
			'url'=>'http://www.tienphong.vn/the-gioi/',
			'items'=>'.//section [@class="firstpage storylisting"]/article',
			'title' =>'.//h2 [@class="title"]/a',
			'description'=>'.//p [@ class="summary"]',
			'image'=>'.//p [@class="thumb"]/a/img/@src',
			'url2'=>'.//h2 [@class="title"]/a/@href',
			'content'=>'.//div [@class="article-contents cmscontents"] | .//div [@class="album"]',
			'cate_id'=>2,	
			'source_id'=>2,
			'subcate_id'=>1
			),
			// thời trang vnexpress
			array(
			'url'=>'http://giaitri.vnexpress.net/tin-tuc/thoi-trang',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>3,
			'source_id'=>1,
			'subcate_id'=>2
			),
			// thời trang tuổi trẻ
			array(
			'url'=>'http://tuoitre.vn/tin/van-hoa-giai-tri/thoi-trang',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>3,
			'source_id'=>3,
			'subcate_id'=>2
			),
			// ẩm thực vnexpress
			array(
			'url'=>'http://doisong.vnexpress.net/tin-tuc/noi-tro',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>3,
			'source_id'=>1,
			'subcate_id'=>3
			),
			// ẩm thực tuổi trẻ
			array(
			'url'=>'http://dulich.tuoitre.vn/tin/am-thuc',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a/@title',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>3,
			'source_id'=>3,
			'subcate_id'=>3
			),
			// du lịch vnexpress
			array(
			'url'=>'http://dulich.vnexpress.net',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href | .//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>3,
			'source_id'=>1,
			'subcate_id'=>4
			),
			// // du lịch tuổi trẻ => lỗi html.
			// array(
			// 'url'=>'http://dulich.tuoitre.vn/tin/van-hoa',
			// 'items'=>'.//div [@id="newhot_most_content"]/div',
			// 'title' =>'.//h3/a/@title',
			// 'description'=>'.//p',
			// 'image'=>'//a [@class="img_160_90"]/img/@src',
			// 'url2'=>'.//h3/a/@href',
			// 'content'=>'.//div [@class="fck"]',
			// 'cate_id'=>3,
			// 'source_id'=>3,
			// 'subcate_id'=>3
			// ),
			// tài chinh tuổi trẻ
			array(
			'url'=>'http://tuoitre.vn/tin/kinh-te/tai-chinh',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a/@title',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>4,
			'source_id'=>3,
			'subcate_id'=>5
			),
			// chứng khoán vnexpress
			array(
			'url'=>'http://kinhdoanh.vnexpress.net/tin-tuc/chung-khoan',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>4,
			'source_id'=>1,
			'subcate_id'=>6
			),
			// chứng khoán tiền phong
			array(
			'url'=>'http://www.tienphong.vn/Kinh-Te-Chung-Khoan/',
			'items'=>'.//section [@class="firstpage storylisting"]/article',
			'title' =>'.//h2 [@class="title"]',
			'description'=>'.//p [@ class="summary"]',
			'image'=>'.//p [@class="thumb"]/a/img/@src',
			'url2'=>'.//h2 [@class="title"]/a/@href',
			'content'=>'.//div [@class="article-contents cmscontents"] | .//div [@class="album"]',
			'cate_id'=>4,
			'source_id'=>2,
			'subcate_id'=>6
			),
			// Bất động sản vnexpress
			array(
			'url'=>'http://kinhdoanh.vnexpress.net/tin-tuc/bat-dong-san',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>4,
			'source_id'=>1,
			'subcate_id'=>7
			),
			// Bất động sản tuổi trẻ
			// array(
			// 'url'=>'http://diaoc.tuoitre.vn',
			// 'items'=>'.//div [@id="newhot_most_content"]/div',
			// 'title' =>'.//h3/a/@title',
			// 'description'=>'.//p',
			// 'image'=>'.//a [@class="img_160_90"]/img/@src',
			// 'url2'=>'.//h3/a/@href',
			// 'content'=>'.//div [@class="fck"]',
			// 'cate_id'=>4,
			// 'source_id'=>3,
			// 'subcate_id'=>7
			// ),
			// Bất động sản tiền phong
			array(
			'url'=>'http://www.tienphong.vn/dia-oc/',
			'items'=>'.//section [@class="firstpage storylisting"]/article',
			'title' =>'./h2 [@class="title"]/a',
			'description'=>'.//p [@ class="summary"]',
			'image'=>'.//p [@class="thumb"]/a/img/@src',
			'url2'=>'.//h2 [@class="title"]/a/@href',
			'content'=>'.//div [@class="article-contents cmscontents"]',
			'cate_id'=>4,
			'source_id'=>2,
			'subcate_id'=>7
			),

			//cntt-vn tiền phong
			array(
			'url'=>'http://www.tienphong.vn/cong-nghe-khoa-hoc/',
			'items'=>'.//section [@class="firstpage storylisting"]/article',
			'title' =>'.//h2 [@class="title"]/a',
			'description'=>'.//p [@ class="summary"]',
			'image'=>'.//p [@class="thumb"]/a/img/@src',
			'url2'=>'.//h2 [@class="title"]/a/@href',
			'content'=>'.//div [@class="article-contents cmscontents"] | .//div [@class="album"]',
			'cate_id'=>5,
			'source_id'=>2,
			'subcate_id'=>8
			),
			// khoa học tự nhiên vnexpress
			array(
			'url'=>'http://vnexpress.net/tin-tuc/khoa-hoc/ky-thuat-moi',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>5,
			'source_id'=>1,
			'subcate_id'=>9
			),
			// thiết bị phần cứng vnexpress
			array(
			'url'=>'ttp://sohoa.vnexpress.net/tin-tuc/doi-song-so',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>5,
			'source_id'=>1,
			'subcate_id'=>10
			),
			// thiết bi phần cứng tuổi trẻ
			array(
			'url'=>'http://nhipsongso.tuoitre.vn/tin/thiet-bi-so',
			'items'=>'.//article [@class="block-new"]',
			'title' =>'.//h3/a',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_254_143"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>5,
			'source_id'=>3,
			'subcate_id'=>10
			),
			// Bóng đá vnexpress
			array(
			'url'=>'http://thethao.vnexpress.net/tin-tuc/bong-da',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>6,
			'source_id'=>1,
			'subcate_id'=>11
			),
			// Bóng đá tuổi trẻ
			array(
			'url'=>'http://thethao.tuoitre.vn/trang/bong-da',
			'items'=>'.//div [@class="content_small_cate"]',
			'title' =>'.//h3 [@class="name"]/a',
			'description'=>'.//p [@class="desc"]',
			'image'=>'.//div [@class="image"]/a/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="one_new"]',
			'cate_id'=>6,
			'source_id'=>3,
			'subcate_id'=>11
			),
			// bóng đá tiền phong
			array(
			'url'=>'http://www.tienphong.vn/The-Thao-Bong-Da/',
			'items'=>'.//section [@class="firstpage storylisting"]/article',
			'title' =>'.//h2 [@class="title"]/a',
			'description'=>'.//p [@ class="summary"]',
			'image'=>'.//p [@class="thumb"]/a/img/@src',
			'url2'=>'.//h2 [@class="title"]/a/@href',
			'content'=>'.//div [@class="article-contents cmscontents"] | .//div [@class="album"]',
			'cate_id'=>6,
			'subcate_id'=>11,
			'source_id'=>2,		
			),
			// quần vợt vnexpress
			array(
			'url'=>'http://thethao.vnexpress.net/tin-tuc/tennis',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>6,
			'subcate_id'=>12,
			'source_id'=>1,			
			),
			// quần vợt tuổi trẻ
			array(
			'url'=>'http://thethao.tuoitre.vn/trang/quan-vot',
			'items'=>'.//div [@class="content_small_cate"]',
			'title' =>'.//h3 [@class="name"]/a',
			'description'=>'.//p [@class="desc"]',
			'image'=>'.//div [@class="image"]/a/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="one_new"]',
			'cate_id'=>6,
			'subcate_id'=>12,
			'source_id'=>3,			
			),
			// âm nhạc vnexpress
			array(
			'url'=>'http://giaitri.vnexpress.net/tin-tuc/nhac',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>7,
			'subcate_id'=>13,
			'source_id'=>1,			
			),
			// âm nhạc -  tuổi trẻ
			array(
			'url'=>'http://tuoitre.vn/tin/van-hoa-giai-tri/am-nhac',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a/@title',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>7,
			'subcate_id'=>13,
			'source_id'=>3,			
			),
			// Sân khấu điện ảnh - vnexpress
			array(
			'url'=>'http://giaitri.vnexpress.net/tin-tuc/san-khau-my-thuat',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>7,
			'subcate_id'=>14,
			'source_id'=>1,			
			),
			// Sân khấu điện ảnh - tuổi trẻ
			array(
			'url'=>'http://tuoitre.vn/tin/van-hoa-giai-tri/dien-anh',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a/@title',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>7,
			'subcate_id'=>14,
			'source_id'=>3,			
			),
			// Điện ảnh - vnexpress
			array(
			'url'=>'http://giaitri.vnexpress.net/tin-tuc/truyen-hinh',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>7,
			'subcate_id'=>14,
			'source_id'=>1,			
			),
			// Sách báo văn thơ - vnexpress
			array(
			'url'=>'http://giaitri.vnexpress.net/tin-tuc/sach',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>7,
			'subcate_id'=>15,
			'source_id'=>1,			
			),
			//Sách báo văn thơ - tuổi trẻ
			array(
			'url'=>'http://tuoitre.vn/tin/van-hoa-giai-tri/van-hoc-sach',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a/@title',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>7,
			'subcate_id'=>15,
			'source_id'=>3,			
			),
			// Thời sự - vnexpress
			array(
			'url'=>'http://vnexpress.net/tin-tuc/phap-luat/ho-so-pha-an',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>8,
			'subcate_id'=>16,
			'source_id'=>1,			
			),
			//Thời sự  - tiền phong
			array(
			'url'=>'http://www.tienphong.vn/ban-tin-113/',
			'items'=>'.//section [@class="firstpage storylisting"]/article',
			'title' =>'.//h2 [@class="title"]/a',
			'description'=>'.//p [@ class="summary"]',
			'image'=>'.//p [@class="thumb"]/a/img/@src',
			'url2'=>'.//h2 [@class="title"]/a/@href',
			'content'=>'.//div [@class="article-contents cmscontents"] | .//div [@class="album"]',
			'cate_id'=>8,
			'subcate_id'=>16,
			'source_id'=>2,		
			),
			// An ninh - tuổi trẻ
			array(
			'url'=>'http://tuoitre.vn/tin/phap-luat/ky-su-phap-dinh',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a/@title',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>8,
			'subcate_id'=>17,
			'source_id'=>3,			
			),
			// Du học  - vnexpress
			array(
			'url'=>'http://vnexpress.net/tin-tuc/giao-duc/du-hoc',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>9,
			'subcate_id'=>18,
			'source_id'=>1,			
			),
			// Du học  - tuổi trẻ
			array(
			'url'=>'http://tuoitre.vn/tin/giao-duc/du-hoc',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a/@title',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>9,
			'subcate_id'=>18,
			'source_id'=>3,			
			), 
			// Du học -  tiền phong
			array(
			'url'=>'http://www.tienphong.vn/giao-duc-du-hoc/',
			'items'=>'.//section [@class="firstpage storylisting"]/article',
			'title' =>'.//h2 [@class="title"]/a',
			'description'=>'.//p [@ class="summary"]',
			'image'=>'.//p [@class="thumb"]/a/img/@src',
			'url2'=>'.//h2 [@class="title"]/a/@href',
			'content'=>'.//div [@class="article-contents cmscontents"] | .//div [@class="album"]',
			'cate_id'=>9,
			'subcate_id'=>18,
			'source_id'=>2,		
			),
			// Tuyển sinh - vnexpress
			array(
			'url'=>'http://vnexpress.net/tin-tuc/giao-duc/tuyen-sinh',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>9,
			'subcate_id'=>19,
			'source_id'=>1,			
			),
			// Tuyển sinh - tuổi trẻ
			array(
			'url'=>'http://tuyensinh.tuoitre.vn',
			'items'=>'.//div [@class="left-side"]/div',
			'title' =>'.//h3/a',
			'description'=>'.//p',
			'image'=>'.//a/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>9,
			'subcate_id'=>19,
			'source_id'=>3,			
			),
			// Làm đẹp -vnexpress
			array(
			'url'=>'http://giaitri.vnexpress.net/tin-tuc/lam-dep',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>10,
			'subcate_id'=>20,
			'source_id'=>1,			
			),
			// Tình yêu - vnexpress
			array(
			'url'=>'http://vnexpress.net/tin-tuc/tam-su/loi-yeu',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>10,
			'subcate_id'=>21,
			'source_id'=>1,	
			),
			// Tình yêu - Tuổi trẻ
			array(
			'url'=>'http://tuoitre.vn/tin/nhip-song-tre/song-va-yeu',
			'items'=>'.//div [@id="newhot_most_content"]/div',
			'title' =>'.//h3/a/@title',
			'description'=>'.//p',
			'image'=>'.//a [@class="img_160_90"]/img/@src',
			'url2'=>'.//h3/a/@href',
			'content'=>'.//div [@class="fck"]',
			'cate_id'=>10,
			'subcate_id'=>21,
			'source_id'=>3,			
			),
			// giới tính - vnexpress
			array(
			'url'=>'http://doisong.vnexpress.net/tin-tuc/gioi-tinh',
			'items'=>'.//ul [@id="news_home" ]/li',
			'title' =>'.//div [@class="title_news"]/a',
			'description'=>'.//div [@class="news_lead"]',
			'image'=>'.//div [@class="thumb"]/a/img/@src',
			'url2'=>'.//div [@class="title_news"]/a/@href',
			'content'=>'.//div [@id="article_content"] | .//div [@class="fck_detail width_common"]',
			'cate_id'=>10,
			'subcate_id'=>21,
			'source_id'=>1,			
			)

		);
		$xpath = array();
		$data_xpath=  $this->Get_news_model->get_xpath();
		foreach ($data_xpath as $data) {
			$xpath[] = $data['url'];

		}
		foreach ($raws_data as $data) {
			$url = $data['url'];
			if(!in_array($url, $xpath)){
				$this->Insert_news_model->insert_xpath($data);
				echo "insert: ".$url."<br/>";
			}else{
				echo "not insert: ".$url."<br/>";
			}
		}
		
	}
}
 ?>