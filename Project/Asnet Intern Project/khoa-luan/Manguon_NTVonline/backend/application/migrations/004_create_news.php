<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_news extends CI_Migration {

  public function up() {
    $this->dbforge->add_field(array(
      'news_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'source_id' => array(
        'type' => 'INT',
        'constraint' => '11'
      ),
      'title' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
        'null' => TRUE
      ),
      'link' => array(
        'type' => 'VARCHAR',
        'constraint' => '500',
        'null' => TRUE
      ),
      'image' => array(
        'type' => 'VARCHAR',
        'constraint' => '500',
        'null' => TRUE,
      ),
      'description' => array(
        'type' => 'TEXT',
        'null' => TRUE
      ),

      'create_date' => array(
        'type' => 'DATETIME'
      ),
      'hot_news' => array(
        'type' => 'TINYINT'
      ),
      'number_read' => array(
        'type' => 'INT',
        'constraint' => '11'
      ),
      'content' => array(
        'type' => 'TEXT'
      ),
      'status' => array(
        'type' => 'TINYINT'
      ),
    ));
    $this->dbforge->add_key('news_id', TRUE);
    $this->dbforge->create_table('news');
  }

  public function down()
  {
    $this->dbforge->drop_table('news');
  }
}
 ?>