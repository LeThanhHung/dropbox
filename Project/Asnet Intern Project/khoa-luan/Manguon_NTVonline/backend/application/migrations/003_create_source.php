<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_source extends CI_Migration {

  public function up() {
    $this->dbforge->add_field(array(
      'source_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'source_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255'
      ),
      'status' => array(
        'type' => 'TINYINT'
      ),
    ));
    $this->dbforge->add_key('source_id', TRUE);
    $this->dbforge->create_table('sources');
  }

  public function down()
  {
    $this->dbforge->drop_table('sources');
  }
}
 ?>