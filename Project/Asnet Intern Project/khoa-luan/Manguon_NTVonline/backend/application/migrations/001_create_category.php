<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_category extends CI_Migration {

  public function up() {
    $this->dbforge->add_field(array(
      'cate_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'cate_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255'
      ),
      'link' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      ),
      'status' => array(
        'type' => 'TINYINT'
      ),
    ));
    $this->dbforge->add_key('cate_id', TRUE);
    $this->dbforge->create_table('categories');
  }

  public function down()
  {
    $this->dbforge->drop_table('categories');
  }
}
 ?>