<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_subcategory extends CI_Migration {

  public function up() {
    $this->dbforge->add_field(array(
      'subcate_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'cate_id' => array(
        'type' => 'INT',
        'constraint' => 11
      ),
      'subcate_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255'
      ),
      'link' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      ),
      'status' => array(
        'type' => 'TINYINT'
      ),
    ));
    $this->dbforge->add_key('subcate_id', TRUE);
    $this->dbforge->create_table('subcategories');
  }

  public function down()
  {
    $this->dbforge->drop_table('subcategories');
  }
}
 ?>