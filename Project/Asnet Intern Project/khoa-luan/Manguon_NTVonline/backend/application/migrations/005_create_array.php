<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_array extends CI_Migration {
    
  public function up() {
    $this->dbforge->add_field(array(
        'id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'url' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      ),
      'items' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      ),
       'title' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      ),
       'description' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      ),
       'image' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      ),
      'url2' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      ),
       'content' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      ),
       'cate_id' => array(
        'type' => 'INT',
        'constraint' => '11'
      ),
       'source_id' => array(
        'type' => 'INT',
        'constraint' => '11'
      ),
       'subcate_id' => array(
        'type' => 'INT',
        'constraint' => '11'
      )
    ));
    $this->dbforge->add_key('id', TRUE);
    $this->dbforge->create_table('raws');
  }

  public function down()
  {
    $this->dbforge->drop_table('raws');
  }
}
 ?>