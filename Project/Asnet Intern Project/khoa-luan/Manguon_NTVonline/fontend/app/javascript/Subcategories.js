var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var currentSelectedIndex = 0,
    currentSelectedIndexLeftOld = 0,
    currentSelectedIndexLeft = 0,
    numMaxRowLeft = 0,
    postNewsCount = 3,
    row = 1,
    numMaxRow = 2,
    iTem = 0,
    typeMenu = 1, // right
    selectItem,
    selectItemLeft,
    indexLink = 2,
    idPage = 0,
    subStartIndex = 0,
    subnews = 0,
    countSubNews = 0,
    currentPage = 1,
    maxPage = 0,
    numMaxPage = 0;
var Subcategories = {

};

//write cookie
function writeCookie(name, value, days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

// reading Cookie
function readCookie(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for (i = 0; i < ca.length; i++) {
        c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return '';
}

function loadSub(cate_id) {
    $.ajax({
        type: 'GET',
        url: 'http://192.168.56.1/backend/Json_load_ajax_subcate',
        data: 'cate_id=' + cate_id,
        dataType: 'json',
        async: false,
        success: function(data) {
            alert('success');
            $.each(data, function(key, value) {
                if (subStartIndex == 0) subStartIndex = parseInt(value.subcate_id);
                numMaxRowLeft++;
                var html;
                html = '<li class="navi_' + value.subcate_id + ' menu">';
                html += '<a href="subcategory.html?subcate_id=' + value.subcate_id + '" class="linkSub">' + value.subcate_name + '</a>';
                html += '</li>';
                $('#navigation').append($(html));
            });

            $('.menu a').eq(currentSelectedIndexLeft).focus();
            selectItemLeft = $('.menu').eq(currentSelectedIndexLeft).addClass('selected');
            document.getElementById("anchor").focus();
        },

        error: function(request, status, error) {
            alert("error:" + request.responseText);
        }

    });
}

function loadSubNews(subcate_id, idPage) {
    $.ajax({
        type: 'GET',
        url: 'http://192.168.56.1/backend/Json_load_ajax_subcate_news',
        data: 'subcate_id=' + subcate_id + '&id=' + idPage,
        dataType: 'json',
        async: false,
        success: function(data) {
            var html;
            html = '<div class="row top-news">';
            $.each(data, function(key, value) {
                html += '<div class="col-sm-4">';
                html += '<div class="post-content">';
                html += '<div class="post-' + currentSelectedIndexLeft + '">';
                html += ' <a  href="details.html?news_id=' + value.news_id + '"  class="link-Main"><img src="' + value.image + '"></a>';
                html += '</div>';
                html += '<a href="javascript:void(0)">' + value.source_name + '</a>';
                html += '<p>' + value.title + '</p>';
                html += '</div>';
                html += '</div>';
            });
            html += '</div>';
            $('#post_category').append(html);
            alert('success news');
            $('.post-' + currentSelectedIndexLeft + ' a').eq(currentSelectedIndex).focus();
            selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
            iTem = $('.post-' + currentSelectedIndexLeft).length;
            numMaxRow = iTem / postNewsCount;
            document.getElementById("anchor").focus();
        },
        error: function(request, status, error) {
            alert("error: " + request.responseText);
        }
    });

}

// function load Date
function loadDate() {
    var d = new Date();
    var days = new Array("Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy");
    document.getElementById("days").innerHTML = days[d.getDay()];
}

//count hot news.
function subNewsCount(id) {
    var subnews = $.ajax({
        type: 'GET',
        url: 'http://192.168.56.1/backend/Count_news_sub',
        data: 'id=' + id,
        dataType: 'json',
        async: false,
        success: function(data) {},
        error: function() {
            alert('Error function countSubNews');
        }
    });
    return subnews.responseText;
}

//function load number page
function numberPage(currentPage, numMaxPage) {
    numMaxPage = Math.ceil(countSubNews / 6); // tong so trang
    document.getElementById("page").innerHTML = currentPage + " / " + numMaxPage;
    return numMaxPage;
}

Subcategories.onLoad = function() {

    //this.enableKeys();
    widgetAPI.sendReadyEvent();
    indexLink = readCookie('indexLink');
    var link = window.location.href;
    var cate_id = link.split('=');
    cate_id = parseInt(cate_id[1]);
    cate_id += 1;

    // load sub category
    loadSub(cate_id);

    // load news sub category
    loadSubNews(parseInt(subStartIndex) + currentSelectedIndexLeft, 0);

    // count all news of the sub category
    countSubNews = subNewsCount(subStartIndex + currentSelectedIndexLeft);

    // Display days
    loadDate();

    // load number page
    numberPage(currentPage, numMaxPage);
    maxPage = numberPage(currentPage, numMaxPage);

};

Subcategories.onUnload = function() {

};

/*
 * Subcategories.enableKeys = function() {

    iTem = $('.post-' + currentSelectedIndexLeft).length;
    numMaxRow = iTem / postNewsCount;
    document.getElementById("anchor").focus();
    $('.menu a').eq(currentSelectedIndexLeft).focus();
    selectItemLeft = $('.menu').eq(currentSelectedIndexLeft).addClass('selected');
    $('.post-' + currentSelectedIndexLeft + ' a').eq(currentSelectedIndex).focus();
    selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');

};
 */
Subcategories.keyDown = function() {
    var keyCode = event.keyCode;
    alert("Key pressed: " + keyCode);
    alert("typeMenu: " + typeMenu);
    switch (typeMenu) {
        case 1: // menu right
            switch (keyCode) {
                case tvKey.KEY_RETURN:
                    alert("KEY_RETURN");
                    widgetAPI.blockNavigation(event);
                    history.go(-1);
                case tvKey.KEY_LEFT:
                    alert("LEFT");
                    $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                    if (currentSelectedIndex == 0) {
                        $('.menu a').eq(currentSelectedIndexLeft).focus();
                        selectItemLeft = $('.menu').eq(currentSelectedIndexLeft).addClass('selected');
                        currentSelectedIndex = 0;
                        typeMenu = 0; //left
                        document.getElementById("anchor").focus();
                    } else {
                        if (currentSelectedIndex % 3 == 0) {
                            $('.menu a').eq(currentSelectedIndexLeft).focus();
                            selectItemLeft = $('.menu').eq(currentSelectedIndexLeft).addClass('selected');
                            typeMenu = 0; // left
                            currentSelectedIndex = 0;
                            row = 1;
                            document.getElementById("anchor").focus();
                        } else {
                            currentSelectedIndex--;
                            selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
                        }
                    }
                    break;
                case tvKey.KEY_RIGHT:
                    alert("RIGHT");
                    $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                    if (currentSelectedIndex == iTem - 1) {
                    	if (idPage + 6 < countSubNews) {
                            idPage += 6;
                            currentPage += 1;
                            $('#post_category').html('');
                            loadSubNews(subStartIndex + currentSelectedIndexLeft, idPage);
                            numberPage(currentPage, numMaxPage);
                        }
                        if (currentPage == maxPage) {
                            typeMenu = 1;
                            currentSelectedIndex = 0;
                            $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
                        }
                    } else {
                        currentSelectedIndex++;
                        if (currentSelectedIndex % 3 == 0) {
                            row++;
                        }
                    }
                    selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
                    break;
                case tvKey.KEY_UP:
                    alert("UP");
                    if (row > 1) {
                        $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                        row--;
                        currentSelectedIndex = currentSelectedIndex - postNewsCount;
                        selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
                    } else {
                        $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                        row = numMaxRow;
                        currentSelectedIndex = currentSelectedIndex + ((numMaxRow - 1) * postNewsCount);
                        selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
                    }
                    break;
                case tvKey.KEY_DOWN:
                    alert("DOWN");
                    if (row < numMaxRow) {
                        $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                        row++;
                        currentSelectedIndex += postNewsCount;
                        selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');

                    } else {
                        $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                        currentSelectedIndex = currentSelectedIndex - ((numMaxRow - 1) * postNewsCount);
                        row = 1;
                        selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
                    }
                    break;
                case tvKey.KEY_RED:
                    alert("RED");
                    window.location = 'categories.html';
                    break;
                case tvKey.KEY_GREEN:
                    alert("GREEN");
                    window.location = 'numberRead.html';
                    break;
                case tvKey.KEY_YELLOW:
                    alert("YELLOW");
                    window.location = 'sources.html';
                    break;                 

                case tvKey.KEY_RW:
                    alert("KEY_RW");
                    if (idPage - 6 >= 0) {
                        idPage -= 6;
                        currentPage -= 1;
                        $('#post_category').html('');
                        loadSubNews(subStartIndex + currentSelectedIndexLeft, idPage);
                        numberPage(currentPage, numMaxPage);
                    }
                    break;
                case tvKey.KEY_FF:
                    alert("KEY_FF");
                    if (idPage + 6 < countSubNews) {
                        idPage += 6;
                        currentPage += 1;
                        $('#post_category').html('');
                        loadSubNews(subStartIndex + currentSelectedIndexLeft, idPage);
                        numberPage(currentPage, numMaxPage);
                    }
                    if (currentPage == maxPage) {
                        typeMenu = 1;
                        currentSelectedIndex = 0;
                        $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
                    }
                    break;

                case tvKey.KEY_ENTER:
                case tvKey.KEY_PANEL_ENTER:
                    alert("ENTER");
                    var subLink = selectItem.find('.link-Main').attr("href");
                    writeCookie(indexLink++, window.location, null);
                    writeCookie('indexLink', indexLink, null);
                    window.location = subLink;
                    break;
                default:
                    alert("Unhandled key");
                    break;
            };
            break;
        case 0: // menu left
            switch (keyCode) {
                case tvKey.KEY_RETURN:
                case tvKey.KEY_PANEL_RETURN:
                    alert("RETURN");
                    widgetAPI.blockNavigation(event);
                    history.go(-1);
                    break;
                case tvKey.KEY_RIGHT:
                    alert("RIGHT");
                    typeMenu = 1;
                    currentSelectedIndex = 0;
                    $('#post_category').html('');
                    loadSubNews(subStartIndex + currentSelectedIndexLeft, 0);

                    currentSelectedIndexLeftOld = currentSelectedIndexLeft;
                    $('.post-' + currentSelectedIndexLeft + ' a').eq(currentSelectedIndex).focus();
                    selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
                    iTem = $('.post-' + currentSelectedIndexLeft).length;
                    numMaxRow = iTem / postNewsCount;
                    document.getElementById("anchor").focus();
                    break;
                case tvKey.KEY_UP:
                    alert("UP");
                    if (currentSelectedIndexLeft == 0) {
                        $('.menu').eq(currentSelectedIndexLeft).removeClass('selected');
                        currentSelectedIndexLeft = numMaxRowLeft - 1;
                        selectItemLeft = $('.menu').eq(currentSelectedIndexLeft).addClass('selected');
                    } else {
                        $('.menu').eq(currentSelectedIndexLeft).removeClass('selected');
                        currentSelectedIndexLeft--;
                        selectItemLeft = $('.menu').eq(currentSelectedIndexLeft).addClass('selected');
                    }
                    break;
                case tvKey.KEY_DOWN:
                    alert("DOWN");
                    if (currentSelectedIndexLeft == (numMaxRowLeft - 1)) {
                        $('.menu').eq(currentSelectedIndexLeft).removeClass('selected');
                        currentSelectedIndexLeft = 0;
                        selectItemLeft = $('.menu').eq(currentSelectedIndexLeft).addClass('selected');
                    } else {
                        $('.menu').eq(currentSelectedIndexLeft).removeClass('selected');
                        currentSelectedIndexLeft++;
                        selectItemLeft = $('.menu').eq(currentSelectedIndexLeft).addClass('selected');
                    }
                    break;
                case tvKey.KEY_RED:
                    alert("RED");
                    window.location = 'categories.html';
                    break;
                case tvKey.KEY_GREEN:
                    alert("GREEN");
                    window.location = 'numberRead.html';
                    break;
                case tvKey.KEY_YELLOW:
                    alert("YELLOW");
                    window.location = 'sources.html';
                    break; 
                case tvKey.KEY_ENTER:
                case tvKey.KEY_PANEL_ENTER:
                    alert("ENTER");
                    typeMenu = 1;
                    currentSelectedIndex = 0;
                   // load news of subcategory
                    $('#post_category').html('');
                    loadSubNews(subStartIndex + currentSelectedIndexLeft, 0);

                    //count all news of subcategory
                    countSubNews = subNewsCount(subStartIndex + currentSelectedIndexLeft);

                    //load number page
                    currentPage = 1;
                    numberPage(currentPage, numMaxPage);
                    maxPage = numberPage(currentPage, numMaxPage);
                    break;
                default:
                    alert("Unhandled key");
                    break;
            }
    }

};