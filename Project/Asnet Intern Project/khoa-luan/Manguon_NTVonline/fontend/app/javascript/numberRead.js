var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();

var currentSelectedIndex = 0,
    currentSelectedIndexLeftOld = 0,
    currentSelectedIndexLeft = 0,
    postNewsCount = 4,
    row = 1,
    numMaxRow = 2,
    iTem = 0,
    selectItem,
    selectItemLeft,
    idPage = 0,
    countNumberRead = 30,
    currentPage = 1,
    numMaxPage = 0,
    maxPage = 0;
var NumberRead = {};


//load news categories
function loadPage(idPage) {
    $.ajax({
        type: 'GET',
        url: 'http://192.168.56.1/backend/Json_load_number_read',
        data: 'id=' + idPage,
        dataType: 'json',
        success: function(data) {
            alert('success1');
            $.each(data, function(key, value) {
                var html;
                html = '<div class="col-sm-3">';
                html += '<div class="post-content">';
                html += '<div class="post-0">';
                html += ' <a  href="details.html?news_id=' + value.news_id + '"  class="link-Main"><img src="' + value.image + '"></a>';
                html += '</div>';
                html += '<a href="javascript:void(0)">' + value.source_name + '</a>';
                html += '<p class="pull-right"> <i style="font-size:10px;">Lượt xem: ' + value.number_read + '</i></p>';
                html += '<p>' + value.title + '</p>';
                html += '</div>';
                html += '</div>';
                $('#post_category').append($(html));
            });

            $('.post-' + currentSelectedIndexLeft + ' a').eq(currentSelectedIndex).focus();
            selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
            iTem = $('.post-' + currentSelectedIndexLeft).length;
            numMaxRow = iTem / postNewsCount;
            document.getElementById("anchor").focus();
        }
    });
}

function loadCate() {
    $.ajax({
        type: 'GET',
        url: 'http://192.168.56.1/backend/Json_load_ajax_navi',
        dataType: 'json',
        success: function(data) {
            alert('success2');
            $.each(data, function(key, value) {
                var html;
                html = '<li class="navi_' + value.cate_id + ' menu">';
                html += '<a href="subcategory.html?cate_id=' + value.cate_id + '" class="linkSub">' + value.cate_name + '</a>';
                html += '</li>';
                $('#navigation').append($(html));

            });
        },
        error: function() {
            alert('Error: ');
        }
    });
}

// function load date
function loadDate() {
    var d = new Date();
    var days = new Array("Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy");
    document.getElementById("days").innerHTML = days[d.getDay()];
}

//function load number page
function numberPage(currentPage, numMaxPage) {
    numMaxPage = Math.ceil(countNumberRead / 6); // tong so trang
    document.getElementById("page").innerHTML = currentPage + " / " + numMaxPage;
    return numMaxPage;
}

NumberRead.onLoad = function() {
    //NumberRead.enableKeys();
    widgetAPI.sendReadyEvent();

    // get cate_name of categories.
    loadCate();

    // paging for part hot  news.
    loadPage(0);

    // Display days
    loadDate();

    // load number page
    numberPage(currentPage, numMaxPage);

    maxPage = numberPage(currentPage, numMaxPage);

};

NumberRead.onUnload = function() {

};

/*
 * NumberRead.enableKeys = function() {
    $('.post-' + currentSelectedIndexLeft + ' a').eq(currentSelectedIndex).focus();
    selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
    iTem = $('.post-' + currentSelectedIndexLeft).length;
    numMaxRow = iTem / postNewsCount;
    document.getElementById("anchor").focus();
};
 */

NumberRead.keyDown = function() {
    var keyCode = event.keyCode;
    alert("Key pressed: " + keyCode);

    switch (keyCode) {
        case tvKey.KEY_RETURN:
            alert("KEY_RETURN");
            widgetAPI.blockNavigation(event);
            history.go(-1);
            break;
        case tvKey.KEY_LEFT:
            alert("LEFT");
            $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
            if (currentSelectedIndex == 0) {
                if (idPage - 8 >= 0) {
                    idPage -= 8;
                    currentPage -= 1;
                    $('#post_category').html($(''));
                    loadPage(idPage);
                    numberPage(currentPage, numMaxPage);
                }
            } else {
                currentSelectedIndex--;
                if (currentSelectedIndex % 3 == 0) {
                    row--;
                }
            }
            selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');

            break;
        case tvKey.KEY_RIGHT:
            alert("RIGHT");
            $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
            if (currentSelectedIndex == iTem - 1) {
                if (idPage + 8 < 40) {
                    idPage += 8;
                    currentPage += 1;
                    $('#post_category').html($(''));
                    loadPage(idPage);
                    numberPage(currentPage, numMaxPage);
                }
                if (currentPage == maxPage) {
                    currentSelectedIndex = 0;
                    $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
                }

            } else {
                currentSelectedIndex++;
                if (currentSelectedIndex % 3 == 0) {
                    row++;
                }
            }

            selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
            break;
        case tvKey.KEY_UP:
            alert("UP");
            if (row > 1) {
                $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                row--;
                currentSelectedIndex = currentSelectedIndex - postNewsCount;
                selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
            } else {
                $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                row = numMaxRow;
                currentSelectedIndex = currentSelectedIndex + ((numMaxRow - 1) * postNewsCount);
                selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
            }
            break;
        case tvKey.KEY_DOWN:
            alert("DOWN");
            if (row < numMaxRow) {
                $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                row++;
                currentSelectedIndex += postNewsCount;
                selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');

            } else {
                $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).removeClass('selected');
                currentSelectedIndex = currentSelectedIndex - ((numMaxRow - 1) * postNewsCount);
                row = 1;
                selectItem = $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
            }
            break;
        case tvKey.KEY_RED:
            alert("RED");
            window.location = 'categories.html';
            break;
        case tvKey.KEY_RW:
            alert("KEY_RW");
            if (idPage - 8 >= 0) {
                idPage -= 8;
                currentPage -= 1;
                $('#post_category').html($(''));
                loadPage(idPage);
                numberPage(currentPage, numMaxPage);
            }
            break;
        case tvKey.KEY_FF:
            alert("KEY_FF");
            if (idPage + 8 < 40) {
                idPage += 8;
                currentPage += 1;
                $('#post_category').html($(''));
                loadPage(idPage);
                numberPage(currentPage, numMaxPage);
            }
            if (currentPage == maxPage) {
                currentSelectedIndex = 0;
                $('.post-' + currentSelectedIndexLeft).eq(currentSelectedIndex).addClass('selected');
            }
            break;
        case tvKey.KEY_ENTER:
        case tvKey.KEY_PANEL_ENTER:
            alert("ENTER");
            var ilink = selectItem.find('.link-Main').attr("href");;
            window.location = ilink;
            break;
        default:
            alert("Unhandled key");
            break;
    };


};